package org.v2.finance.expenses.ads;

import android.app.Activity;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-09-2019
 * @since 08-09-2019
 */
public class BannerAd extends Ad {

    private final Logger logger = LoggerFactory.getLogger(BannerAd.class);

    private final PublisherAdView bannerAd;

    public BannerAd(Activity context, PublisherAdView bannerAd) {
        super(context);
        this.bannerAd = bannerAd;
    }

    @Override
    public void assignAd() {
        bannerAd.loadAd(getAdRequest());
    }

    private PublisherAdRequest getAdRequest() {
        if (BuildConfig.DEBUG) logger.info("Requesting Banner Ad now!");
        return new PublisherAdRequest.Builder()
                //.addTestDevice("EF6225FB87BD949F87A70CB1E5F23B4A")
                .build();
    }

}
