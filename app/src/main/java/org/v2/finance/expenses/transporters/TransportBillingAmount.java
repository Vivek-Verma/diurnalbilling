package org.v2.finance.expenses.transporters;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 16-02-2019
 * @since 16-02-2019
 */
public class TransportBillingAmount {

    private final String sign;
    private final Double receivedAmount;
    private Double actualAmount;
    private Boolean isAmountPositive;
    private final String currency;

    public TransportBillingAmount(String sign, String currency, Double receivedAmount) {
        this.sign = sign;
        this.currency = currency;
        this.receivedAmount = receivedAmount;
        setTheRequiredParams(sign, receivedAmount);
    }

    private void setTheRequiredParams(String sign, Double amount) {
        actualAmount = Double.NaN;
        switch (sign) {
            case ENTRY_TYPE_POSITIVE:
                actualAmount = amount;
                isAmountPositive = true;
                break;
            case ENTRY_TYPE_NEGATIVE:
                actualAmount = amount * -1;
                isAmountPositive = false;
                break;
        }
    }

    public String getSign() {
        return sign;
    }

    public Double getReceivedAmount() {
        return receivedAmount;
    }

    public Double getActualAmount() {
        return actualAmount;
    }

    public Boolean getIsAmountPositive() {
        return isAmountPositive;
    }

    public String getCurrency() {
        return currency;
    }
}
