package org.v2.finance.expenses.transporters;

import org.v2.finance.expenses.constants.Constants;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 23-12-2018
 * @since 23-12-2018
 * <p>
 * used to store the results of verification of string from a edit text
 */
public class TransportEditText {

    private final Boolean valid;
    private final String data;

    public TransportEditText(Boolean isValid) {
        this.valid = isValid;
        this.data = Constants.STRING_NULL_OBJ;
    }

    public TransportEditText(Boolean isValid, String data) {
        this.valid = isValid;
        this.data = data;
    }

    public Boolean isValid() {
        return valid;
    }

    public String getData() {
        return data;
    }
}
