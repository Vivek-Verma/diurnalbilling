package org.v2.finance.expenses.util;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import java.util.HashMap;
import java.util.Map;

import static org.v2.finance.expenses.constants.Constants.CURRENCY_CND;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_EUR;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_FR;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_GBP;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_INR;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_RUB;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_USD;
import static org.v2.finance.expenses.constants.Constants.CURRENCY_YEN;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_CURRENCY;

/**
 * @author Vivek Verma
 * @since 12/4/21
 */
public class CurrencyUtil {


    public static final Map<UserMappingProto.Currency, String> CURRENCY_SIGN = new HashMap<>();
    public static UserMappingProto.Currency currencyToApply = DEFAULT_CURRENCY;
    public static String currencySignToDisplay = CURRENCY_INR;

    static {
        CURRENCY_SIGN.put(UserMappingProto.Currency.INR, CURRENCY_INR);
        CURRENCY_SIGN.put(UserMappingProto.Currency.CND, CURRENCY_CND);
        CURRENCY_SIGN.put(UserMappingProto.Currency.USD, CURRENCY_USD);
        CURRENCY_SIGN.put(UserMappingProto.Currency.YEN, CURRENCY_YEN);
        CURRENCY_SIGN.put(UserMappingProto.Currency.GBP, CURRENCY_GBP);
        CURRENCY_SIGN.put(UserMappingProto.Currency.RUB, CURRENCY_RUB);
        CURRENCY_SIGN.put(UserMappingProto.Currency.EUR, CURRENCY_EUR);
        CURRENCY_SIGN.put(UserMappingProto.Currency.FR, CURRENCY_FR);
    }
}
