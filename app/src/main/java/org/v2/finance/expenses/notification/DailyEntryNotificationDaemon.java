package org.v2.finance.expenses.notification;

import android.content.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.util.Helper;

/**
 * @author Vivek Verma
 * @since 15-12-2019
 */
public class DailyEntryNotificationDaemon implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(DailyEntryNotificationDaemon.class);
    private final Notifier notifier;
    private final LocalSavedPrefs localSavedPrefs;

    public DailyEntryNotificationDaemon(Context context, Notifier notifier) {
        this.notifier = notifier;

        localSavedPrefs = new LocalSavedPrefs();
        (new ImportToLocalPrefs(context, localSavedPrefs)).populateLocalPrefsFromActualInternalPrefs(); //read only
    }

    @Override
    public void run() {
        if (Helper.isNewDay(localSavedPrefs.getLastSavedTimeStamp())) { //updating daily notif on last saved ts instead
            if (BuildConfig.DEBUG) logger.info("Registered a new day when called in for daily entry check! Cranking up notification now");
            notifier.setLocalSavedPrefs(localSavedPrefs);
            notifier.fireNotification();
        }
    }
}
