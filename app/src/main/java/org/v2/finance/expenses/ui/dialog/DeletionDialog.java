package org.v2.finance.expenses.ui.dialog;

import android.app.Activity;
import android.widget.Button;
import android.widget.ListView;

import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.adapter.AbstractDeletionAdapter;
import org.v2.finance.expenses.adapter.DeletionAdapterForMainScreen;
import org.v2.finance.expenses.adapter.DeletionAdapterForViewDayRecord;
import org.v2.finance.expenses.contracts.DeletionContract;

import static org.v2.finance.expenses.util.Helper.classNameAndRespectiveEnumMap;

/**
 * @author Vivek Verma
 * @since 29-11-2019
 */
public class DeletionDialog extends AbstractDialog {

    private final DeletionContract contract;

    public DeletionDialog(Activity context, DeletionContract contract, String title, int layout) {
        super(context, title, layout);
        this.contract = contract;
    }

    @Override
    public <T> void fillUpDialog(T[] content) {
        final AbstractDeletionAdapter deletionAdapter;
        if (BuildConfig.DEBUG) logger.info("Enum for {} : {}", context.getClass().getSimpleName(), classNameAndRespectiveEnumMap.get(context.getClass().getSimpleName()));
        switch (classNameAndRespectiveEnumMap.get(context.getClass().getSimpleName())) {
            case DEL_VIEW_DAY_REC:
                deletionAdapter = new DeletionAdapterForViewDayRecord(context, R.layout.deletion_list_row_view, content);
                break;
            case DEL_MAIN_SCREEN:
                deletionAdapter = new DeletionAdapterForMainScreen(context, R.layout.deletion_list_row_view, content);
                break;
            default:
                deletionAdapter = null;
        }
        final ListView entriesToBeDeleted = view.findViewById(R.id.deletion_entries);
        entriesToBeDeleted.setAdapter(deletionAdapter);

        //handling for ok and cancel here
        final Button dialogOk = view.findViewById(R.id.delete_ok);
        dialogOk.setOnClickListener(v -> {
            contract.handleDialogOk();
            dialog.cancel();
        });
        final Button dialogCancel = view.findViewById(R.id.delete_cancel);
        dialogCancel.setOnClickListener(v -> {
            contract.handleDialogCancel();
            dialog.cancel();
        });
    }
}
