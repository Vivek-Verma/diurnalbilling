package org.v2.finance.expenses.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.notification.ApkUpdateNotificationDaemon;
import org.v2.finance.expenses.notification.Notifier;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Vivek Verma
 * @since 03-11-2019
 */
public class ApkUpdateBroadcastRx extends BroadcastReceiver {

    private final Logger logger = LoggerFactory.getLogger(ApkUpdateBroadcastRx.class);

    /**
     * Cannot put a flag / lock here for mitigating the calls for update check from MainActivity's onCreate() and the Alarm set, as the init calls happens within 1 sec (~10 ms), hence no sets show any effect
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (BuildConfig.DEBUG) logger.info("Received broadcast for apk update at '{}', context : {}, intent : {}", Calendar.getInstance().getTime(), context, intent);
        final Notifier notifier = new Notifier(context, intent);
        final ApkUpdateNotificationDaemon daemon = new ApkUpdateNotificationDaemon(notifier);
        final ExecutorService service = Executors.newSingleThreadExecutor();
        service.submit(daemon);
        service.shutdown();
    }
}
