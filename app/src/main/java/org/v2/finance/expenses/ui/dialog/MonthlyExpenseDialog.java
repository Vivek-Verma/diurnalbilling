package org.v2.finance.expenses.ui.dialog;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.v2.finance.expenses.R;
import org.v2.finance.expenses.analytics.monthly.MonthlyExpenseStats;

import java.util.Locale;

/**
 * @author Vivek Verma
 * @since 28-11-2019
 */
public class MonthlyExpenseDialog extends AbstractDialog {

    public MonthlyExpenseDialog(Activity context, String title, int layout) {
        super(context, title, layout);
    }

    @Override
    public <T> void fillUpDialog(T[] content) {
        //Creating the custom adapter to implement the list of the checkboxes...
        final ArrayAdapter<T> customAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, content);
        final ListView dialog_list_Problems = view.findViewById(R.id.listViewMonthlyExpense);
        final TextView textTotalMonthlyExpense = view.findViewById(R.id.textTotalMonthlyExpense);
        final TextView textAvgMonthlyExpense = view.findViewById(R.id.textAvgMonthlyExpense);
        final TextView textMaxMonthlyExpense = view.findViewById(R.id.textMaxMonthlyExpense);
        final TextView textMinMonthlyExpense = view.findViewById(R.id.textMinMonthlyExpense);
        final MonthlyExpenseStats monthlyExpenseStats = new MonthlyExpenseStats();
        textTotalMonthlyExpense.setText(String.format(Locale.getDefault(), "Total months : %2d", monthlyExpenseStats.getNumberOfMonths()));
        textAvgMonthlyExpense.setText(String.format(Locale.getDefault(), "Avg monthly  : %.2f", monthlyExpenseStats.getAvgMonthlyExpense()));
        textMaxMonthlyExpense.setText(String.format(Locale.getDefault(), "Max monthly : %.2f", monthlyExpenseStats.getMaxMonthlyExpense()));
        textMinMonthlyExpense.setText(String.format(Locale.getDefault(), "Min monthly  : %.2f", monthlyExpenseStats.getMinMonthlyExpense()));
        dialog_list_Problems.setAdapter(customAdapter);

        final Button buttonOkMonthlyExpense = view.findViewById(R.id.buttonOk_MonthlyExpense);
        buttonOkMonthlyExpense.setOnClickListener(buttonView -> dialog.cancel());
    }
}
