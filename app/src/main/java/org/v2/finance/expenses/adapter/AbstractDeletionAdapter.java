package org.v2.finance.expenses.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.v2.finance.expenses.util.ModelForDeletion;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-06-2019
 * @since 17-06-2019
 */
public abstract class AbstractDeletionAdapter extends ArrayAdapter<ModelForDeletion> {
    protected final Context context;
    protected final ModelForDeletion[] modelItems;
    protected final int layoutToBeFollowed;

    public <T> AbstractDeletionAdapter(Context context, int layoutToBeFollowed, T[] resource) {
        super(context, layoutToBeFollowed, (ModelForDeletion[]) resource);

        this.context = context;
        this.layoutToBeFollowed = layoutToBeFollowed;
        this.modelItems = (ModelForDeletion[]) resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
