package org.v2.finance.expenses.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.Editable;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.MainActivity;
import org.v2.finance.expenses.ads.BannerAd;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.core.VersionInfo;
import org.v2.finance.expenses.core.settings.sub.Payment;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.receiver.ApkUpdateBroadcastRx;
import org.v2.finance.expenses.receiver.DailyEntryBroadcastRx;
import org.v2.finance.expenses.transporters.TransportBillingAmount;
import org.v2.finance.expenses.transporters.TransportEditText;
import org.v2.finance.expenses.ui.ViewDayRecord;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static org.v2.finance.expenses.constants.Constants.BILLING_DIRECTORY_NAME;
import static org.v2.finance.expenses.constants.Constants.BILLING_FILE_NAME_PREFIX;
import static org.v2.finance.expenses.constants.Constants.CallersForDeletion.DEL_MAIN_SCREEN;
import static org.v2.finance.expenses.constants.Constants.CallersForDeletion.DEL_VIEW_DAY_REC;
import static org.v2.finance.expenses.constants.Constants.DAILY_ENTRY_PROMPT_HOUR;
import static org.v2.finance.expenses.constants.Constants.DAILY_ENTRY_PROMPT_MINUTES;
import static org.v2.finance.expenses.constants.Constants.DAILY_ENTRY_PROMPT_SECONDS;
import static org.v2.finance.expenses.constants.Constants.DATE_SPLITTER_CHAR;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_DAY_OF_WEEK;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_INT_VALUE;
import static org.v2.finance.expenses.constants.Constants.FILE_HEADER_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.FILE_PATH_PREFIX_INTERNAL_STORAGE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DATE_PICKER_DISPLAY_DATE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_DATE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_FILE_TITLE_DATE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_GRAND_TOTAL_DISPLAY_DATE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_INTERNAL_FILE_HEADER;
import static org.v2.finance.expenses.constants.Constants.FORMAT_INTERNAL_FOLDER_PATH;
import static org.v2.finance.expenses.constants.Constants.FORMAT_MONTHLY_EXPENSE_KEY_DIALOG;
import static org.v2.finance.expenses.constants.Constants.FORMAT_MONTHLY_EXPENSE_KEY_INTERNAL;
import static org.v2.finance.expenses.constants.Constants.GDRIVE_SITE_FORMAT;
import static org.v2.finance.expenses.constants.Constants.GITHUB_SITE_FORMAT;
import static org.v2.finance.expenses.constants.Constants.INTERNAL_FOLDER;
import static org.v2.finance.expenses.constants.Constants.REGEX_HYPHEN_DATE_SPLITTER;
import static org.v2.finance.expenses.constants.Constants.REGEX_SPACE;
import static org.v2.finance.expenses.constants.Constants.REPEAT_INTERVAL_APK_CHECK;
import static org.v2.finance.expenses.constants.Constants.monthNames;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 23-10-2018
 */
public class Helper {

    private final static Logger logger = LoggerFactory.getLogger(Helper.class);

    public static final SimpleDateFormat SDF_DISPLAY_DATE = new SimpleDateFormat(FORMAT_DISPLAY_DATE);
    public static final SimpleDateFormat SDF_DAY_OF_WEEK_ENTIRE = new SimpleDateFormat("EEEE");
    public static final SimpleDateFormat SDF_DAY_OF_WEEK = new SimpleDateFormat("EEE");
    public static final DecimalFormat decimalFormat = new DecimalFormat("#.00");

    private static final Random randomNumberGenerator = new Random();

    public static ScheduledExecutorService scheduledInsttAdExecutorService = Executors.newSingleThreadScheduledExecutor();

    public static void jumpStartScheduledExecutorService() {
        scheduledInsttAdExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public static Integer layOverIndexFromMain = DEFAULT_INT_VALUE;
    public static Entry layOverEntry;
    public static EntryOfADay layOverEntryOfADay;
    public static Boolean isDayRecordEditable = Boolean.FALSE;
    public static Boolean areAdsToBeDisplayed = true;
    public static Boolean isPremiumUser = false;
    public static UserMappingProto.Currency userCurrency = UserMappingProto.Currency.INR;
    public static int ROUTING_PATH;
    public static AtomicBoolean PERFORM_HEAVY_IMPORTING = new AtomicBoolean(false);
    public static boolean isSaveDoneAtleastOnce = false;
    public static Map<String, EntryOfADay> mapOfDateAndDailyEntries = new HashMap<>(); //map of date dd-MM-yyyy and EntryOfADay
    public static Map<String, String> mapOfDateAndDataOfDailyEntries = new HashMap<>(); //map of date and the entire string data of it's respective EntryOfADay
    public static Map<String, String> mapOfDisplayDateAndFileTitleDate; //map of dd-MM-yyyy and it's corresponding yyyy_MM_dd
    public static Map<String, String> mapOfDisplayDateAndDayOfWeek; //map of dd-MM-yyyy and it's corresponding day of week in upper case 'WED'
    public static final Map<Integer, Boolean> mapOfSavesVsAdShown = new HashMap<>();
    public static String[] monthlyExpenseList;
    public static TreeMap<Integer, String> tempMapOfIndexAndDate;

    public static final List<VersionInfo> centralChangeLogs = new ArrayList<>();
    public static final Map<String, Constants.CallersForDeletion> classNameAndRespectiveEnumMap = new HashMap<>();
    public static Payment payment = null;

    static {
        classNameAndRespectiveEnumMap.put(ViewDayRecord.class.getSimpleName(), DEL_VIEW_DAY_REC);
        classNameAndRespectiveEnumMap.put(MainActivity.class.getSimpleName(), DEL_MAIN_SCREEN);
    }

    public static int numberOfSavesPerformed = 0;

    public static final AtomicBoolean wasAdNotDisplayedOnTime = new AtomicBoolean(false);

    public static void fireUpBannerAd(Activity context, int id) {
        if (Helper.areAdsToBeDisplayed) {
            final BannerAd bannerAd = new BannerAd(context, context.findViewById(id));
            bannerAd.assignAd();
        }
    }

    public static Integer getIndexFromDateForList(String date, Map<String, Integer> reverseMapOfIndexAndDate) {
        Integer requiredIndex = null;
        if (reverseMapOfIndexAndDate != null) requiredIndex = reverseMapOfIndexAndDate.get(date);
        if (BuildConfig.DEBUG) logger.info("Shouting from forwarding the click on day rec launch, date being: {}, index being : {}", date, requiredIndex);
        return requiredIndex;
    }

    public static void incrementNumberOfSavesPerformed() {
        numberOfSavesPerformed++;
    }

    public static String updateMapOfDisplayDateAndDayOfWeek(String date) {
        final String dayOfWeek = convertDisplayDateToDayOfWeek(date);
        mapOfDisplayDateAndDayOfWeek.put(date, dayOfWeek);
        return dayOfWeek;
    }

    public static void handleChangeLogUpdate(List<VersionInfo> logs) {
        if (logs != null) {
            centralChangeLogs.clear();
            centralChangeLogs.addAll(logs);
        } else {
            if (BuildConfig.DEBUG) logger.warn("Failed to update the ChangeLogs on obtaining data from server");
        }
    }

    public static void handleRemoteSettingsUpdate(Payment payment) {
        if (payment != null) {
            Helper.payment = payment;
        } else {
            if (BuildConfig.DEBUG) logger.warn("Failed to update the Settings' payment wing on obtaining data from server");
        }
    }

    public static String gDriveLinkGenerator(String id) {
        return String.format(GDRIVE_SITE_FORMAT, id);
    }

    public static String gitHubLinkGenerator(String uri) {
        return String.format(GITHUB_SITE_FORMAT, uri);
    }

    public static String obtainDayOfWeek(String entryDate) {
        try {
            Date date = SDF_DISPLAY_DATE.parse(entryDate);
            return obtainDayOfWeek(date);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) logger.error("Failed to parse the incoming entry date of '{}', will display nothing!", entryDate);
            return DEFAULT_DAY_OF_WEEK;
        }
    }

    public static String getMonthNumberFromName(String name) {
        int i = 1;
        for (; i < monthNames.length; i++) if (monthNames[i].equals(name)) break;
        return String.format("%02d", i);
    }

    public static String obtainDayOfWeek(Date date) {
        return SDF_DAY_OF_WEEK.format(date).toUpperCase();
    }

    public static String[] splitStringOnDelim(String data) {
        return splitStringOnDelim(data, REGEX_HYPHEN_DATE_SPLITTER);
    }

    public static String[] splitStringOnDelim(String data, String regex) {
        String[] dataArr = data.split(regex);
        for (int i = 0; i < dataArr.length; i++) dataArr[i] = dataArr[i].trim();
        return dataArr;
    }

    /**
     * Coverts the input date from dd-MM-yyyy to yyyy_MM_dd, as that is used in the file title
     *
     * @param displayDate
     * @return yyyy_MM_dd
     */
    public static String convertDisplayDateToFileTitleDate(String displayDate) {
        String[] displayDateArr = splitStringOnDelim(displayDate);
        return String.format(FORMAT_FILE_TITLE_DATE, displayDateArr[2], displayDateArr[1], displayDateArr[0]);
    }

    /**
     * Checks whether the date being sent in from the backup text file is in the format of dd-MM-yyyy.
     * If the format is not correct i.e. d-MM-yyyy, then convert it to the correct one and then return the yyyy_MM_dd
     *
     * @param date
     * @return yyyy_MM_dd date
     */
    public static String returnFormattedDateForImport(String date) {
        String[] dateArr = splitStringOnDelim(date);
        if (dateArr[0].length() == 1) { //the length of dateArr[0] will always be either 1 or 2
            date = String.format("0%s-%s-%s", dateArr[0], dateArr[1], dateArr[2]);
            if (BuildConfig.DEBUG) logger.info("The formatted name from the returnFormattedDateForImport : {}", date);
        }
        return convertDisplayDateToFileTitleDate(date); //convert the dd-MM-yyyy to yyyy_MM_dd for the file title
    }

    public static String convertDisplayDateToDayOfWeek(String date) {
        return obtainDayOfWeek(convertDisplayDateToActualDate(date));
    }

    public static Date convertDisplayDateToActualDate(String date) {
        try {
            return SDF_DISPLAY_DATE.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static boolean isDayWeekend(String code) {
        return "SAT".equals(code) || "SUN".equals(code);
    }

    /**
     * Returns key from date for month level
     *
     * @param date dd-mm-yyyy
     * @return yyyy-mm
     */
    public static String convertDateForMonthlyExpenseMapKey(String date) {
        date = date.substring(date.indexOf(DATE_SPLITTER_CHAR) + 1); //dd-mm-yyyy to mm-yyyy
        int hyphenIndex = date.indexOf(DATE_SPLITTER_CHAR);
        return String.format(FORMAT_MONTHLY_EXPENSE_KEY_INTERNAL, date.substring(hyphenIndex + 1), date.substring(0, hyphenIndex));
    }

    /**
     * @return current date in display date string format of dd-MM-yyyy
     */
    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return String.format(Locale.getDefault(), FORMAT_DATE_PICKER_DISPLAY_DATE, calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(YEAR));
    }

    public static void setFilePathPrefixInternalStorage(String filePathPrefixInternalStorage) {
        FILE_PATH_PREFIX_INTERNAL_STORAGE = filePathPrefixInternalStorage;
        FILE_HEADER_INTERNAL_FILE = String.format(FORMAT_INTERNAL_FILE_HEADER, FILE_PATH_PREFIX_INTERNAL_STORAGE, BILLING_DIRECTORY_NAME, BILLING_FILE_NAME_PREFIX);
        INTERNAL_FOLDER = String.format(FORMAT_INTERNAL_FOLDER_PATH, FILE_PATH_PREFIX_INTERNAL_STORAGE, BILLING_DIRECTORY_NAME);
    }

    public static String getFilePathPrefixInternalStorage() {
        return FILE_PATH_PREFIX_INTERNAL_STORAGE;
    }

    public static int getRoutingPath() {
        return ROUTING_PATH;
    }

    public static boolean performHeavyImporting() {
        return PERFORM_HEAVY_IMPORTING.get();
    }

    public static void activateHeavyImportingFlag() {
        PERFORM_HEAVY_IMPORTING.set(true);
        isSaveDoneAtleastOnce = true;
    }

    public static void deactivateHeavyImportingFlag() {
        PERFORM_HEAVY_IMPORTING.set(false);
    }

    public static Integer getLayOverIndexFromMain() {
        return layOverIndexFromMain;
    }

    public static Entry getLayOverEntry() {
        return layOverEntry;
    }

    public static EntryOfADay getLayOverEntryOfADay() {
        return layOverEntryOfADay;
    }

    public static List<String> extractValuesFromMapInOrder(TreeMap<Integer, String> map) {
        List<String> list = new ArrayList<>();
        for (Integer index : map.descendingMap().keySet()) list.add(map.get(index));
        return list;
    }

    public static List<EntryOfADay> extractEOADInOrder(TreeMap<Integer, String> mapOfIndexAndDate) {
        List<EntryOfADay> list = new ArrayList<>();
        for (Integer index : mapOfIndexAndDate.keySet()) {
            list.add(mapOfDateAndDailyEntries.get(mapOfIndexAndDate.get(index)));
        }
        return list;
    }

    public static List<EntryOfADay> extractEOADInOrder(Map<Integer, String> mapOfIndexAndDate) {
        List<EntryOfADay> list = new ArrayList<>();
        for (Integer index : mapOfIndexAndDate.keySet()) {
            list.add(mapOfDateAndDailyEntries.get(mapOfIndexAndDate.get(index)));
        }
        return list;
    }

    public static TransportEditText validateText(Editable editText) {
        if (editText == null) return new TransportEditText(false);

        String data = editText.toString().trim();
        if (data.isEmpty()) return new TransportEditText(false);
        return new TransportEditText(true, data);
    }

    public static Double stringToDoubleConverter(String data) {
        if (data == null || data.isEmpty()) return Double.NaN;
        return Double.parseDouble(data.trim());
    }

    public static Integer stringToIntegerConverter(String data) {
        if (data == null || data.isEmpty()) return Integer.MIN_VALUE;
        return Integer.parseInt(data.trim());
    }

    public static Long stringToLongConverter(String data) {
        if (data == null || data.isEmpty()) return Long.MIN_VALUE;
        return Long.parseLong(data.trim());
    }

    public static String doubleToStringConverter(Double data) {
        return Double.toString(data);
    }

    public static Long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static int getRandomValue(int upperLimit) {
        return randomNumberGenerator.nextInt(upperLimit);
    }

    /**
     * Takes in the existing timestamp from the internal saved preferences, and compares it against the current timestamp
     *
     * @param existingTimestamp time to be supplied in milliseconds
     * @return true if both the timestamp of different dates
     */
    public static boolean isNewDay(long existingTimestamp) {
        Calendar cal1 = Calendar.getInstance(); //current
        Calendar cal2 = Calendar.getInstance(); //to be compared against
        cal2.setTimeInMillis(existingTimestamp);
        return !(cal1.get(YEAR) == cal1.get(YEAR) && cal1.get(MONTH) == cal2.get(MONTH) && cal1.get(DAY_OF_MONTH) == cal2.get(DAY_OF_MONTH));
    }

    public static boolean isServiceRunning(Context context, Class serviceClass) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        try {
            final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
            for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
                logger.info("Service: {}", runningServiceInfo.service.getClassName());
                if (runningServiceInfo.service.getClassName().equals(serviceClass.getName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            logger.error("Failed to retrieve the list of running services due to : ", e);
        }
        return false;
    }

    public static boolean isAlarmActiveForApkCheck(Context context) {
        return obtainPIforExistingApkCheckAlarm(context) != null;
    }

    public static boolean isAlarmActiveForDailyEntryCheck(Context context) {
        return obtainPIforExistingDailyEntryCheckAlarm(context) != null;
    }

    public static Intent obtainApkUpdateBroadCastRxIntent(Context context) {
        final Intent intent = new Intent(context, ApkUpdateBroadcastRx.class);
        intent.setAction(String.valueOf(Constants.NotificationCaller.APK_UPDATE_CHECK));
        return intent;
    }

    public static Intent obtainDailyEntryBroadCastRxIntent(Context context) {
        final Intent intent = new Intent(context, DailyEntryBroadcastRx.class);
        intent.setAction(String.valueOf(Constants.NotificationCaller.DAILY_ENTRY_CHECK));
        return intent;
    }

    public static AlarmManager obtainAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public static PendingIntent obtainPIforNewApkCheckAlarm(Context context) {
        return extractPendingIntent(context, obtainApkUpdateBroadCastRxIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent obtainPIforExistingApkCheckAlarm(Context context) {
        return extractPendingIntent(context, obtainApkUpdateBroadCastRxIntent(context), PendingIntent.FLAG_NO_CREATE);
    }

    public static PendingIntent obtainPIforNewDailyEntryCheckAlarm(Context context) {
        return extractPendingIntent(context, obtainDailyEntryBroadCastRxIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent obtainPIforExistingDailyEntryCheckAlarm(Context context) {
        return extractPendingIntent(context, obtainDailyEntryBroadCastRxIntent(context), PendingIntent.FLAG_NO_CREATE);
    }

    private static PendingIntent extractPendingIntent(Context context, Intent intent, int flag) {
        return PendingIntent.getBroadcast(context, 0, intent, flag);
    }

    private static PendingIntent obtainPIForCancellingApkCheckAlarm(Context context) {
        return obtainPIforNewApkCheckAlarm(context); //for now pending intent, cancelling is the same as creation
    }

    public static void createAndFireApkCheckAlarm(Context context) {
        /*final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, START_HOUR_OF_DAY_APK_CHECK);
        calendar.set(Calendar.MINUTE, getNoiseForMinute());
        calendar.set(Calendar.SECOND, 0);*/

        if (BuildConfig.DEBUG) logger.info("Schedulin the start of apk notif daemon at '{}'", Calendar.getInstance().getTime());
        final AlarmManager alarm = obtainAlarmManager(context);
        //alarm.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), REPEAT_INTERVAL_APK_CHECK, obtainPIforNewApkCheckAlarm(context));
        alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), REPEAT_INTERVAL_APK_CHECK, obtainPIforNewApkCheckAlarm(context));
    }

    public static void createAndFireDailyEntryAlarm(Context context) {
        if (BuildConfig.DEBUG) logger.info("Schedulin the single hit of daily entry notif daemon at '{}'", Calendar.getInstance().getTime());
        final AlarmManager alarm = obtainAlarmManager(context);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, DAILY_ENTRY_PROMPT_HOUR);
        calendar.set(Calendar.MINUTE, DAILY_ENTRY_PROMPT_MINUTES);
        calendar.set(Calendar.SECOND, DAILY_ENTRY_PROMPT_SECONDS);
        long destinationTimeStamp = calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY; //setting for next day at scheduled time
        if (BuildConfig.DEBUG) logger.info("Timer set for {}", (new Date((destinationTimeStamp))));

        alarm.setExact(AlarmManager.RTC_WAKEUP, destinationTimeStamp, obtainPIforNewDailyEntryCheckAlarm(context));
        //to be set on each hit from the broadcast receiver
    }

    private static int getNoiseForMinute() {
        return (new Random()).nextInt(50);
    }

    public static void searchAndKillApkCheckAlarm(Context context) {
        if (isAlarmActiveForApkCheck(context)) {
            try {
                final AlarmManager alarm = obtainAlarmManager(context);
                alarm.cancel(obtainPIForCancellingApkCheckAlarm(context));
                if (BuildConfig.DEBUG) logger.info("Cancelled the alarm!");
            } catch (Exception e) {
                if (BuildConfig.DEBUG) logger.error("Cannot cancel the alarm! Err : ", e);
            }
        } else {
            if (BuildConfig.DEBUG) logger.info("No alarm running, hence no cancellation performed!");
        }
    }

    public static TransportBillingAmount crackUpTheMonthRow(String monthRow) {
        String[] monthRowArr = Helper.splitStringOnDelim(monthRow, REGEX_SPACE); //<bullet>. mmm 'yy : sign currency amount
        return crackUpTheMonthRow(monthRowArr);
    }

    public static TransportBillingAmount crackUpTheMonthRow(String[] monthRow) {
        return new TransportBillingAmount(monthRow[4], monthRow[5], stringToDoubleConverter(monthRow[6]));
    }

    public static String renderDateToReadableDate(String date) {
        String[] components = splitStringOnDelim(date);
        try {
            if (components.length == 3) //20-11-2019
                return String.format(FORMAT_GRAND_TOTAL_DISPLAY_DATE, components[0], monthNames[Integer.parseInt(components[1])], components[2].substring(2));
            else if (components.length == 2) //2019-11
                return String.format(FORMAT_MONTHLY_EXPENSE_KEY_DIALOG, monthNames[Integer.parseInt(components[1])], components[0].substring(2));
        } catch (Exception e) {
            if (BuildConfig.DEBUG) logger.error("Failed to render date '{}', err : ", date, e);
        }
        return date;
    }

    public static void writeBackUserInfoToCache(UserMappingProto.UserMapping retrievedUserMapping, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache) {
        localSavedPrefs.setUserName(retrievedUserMapping.getUsername());
        localSavedPrefs.setPremiumUserStatus(retrievedUserMapping.getPremiumUser());
        localSavedPrefs.setUserMobile(retrievedUserMapping.getMobile());
        localSavedPrefs.setUserEmail(retrievedUserMapping.getEmail());
        localSavedPrefs.setUserCredHash(retrievedUserMapping.getHashCred());
        localSavedPrefs.setCurrency(retrievedUserMapping.getCurrency());
        localSavedPrefs.setPaymentExpiryTimestamp(retrievedUserMapping.getPaymentExpiryTimestamp());
        localSavedPrefs.setLastCloudUploadTimeStamp(retrievedUserMapping.getLastCloudSaveTimestamp());

        exportToInternalCache.writeUserName();
        exportToInternalCache.writePremiumUserStatus();
        exportToInternalCache.writeUserMobile();
        exportToInternalCache.writeUserEmail();
        exportToInternalCache.writeUserCredHash();
        exportToInternalCache.writeCurrency();
        exportToInternalCache.writePaymentExpiryTime();
        exportToInternalCache.writeLastCloudUploadTime();
    }

    public static Boolean isPremiumUser() {
        return isPremiumUser;
    }

    public static void setIsPremiumUser(Boolean isPremiumUser) {
        Helper.isPremiumUser = isPremiumUser;
    }

    public static void implementPostPaymentExpirySteps(Context context) {
        ToasterUtil.longToaster(context, "Payment expired! Falling back to non-premium status");
        setIsPremiumUser(false);
    }

    public static UserMappingProto.Currency getUserCurrency() {
        return userCurrency;
    }

    public static void setUserCurrency(UserMappingProto.Currency userCurrency) {
        Helper.userCurrency = userCurrency;
    }
}
