package org.v2.finance.expenses.ads;

import android.app.Activity;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.util.Helper;

import java.util.concurrent.TimeUnit;

import static org.v2.finance.expenses.constants.Constants.ZERO_INT;
import static org.v2.finance.expenses.util.Helper.scheduledInsttAdExecutorService;

/**
 * @author Vivek Verma
 * @since 08-09-2019
 */
public class InterstitialAd extends Ad {

    private final Logger logger = LoggerFactory.getLogger(InterstitialAd.class);

    private final PublisherInterstitialAd interstitialAd;
    private final int SCHEDULED_POLLING;

    public InterstitialAd(Activity context, String adUnitId, int pollingTime) {
        super(context);
        this.SCHEDULED_POLLING = pollingTime;

        interstitialAd = new PublisherInterstitialAd(context);
        interstitialAd.setAdUnitId(adUnitId);
        if (BuildConfig.DEBUG) logger.info("The ad unit set for interstitial ad : {}", interstitialAd.getAdUnitId());
    }

    public PublisherInterstitialAd getInterstitialAd() {
        return interstitialAd;
    }

    @Override
    public void assignAd() {
        if (scheduledInsttAdExecutorService.isShutdown() || scheduledInsttAdExecutorService.isTerminated()) {
            if (BuildConfig.DEBUG) logger.warn("The scheduler is offline... jump starting it");
            Helper.jumpStartScheduledExecutorService();
        }
        scheduledInsttAdExecutorService.scheduleAtFixedRate(adLoadTask(), ZERO_INT, SCHEDULED_POLLING, TimeUnit.SECONDS);
    }

    private Runnable adLoadTask() {
        //the problematic part
        return () -> context.runOnUiThread(() -> {
            if (interstitialAd.isLoaded()) {
                if (BuildConfig.DEBUG) logger.info("*** INTERSTITIAL AD is READY ***");
                //Helper.shortToaster(context, "Ad READY :)");
            } else {
                if (BuildConfig.DEBUG) logger.warn("INTERSTITIAL AD still NOT loaded!!");
                //Helper.shortToaster(context, "Ad not ready yet :(");
                assignInterstitialAd();
            }
        });
    }

    private void assignInterstitialAd() {
        interstitialAd.loadAd(getInterstitialAdRequest());
    }

    private PublisherAdRequest getInterstitialAdRequest() {
        if (BuildConfig.DEBUG) logger.info("Requesting Interstitial Ad now!!");
        return new PublisherAdRequest.Builder()
                //.addTestDevice("EF6225FB87BD949F87A70CB1E5F23B4A")
                .build();
    }

}
