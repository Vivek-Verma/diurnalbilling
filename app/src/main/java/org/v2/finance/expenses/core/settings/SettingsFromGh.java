package org.v2.finance.expenses.core.settings;

import com.google.gson.Gson;

import org.v2.finance.expenses.core.settings.sub.Payment;

/**
 * @author Vivek Verma
 * @since 4/5/21
 */
public class SettingsFromGh {

    private Payment payment;

    public SettingsFromGh() {
    }

    public SettingsFromGh(Payment payment) {
        this.payment = payment;
    }

    public static SettingsFromGh populatePaymentDetails(String json) {
        Gson gson = new Gson();
        SettingsFromGh settingsFromGH = gson.fromJson(json, SettingsFromGh.class);
        return settingsFromGH;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SettingsFromGh{");
        sb.append("payment=").append(payment);
        sb.append('}');
        return sb.toString();
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}