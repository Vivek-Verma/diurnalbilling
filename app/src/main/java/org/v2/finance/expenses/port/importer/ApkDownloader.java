package org.v2.finance.expenses.port.importer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.util.Helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static org.v2.finance.expenses.constants.Constants.ESTIMATE_DOWNLOAD_SIZE;

/**
 * @author Vivek Verma
 * @since 1/6/21
 */
public class ApkDownloader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApkDownloader.class);

    public static boolean downloadLatestApk(String uri, ApkDownloadSource source) {
        String finalUrl = "";
        switch (source) {
            case GDRIVE:
                finalUrl = Helper.gDriveLinkGenerator(uri);
                break;
            case GITHUB:
                finalUrl = Helper.gitHubLinkGenerator(uri);
                break;
        }
        //LOGGER.info("Final URL: {}", finalUrl);
        System.out.println("Final URL: " + finalUrl);

        try {
            URL url = new URL(finalUrl);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setReadTimeout(30000);
            httpsURLConnection.setConnectTimeout(30000);
            httpsURLConnection.connect();

            if (httpsURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpsURLConnection.getInputStream();
                File tmpApk = new File("/tmp/DailyEBilling.apk");
                OutputStream outputStream = new FileOutputStream(tmpApk);

                byte[] buffer = new byte[8 * 1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                    if (BuildConfig.DEBUG) System.out.println("Tmp apk size : " + tmpApk.length());
                    Thread.sleep(10);
                }
                outputStream.flush();
                inputStream.close();
                outputStream.close();
                httpsURLConnection.disconnect();
                return !(tmpApk.length() < ESTIMATE_DOWNLOAD_SIZE * .80);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Error while downloading APK. " + e);
        }
        return false;
    }

}
