package org.v2.finance.expenses.port.importer;

import android.content.Context;

import com.vv.personal.diurnal.artifactory.generated.DataTransitProto;
import com.vv.personal.diurnal.artifactory.generated.ResponsePrimitiveProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.contracts.CallBackWithUserMappingContract;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.port.export.Export;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.FILE_HEADER_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.INTERNAL_FOLDER;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;
import static org.v2.finance.expenses.util.Helper.returnFormattedDateForImport;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-11-2018
 * @since 17-11-2018
 */
public class EmergencyBackupRestore implements CallBackWithUserMappingContract {

    private final Logger LOGGER = LoggerFactory.getLogger(EmergencyBackupRestore.class);

    private final Context context;
    private final String backUpFilePath;
    private final EmergencyRestoreEnum emergencyRestoreEnum;
    private final LocalSavedPrefs localSavedPrefs;
    private final ExportToInternalPrefs exportToInternalCache;
    private PropertyReader properties = null;

    public EmergencyBackupRestore(Context context, String backUpFilePath, EmergencyRestoreEnum emergencyRestoreEnum, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache) {
        this.context = context;
        this.backUpFilePath = backUpFilePath;
        this.emergencyRestoreEnum = emergencyRestoreEnum;
        this.localSavedPrefs = localSavedPrefs;
        this.exportToInternalCache = exportToInternalCache;
    }

    public void fireBackUpRestore() {
        //TODO -- 20210427 - might have to wipe out all the files from the internal storage before restoring, but that's risky as backup might fail, causing possible data loss...
        switch (emergencyRestoreEnum) {
            case LOCAL:
                fireLocalBackUpRestore();
                break;
            case CLOUD:
                fireCloudBackUpRestore();
                break;
        }
    }

    public void fireLocalBackUpRestore() {
        LOGGER.info("Back up file path -- {}", backUpFilePath);
        File file = new File(backUpFilePath);
        LOGGER.info("The back up file exist status : " + file.exists());
        if (file.exists()) {
            Queue<String> backUpFileLines = new LinkedList<>();
            try (BufferedReader in = new BufferedReader(new FileReader(file))) {
                String line;
                int lineNo = 0;
                final File internalTargetFolder = new File(INTERNAL_FOLDER);
                if (!internalTargetFolder.exists() && !internalTargetFolder.mkdir()) {
                    ToasterUtil.shortToaster(context, "Restoration via back up failed!");
                    return;
                }
                while ((line = in.readLine()) != null) {
                    backUpFileLines.add(line);
                    lineNo++;
                }
                in.close();
                if (lineNo > 0) {
                    performActualBackUpRestore(backUpFileLines);
                }
            } catch (IOException e) {
                LOGGER.error("Error encountered whilst reading in the backup file. Error : ", e);
                ToasterUtil.longToaster(context, "Either no backup file / No user permission for file system reading.");
            }
        } else {
            ToasterUtil.shortToaster(context, "BackUp file not found! Restore failed!!");
        }
    }

    public void fireCloudBackUpRestore() {
        properties = new PropertyReader(context, context.getString(R.string.diurnal_properties));

        AsyncUserInfoPull asyncSignIn = new AsyncUserInfoPull(context, localSavedPrefs, exportToInternalCache, this, localSavedPrefs.getUserEmail(),
                properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN),
                properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT, 90L),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
        );
        asyncSignIn.execute(); //driving point passed onto Async
    }

    public void performActualBackUpRestore(Queue<String> backUpLines) {
        StringBuilder data = new StringBuilder();
        String fileTitle = EMPTY_STR;
        int lineNo = 0;
        if (BuildConfig.DEBUG) LOGGER.info("Rx-ed {} lines of backup data to restore", backUpLines.size());
        while (!backUpLines.isEmpty()) {
            String line = backUpLines.poll().trim();
            if (BuildConfig.DEBUG) LOGGER.info("Line {} -- {}", lineNo, line);
            if (line.contains("::")) {
                String date = line.substring(0, line.indexOf(' '));
                date = returnFormattedDateForImport(date);
                if (BuildConfig.DEBUG) LOGGER.info("\tDate --> {}", date);

                fileTitle = String.format(FORMAT_INTERNAL_FILE, FILE_HEADER_INTERNAL_FILE, date);
                if (BuildConfig.DEBUG) LOGGER.info("file title being used for export : " + fileTitle);
                data = new StringBuilder().append(line).append(NEW_LINE);
            } else if (line.length() == 0 || NEW_LINE.equals(line)) {
                if (BuildConfig.DEBUG) LOGGER.info("file title being used for export from new line : " + fileTitle);
                new Export(fileTitle, data.toString()).export(); //fires creation of new file inside the device
            } else {
                data.append(line).append(NEW_LINE);
            }
            lineNo++;
        }
        if (lineNo > 0) {
            if (BuildConfig.DEBUG) LOGGER.info("OUTER FILE FOR INNER WRITING - path : {}", fileTitle); //for the last in the external file
            new Export(fileTitle, data.toString()).export();

            ToasterUtil.longToaster(context, "Restarting app!");
            LOGGER.info("Restarting app after new emergency restoring...");
            new ActivityStarter(context).fireUpMainActivity();
        }
        //no need of adding an extra break line at the end. that has been handled accordingly.
    }

    @Override
    public void callBack(UserMappingProto.UserMapping retrievedUserMapping) {
        if (TimingUtil.hasTimestampExpired(retrievedUserMapping.getPaymentExpiryTimestamp())) {
            Helper.implementPostPaymentExpirySteps(context);
            return;
        }

        AsyncEmergencyCloudRestore asyncEmergencyCloudRestore = new AsyncEmergencyCloudRestore(context, localSavedPrefs.getUserEmail(),
                properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE),
                properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE_TIMEOUT, 90L),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
        );
        asyncEmergencyCloudRestore.execute();
    }

    private class AsyncEmergencyCloudRestore extends AsyncSignInUpModule {
        private final DataTransitProto.DataTransit dataTransit;
        private final PingServerWithData<DataTransitProto.DataTransit> pingServer;
        private Queue<String> backUpQueue = new LinkedList<>();

        public AsyncEmergencyCloudRestore(Context context, String email,
                                          String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);

            if (BuildConfig.DEBUG) LOGGER.info("AsyncEmergencyCloudRestore isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
            dataTransit = DataTransitProto.DataTransit.newBuilder().setEmail(email).build();
            ToasterUtil.shortToaster(context, "Starting restore from cloud backup");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            try {
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, dataTransit));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                List<String> rawBackUpList = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getResponsesList();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), rawBackUpList);
                packageResponseDetail.close();
                Boolean isBackUpReceived = !rawBackUpList.isEmpty();
                if (isBackUpReceived) {
                    backUpQueue = new LinkedList<>(rawBackUpList);
                    rawBackUpList = null;
                }
                return isBackUpReceived;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact cloud-backup-restore module for DBI. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while cloud-backup-restore DBI. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean isBackUpPresent) {
            if (isBackUpPresent == null) {
                ToasterUtil.longToaster(context, "Server unreachable for restore! Check internet connection OR contact developer.");
                return;
            }
            if (isBackUpPresent) {
                ToasterUtil.shortToaster(context, "Backup received, processing now.");
                performActualBackUpRestore(backUpQueue);
            } else {
                ToasterUtil.longToaster(context, "Cloud backup not found!");
            }
        }
    }
}


