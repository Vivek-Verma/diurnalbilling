package org.v2.finance.expenses.connectivity;

import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;

/**
 * @author Vivek Verma
 * @since 4/5/21
 */
public class SettingsPinger extends AbstractGDriveDataImporter {

    public SettingsPinger(GDriveConnector connector, String id) {
        super(id, connector, GDriveLocators.SETTINGS);
    }

}
