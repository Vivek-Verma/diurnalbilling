package org.v2.finance.expenses.ui.signinup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.auth.AuthUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.connectivity.TestConnectivity;
import org.v2.finance.expenses.contracts.CallBackWithUserMappingContract;
import org.v2.finance.expenses.contracts.TestConnectivityConnector;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.importer.AsyncUserInfoPull;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.util.SignInUpUtil;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_HOST_FOR_PINGING;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.USER_SIGN_IN_EXPIRED;

/**
 * @author Vivek Verma
 * @since 14/3/21
 */
public class SignIn extends AppCompatActivity implements View.OnClickListener, TestConnectivityConnector, CallBackWithUserMappingContract {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignIn.class);

    private final Context context = this;

    private EditText inputSignInEmailAddress;
    private EditText inputSignInPassword;
    private Button buttonSignInProceed;
    private TextView signInPromptSignUpText;

    private LocalSavedPrefs localSavedPrefs = null;
    private ExportToInternalPrefs exportToInternalCache = null;
    private ActivityStarter activityStarter;
    private PropertyReader properties;

    private String signInUserCred = EMPTY_STR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityStarter = new ActivityStarter(context);
        try {
            localSavedPrefs = (LocalSavedPrefs) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
            boolean runPopulate = false;
            if (localSavedPrefs == null) { // if MainActivity not launched from BAU flow
                localSavedPrefs = new LocalSavedPrefs();
                runPopulate = true;
            }
            properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
            ImportToLocalPrefs importToLocalPrefs = new ImportToLocalPrefs(context, localSavedPrefs);
            exportToInternalCache = new ExportToInternalPrefs(context, localSavedPrefs);
            if (runPopulate) { // if launched from SignUpOtp - needs to be updated
                importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();
            } else if (BuildConfig.DEBUG) {
                LOGGER.info("De-serialized saved preferences from SignupOtp => {}", localSavedPrefs.getLastUsedTimeStamp());
            }

            if (localSavedPrefs.getUserCredHashExpiryTimestamp() != USER_SIGN_IN_EXPIRED) {
                activityStarter.fireUpMainActivity(localSavedPrefs, properties);
            } else {
                ToasterUtil.longToaster(context, "Sign in expired. Enter login details.");
            }
            generateTestConn().execute();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) LOGGER.error("Couldn't de-serialize data in SignIn => ", e);
        }

        setContentView(R.layout.activity_signin);
        inputSignInEmailAddress = findViewById(R.id.inputSignInEmailAddress);
        inputSignInPassword = findViewById(R.id.inputSignInPassword);
        buttonSignInProceed = findViewById(R.id.buttonSignUpProceed);
        signInPromptSignUpText = findViewById(R.id.signInPromptSignUpText);

        buttonSignInProceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSignInProceed) {
            if (!SignInUpUtil.verifyUserEmail(inputSignInEmailAddress.getText())) {
                ToasterUtil.longToaster(context, "Invalid email address entered!");
                return;
            }
            if (!SignInUpUtil.verifyUserCred(inputSignInPassword.getText())) {
                ToasterUtil.longToaster(context, "Invalid password entered!");
                return;
            }
            String userEmail = inputSignInEmailAddress.getText().toString();
            String userCred = inputSignInPassword.getText().toString();
            if (BuildConfig.DEBUG) LOGGER.info("Read-in user-email [{}] & [{}]", userEmail, userCred);

            lockAllFields();
            signInUserCred = userCred;

            AsyncUserInfoPull asyncSignIn = new AsyncUserInfoPull(context, localSavedPrefs, exportToInternalCache, this, userEmail,
                    properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN),
                    properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT, 90L),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                    properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
            );
            asyncSignIn.execute(); //driving point passed onto Async
        }
    }

    public void routeToSignUp(View view) {
        if (view == signInPromptSignUpText) {
            activityStarter.launchSignUpScreen();
        }
    }

    private void opOnFields(boolean targetState) {
        inputSignInEmailAddress.setEnabled(targetState);
        inputSignInPassword.setEnabled(targetState);
        buttonSignInProceed.setEnabled(targetState);
    }

    void lockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Locking all fields");
        opOnFields(false);
    }

    void unlockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Unlocking all fields");
        opOnFields(true);
    }

    void clearAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Clearing all fields");
        inputSignInPassword.setText(EMPTY_STR);
        inputSignInEmailAddress.setText(EMPTY_STR);
    }

    private TestConnectivity generateTestConn() {
        return new TestConnectivity(this, DEFAULT_HOST_FOR_PINGING, 0);
    }

    @Override
    public void isConnectedToInternet(boolean internetConnectivity) {
        if (!internetConnectivity) {
            ToasterUtil.shortToaster(context, "Check your internet connectivity!");
            ActivityStarter.shutDownApp(this); //shut down if internet conn not ON
        }
    }

    @Override
    public void callBack(UserMappingProto.UserMapping retrievedUserMapping) {
        if (signInUserCred.isEmpty()) return;

        AsyncUserHashVerification asyncUserHashVerification = new AsyncUserHashVerification(context, localSavedPrefs, exportToInternalCache,
                signInUserCred, retrievedUserMapping);
        asyncUserHashVerification.execute();
    }

    private class AsyncUserHashVerification extends AsyncTask<Void, Void, Boolean> {

        private final Context context;
        private final LocalSavedPrefs localSavedPrefs;
        private final ExportToInternalPrefs exportToInternalCache;
        private final String userCred;
        private final UserMappingProto.UserMapping retrievedUserMapping;

        public AsyncUserHashVerification(Context context, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache, String userCred, UserMappingProto.UserMapping retrievedUserMapping) {
            this.context = context;
            this.localSavedPrefs = localSavedPrefs;
            this.exportToInternalCache = exportToInternalCache;
            this.userCred = userCred;
            this.retrievedUserMapping = retrievedUserMapping;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return !retrievedUserMapping.getHashCred().isEmpty() && AuthUtil.hashMatches(userCred, retrievedUserMapping.getHashCred());
        }

        @Override
        protected void onPostExecute(Boolean userSignInResult) {
            if (userSignInResult) {
                ToasterUtil.longToaster(context, "Signing in!");
                localSavedPrefs.setUserCredHashExpiryTimestamp(TimingUtil.computeExpiryTimestamp(10));
                exportToInternalCache.writeUserCredHashExpiryTime();

                activityStarter.restartApp();
            } else {
                ToasterUtil.longToaster(context, "User not authorized / incorrect details!");
                clearAllFields();
                unlockAllFields();
            }
        }
    }

}
