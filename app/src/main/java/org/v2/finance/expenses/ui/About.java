package org.v2.finance.expenses.ui;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.v2.finance.expenses.R;
import org.v2.finance.expenses.properties.PropertyReader;

import java.util.Objects;

import static org.v2.finance.expenses.constants.Constants.APP_DEV;
import static org.v2.finance.expenses.constants.Constants.APP_FB;
import static org.v2.finance.expenses.constants.Constants.APP_VERSION;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;

/**
 * @author Vivek Verma
 * @since 10-09-2019
 */
public class About extends AppCompatActivity {

    private TextView textAppVersion;
    private TextView textFbLink;
    private TextView textAppDev;
    private PropertyReader properties;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        textAppVersion = findViewById(R.id.textAppVersion);
        textFbLink = findViewById(R.id.textFbLink);
        textAppDev = findViewById(R.id.textAppDev);
        properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
        populateTexts();

        deactivateHeavyImportingFlag();

        Toolbar toolbar = findViewById(R.id.toolbar_about);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }
    }

    private void populateTexts() {
        textAppVersion.setText(properties.getProperty(APP_VERSION));
        textAppDev.setText(properties.getProperty(APP_DEV));
        textFbLink.setText(properties.getProperty(APP_FB));
    }
}
