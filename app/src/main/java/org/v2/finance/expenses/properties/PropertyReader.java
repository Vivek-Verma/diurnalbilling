package org.v2.finance.expenses.properties;

import android.content.Context;
import android.content.res.AssetManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_BOOLEAN_VALUE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_DOUBLE_VALUE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_INT_VALUE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_LONG_VALUE;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;

/**
 * @author Vivek Verma
 * @since 3/5/20
 * Initialized in the MainActivity and sets the public static PropertyReader in Helper
 */
public class PropertyReader implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyReader.class);

    private final Properties properties = new Properties();

    PropertyReader() { //for Test /-
    }

    public PropertyReader(Context context, String diurnalPropertiesTitle) {

        final AssetManager assetManager = context.getAssets();
        try (InputStream inputStream = assetManager.open(diurnalPropertiesTitle)) {
            this.properties.load(inputStream);
        } catch (IOException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Error in reading in the Properties file!!! Err: ", e);
        }
        if (BuildConfig.DEBUG) LOGGER.info("Properties: {}", properties.stringPropertyNames());
    }

    private String getPropertyDirect(String key) {
        return properties.getProperty(key);
    }

    public String getProperty(String key) {
        return getProperty(key, EMPTY_STR);
    }

    public String getProperty(String key, String defVal) {
        return properties.getProperty(key, defVal);
    }

    public int getIntProperty(String key) {
        return getIntProperty(key, DEFAULT_INT_VALUE);
    }

    public int getIntProperty(String key, int defVal) {
        final String val = getPropertyDirect(key);
        int finalVal = defVal;
        try {
            finalVal = Integer.parseInt(val);
        } catch (Exception e) {
            LOGGER.error("FAILED to obtain int value for '{}', falling to default value of '{}'. Err: ", key, defVal, e);
        }
        return finalVal;
    }

    public long getLongProperty(String key) {
        return getLongProperty(key, DEFAULT_LONG_VALUE);
    }

    public long getLongProperty(String key, long defVal) {
        final String val = getPropertyDirect(key);
        long finalVal = defVal;
        try {
            finalVal = Long.parseLong(val);
        } catch (Exception e) {
            LOGGER.error("FAILED to obtain long value for '{}', falling to default value of '{}'. Err: ", key, defVal, e);
        }
        return finalVal;
    }

    public double getDoubleProperty(String key) {
        return getDoubleProperty(key, DEFAULT_DOUBLE_VALUE);
    }

    public double getDoubleProperty(String key, double defVal) {
        final String val = getPropertyDirect(key);
        double finalVal = defVal;
        try {
            finalVal = Double.parseDouble(val);
        } catch (Exception e) {
            LOGGER.error("FAILED to obtain double value for '{}', falling to default value of '{}'. Err: ", key, defVal, e);
        }
        return finalVal;
    }

    public boolean getBooleanProperty(String key) {
        return getBooleanProperty(key, DEFAULT_BOOLEAN_VALUE);
    }

    public boolean getBooleanProperty(String key, boolean defVal) {
        final String val = getPropertyDirect(key);
        boolean finalVal = defVal;
        try {
            finalVal = Boolean.parseBoolean(val);
        } catch (Exception e) {
            LOGGER.error("FAILED to obtain boolean value for '{}', falling to default value of '{}'. Err: ", key, defVal, e);
        }
        return finalVal;
    }
}
