package org.v2.finance.expenses.contracts;

/**
 * @author Vivek Verma
 * @since 28-11-2019
 */
public interface DeletionContract {

    void handleDialogOk();

    void handleDialogCancel();
}
