package org.v2.finance.expenses.graphs;

import android.content.Context;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.exception.GraphingException;
import org.v2.finance.expenses.transporters.TransportBillingAmount;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import static org.v2.finance.expenses.constants.Constants.FORMAT_GRAPH_X_AXIS_DISPLAY;
import static org.v2.finance.expenses.constants.Constants.MAX_MONTHS_ON_GRAPH;
import static org.v2.finance.expenses.constants.Constants.REGEX_SPACE;
import static org.v2.finance.expenses.util.Helper.crackUpTheMonthRow;
import static org.v2.finance.expenses.util.Helper.getMonthNumberFromName;
import static org.v2.finance.expenses.util.ToasterUtil.shortToaster;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 16-02-2019
 * @since 16-02-2019
 */
public class Graphing {

    private final Logger logger = LoggerFactory.getLogger(Graphing.class);

    private final GraphView graph;
    private final StaticLabelsFormatter labelFormatter;
    private final String[] monthlyExpenseList;
    private double[] xCoOrd;
    private String[] xAxisLabels;
    private double[] yCoOrd;
    private final Context callingContext;

    public Graphing(GraphView graph, String[] monthlyExpenseList, Context callingContext) throws GraphingException {
        this.graph = graph;
        this.monthlyExpenseList = monthlyExpenseList;

        this.labelFormatter = new StaticLabelsFormatter(graph);
        this.callingContext = callingContext;
        populateDataForGraph();
    }

    private void populateDataForGraph() throws GraphingException {
        if (monthlyExpenseList == null) {
            logger.warn("Monthly Expense list is null");
            throw new GraphingException("Null month list, cannot proceed with plotting", Constants.GraphingExceptionCases.MONTH_LIST_NULL);
        } else if (monthlyExpenseList.length == 0) {
            logger.warn("Monthly expense list is empty!");
            throw new GraphingException("Empty month list, cannot proceed with plotting", Constants.GraphingExceptionCases.MONTH_LIST_EMPTY);
        } else if (monthlyExpenseList.length == 1) {
            logger.warn("Cannot draw graph from one month only..");
            shortToaster(callingContext, "Number of months to be more than 1 for graph!");
            throw new GraphingException("Only 1 month for graph, cannot proceed with plotting", Constants.GraphingExceptionCases.MONTH_LIST_SINGLE_PARAM_ONLY);
        }

        //processing goes here
        int months = monthlyExpenseList.length;
        int lowerMonthLimit = months - MAX_MONTHS_ON_GRAPH;
        if (lowerMonthLimit < 0) lowerMonthLimit = 0;
        else {
            ToasterUtil.shortToaster(callingContext, "Graphic data can be seen for last " + MAX_MONTHS_ON_GRAPH + " months only!");
            if (BuildConfig.DEBUG) logger.warn("Will be slicing graphing to only {} months", MAX_MONTHS_ON_GRAPH);
        }
        int duration = months - lowerMonthLimit;
        xCoOrd = new double[duration];
        xAxisLabels = new String[duration];
        yCoOrd = new double[duration];

        for (int i = duration - 1, j = 0; i >= 0; i--, j++) {
            String[] monthRow = Helper.splitStringOnDelim(monthlyExpenseList[i], REGEX_SPACE); //<bullet>. mmm 'yy : sign currency amount
            xCoOrd[j] = j;
            xAxisLabels[j] = String.format(FORMAT_GRAPH_X_AXIS_DISPLAY, getMonthNumberFromName(monthRow[1]), monthRow[2].substring(1));//mm/yy
            TransportBillingAmount transportBillingAmount = crackUpTheMonthRow(monthRow);
            yCoOrd[j] = transportBillingAmount.getActualAmount();
        }

        for (int i = 0; i < xCoOrd.length; i++) {
            //yCoOrd[i] = normalized(yCoOrd[i], minAmt, maxAmt) * 10;
            if (BuildConfig.DEBUG) logger.info("[{},{}] :: [{}]", xCoOrd[i], yCoOrd[i], xAxisLabels[i]);
        }

        try {
            labelFormatter.setHorizontalLabels(xAxisLabels);
            graph.getGridLabelRenderer().setLabelFormatter(labelFormatter);
        } catch (Exception e) {
            logger.error("Exception while drawing graph : ", e);
            shortToaster(callingContext, "Couldn't plot graph. Reason : " + e);
        }
    }

    public void invertAmounts() {
        for (int i = 0; i < yCoOrd.length; i++) yCoOrd[i] *= -1;
    }

    private double normalized(double val, double minY, double maxY) {
        return (val - minY) / ((maxY - minY) * 1.0);
    }

    public double[] getxCoOrd() {
        return xCoOrd;
    }

    public double[] getyCoOrd() {
        return yCoOrd;
    }

}
