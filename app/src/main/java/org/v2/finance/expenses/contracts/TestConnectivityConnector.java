package org.v2.finance.expenses.contracts;

/**
 * @author Vivek Verma
 * @since 30-10-2019
 */
public interface TestConnectivityConnector {
    void isConnectedToInternet(boolean internetConnectivity);
}