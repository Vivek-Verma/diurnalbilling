package org.v2.finance.expenses.connectivity;

import android.os.AsyncTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;
import org.v2.finance.expenses.util.Helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static org.v2.finance.expenses.constants.Constants.ERR_WHILE_GDRIVE_CHECK;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;

/**
 * @author Vivek Verma
 * @since 4/5/21
 */
abstract class AbstractGDriveDataImporter extends AsyncTask<Void, Void, String> {

    public final Logger LOGGER = LoggerFactory.getLogger(AbstractGDriveDataImporter.class);
    private final String url;
    private final GDriveConnector connector;
    private final GDriveLocators locators;

    public AbstractGDriveDataImporter(String id, GDriveConnector connector, GDriveLocators locators) {
        this.url = Helper.gDriveLinkGenerator(id);
        this.connector = connector;
        this.locators = locators;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return importRemoteData(url);
    }

    private String importRemoteData(String urlToHit) {
        final URL url;
        try {
            url = new URL(urlToHit);
            if (BuildConfig.DEBUG) LOGGER.info("url to hit for gDrive : {}", urlToHit);
        } catch (MalformedURLException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Malformed URL : {}, Error : ", urlToHit, e);
            return String.format(ERR_WHILE_GDRIVE_CHECK, locators);
        }

        HttpsURLConnection httpsURLConnection = null;
        try {
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.connect();

            if (BuildConfig.DEBUG) LOGGER.info("Response code for gDrive pinger : {}", httpsURLConnection.getResponseCode());
            if (httpsURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                final StringBuilder lines = new StringBuilder();
                try (BufferedReader in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()))) {
                    String line;
                    while ((line = in.readLine()) != null) {
                        lines.append(line).append(NEW_LINE);
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) LOGGER.error("Error occurred while reading the remote gDrive file : ", e);
                }
                return lines.toString();
            }
        } catch (IOException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Unable to establish internet connectivity. Error : ", e);
        } finally {
            if (httpsURLConnection != null) httpsURLConnection.disconnect();
        }
        return String.format(ERR_WHILE_GDRIVE_CHECK, locators);
    }

    @Override
    protected void onPostExecute(String remoteData) {
        connector.readRemoteGDriveData(remoteData, locators);
    }
}
