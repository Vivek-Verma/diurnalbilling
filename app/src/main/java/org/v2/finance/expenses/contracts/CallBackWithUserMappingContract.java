package org.v2.finance.expenses.contracts;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

/**
 * @author Vivek Verma
 * @since 1/5/21
 */
public interface CallBackWithUserMappingContract {

    void callBack(UserMappingProto.UserMapping retrievedUserMapping);

}
