package org.v2.finance.expenses.contracts;

/**
 * @author Vivek Verma
 * @since 4/5/21
 */
public interface GDriveConnector {
    void readRemoteGDriveData(String remoteData, GDriveLocators gDriveLocators);
}