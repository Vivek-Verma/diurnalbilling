package org.v2.finance.expenses.ui.alerter;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.util.ToasterUtil;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 16-07-2019
 * @since 16-07-2019
 */
public abstract class AbstractAlerter {

    protected final AlertDialog.Builder alert;
    protected final Context context;
    private final boolean isBodyHtml;

    public AbstractAlerter(final Context context, String title, String messageForDisplay) {
        this(context, title, messageForDisplay, false);
    }

    public AbstractAlerter(final Context context, String title, String messageForDisplay, boolean isBodyHtml) {
        this.context = context;
        this.alert = new AlertDialog.Builder(context);
        this.isBodyHtml = isBodyHtml;

        this.alert.setTitle(title);
        if (messageForDisplay != null) //alert's message and singleChoiceItems are mutually exclusive in nature
            this.alert.setMessage(isBodyHtml ?
                    Html.fromHtml(messageForDisplay)
                    : messageForDisplay);
    }

    /**
     * Sets the OK button characteristics
     *
     * @param textForButton   Text to be displayed on the button
     * @param displayPositive Text to be displayed in the toast
     */
    void initOkButton(String textForButton, String displayPositive) {
        if (textForButton.isEmpty()) return;
        alert.setPositiveButton(textForButton, (dialog, which) -> displayToast(displayPositive));
    }

    /**
     * Sets the NO button characteristics
     *
     * @param textForButton   Text to be displayed on the button
     * @param displayNegative Text to be displayed in the toast
     */
    void initNoButton(String textForButton, String displayNegative) {
        if (textForButton.isEmpty()) return;
        alert.setNegativeButton(textForButton, (dialog, which) -> displayToast(displayNegative));
    }

    void displayToast(String text) {
        ToasterUtil.shortToaster(context, text);
    }

    public void showAlert(String textPositive, String displayPositive, String textNegative, String displayNegative) {
        initOkButton(textPositive, displayPositive);
        initNoButton(textNegative, displayNegative);
        invokeShowAlert();
    }

    public void showAlert(String textPositive, String textNegative) {
        initOkButton(textPositive, textPositive);
        initNoButton(textNegative, textNegative);
        invokeShowAlert();
    }

    private void invokeShowAlert() {
        AlertDialog actualAlert = alert.create();
        actualAlert.show();
        if (isBodyHtml)
            ((TextView) actualAlert.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void showAlert(String textPositive) {
        showAlert(textPositive, Constants.EMPTY_STR);
    }

}
