package org.v2.finance.expenses.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.notification.DailyEntryNotificationDaemon;
import org.v2.finance.expenses.notification.Notifier;
import org.v2.finance.expenses.util.Helper;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Vivek Verma
 * @since 15-12-2019
 */
public class DailyEntryBroadcastRx extends BroadcastReceiver {
    private final Logger logger = LoggerFactory.getLogger(DailyEntryBroadcastRx.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        if (BuildConfig.DEBUG) logger.info("Received broadcast for daily entry at '{}', context : {}, intent : {}", Calendar.getInstance().getTime(), context, intent);
        final Notifier notifier = new Notifier(context, intent);
        final DailyEntryNotificationDaemon daemon = new DailyEntryNotificationDaemon(context, notifier);
        final ExecutorService service = Executors.newSingleThreadExecutor();
        service.submit(daemon);
        service.shutdown();
        //scheduling the next hit for daily check
        Helper.createAndFireDailyEntryAlarm(context);
    }
}
