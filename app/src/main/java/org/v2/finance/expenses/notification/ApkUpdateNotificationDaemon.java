package org.v2.finance.expenses.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.connectivity.TestConnectivity;
import org.v2.finance.expenses.connectivity.VersionCheckPinger;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;
import org.v2.finance.expenses.contracts.TestConnectivityConnector;
import org.v2.finance.expenses.core.VersionCheck;

import static org.v2.finance.expenses.constants.Constants.ERR_WHILE_VERSION_CHECK;
import static org.v2.finance.expenses.constants.Constants.ID_SITE_VERSION_CHECK;

/**
 * @author Vivek Verma
 * @since 03-11-2019
 */
public class ApkUpdateNotificationDaemon implements Runnable, TestConnectivityConnector, GDriveConnector {

    private final Logger LOGGER = LoggerFactory.getLogger(ApkUpdateNotificationDaemon.class);
    private final Notifier notifier;

    public ApkUpdateNotificationDaemon(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void run() {
        TestConnectivity testConnectivity = new TestConnectivity(this, Constants.DEFAULT_HOST_FOR_PINGING);
        testConnectivity.execute();
    }

    @Override
    public void isConnectedToInternet(boolean internetConnectivity) {
        if (internetConnectivity) {
            //going for second hit for actual version checking from here
            if (BuildConfig.DEBUG) LOGGER.warn("Connected to internet! Proceeding for update checkin!");
            VersionCheckPinger pingerService = new VersionCheckPinger(this, ID_SITE_VERSION_CHECK);
            pingerService.execute();
        }
    }

    @Override
    public void readRemoteGDriveData(String versionHitText, GDriveLocators gDriveLocators) {
        if (gDriveLocators == GDriveLocators.VERSION) {
            if (BuildConfig.DEBUG) LOGGER.info("Rx version hit text from daemon act : {}", versionHitText);
            if (ERR_WHILE_VERSION_CHECK.equals(versionHitText)) {
                if (BuildConfig.DEBUG) LOGGER.info("Couldn't retrieve version check file!");
            } else {
                VersionCheck versionCheck = VersionCheck.generateVersionCheckFromVersionHitText(versionHitText);
                if (versionCheck.isNewVersionAvailable(BuildConfig.VERSION_NAME)) {
                    if (BuildConfig.DEBUG) LOGGER.info("Discovered new version!");
                    notifier.fireNotification();
                } else {
                    if (versionCheck.getUpdate().isEmpty()) {
                        if (BuildConfig.DEBUG) LOGGER.warn("Couldn't query the version!");
                    } else {
                        if (BuildConfig.DEBUG) LOGGER.info("No Update available!");
                    }
                }
                if (BuildConfig.DEBUG) notifier.fireNotification(); //TODO -- to be removed before final push
            }
        }
    }
}