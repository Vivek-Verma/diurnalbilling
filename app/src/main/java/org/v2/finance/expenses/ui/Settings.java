package org.v2.finance.expenses.ui;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.vv.personal.diurnal.artifactory.generated.ResponsePrimitiveProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.core.settings.sub.Payment;
import org.v2.finance.expenses.core.settings.sub.Upi;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.ui.alerter.AlerterForTxToPremium;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.SignInUpUtil;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.BLACK;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.GREEN_DARK;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.LT_YELLOW;
import static org.v2.finance.expenses.constants.Constants.RED;
import static org.v2.finance.expenses.constants.Constants.USER_SIGN_IN_EXPIRED;
import static org.v2.finance.expenses.util.CurrencyUtil.CURRENCY_SIGN;

/**
 * @author Vivek Verma
 * @lastMod 08-04-2021
 * @since 29/3/21
 */
public class Settings extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private final Logger LOGGER = LoggerFactory.getLogger(Settings.class);
    private final Context context = this;

    private EditText displayUserName;
    private TextView displayEmail;
    private EditText displayMobile;
    private TextView displayCurrencyText;
    private TextView displayCurrencySign;
    private Spinner displayCurrency;
    private TextView displayClickableLogoutText;
    private TextView placeHolderIsPremiumUser;
    private Button premiumUserButton;
    private TextView displayIsCloudSaveEnabled;
    private TextView displayPaymentExpiryDate;
    private TextView displayLastLocalSaveTimestamp;
    private TextView displayLastCloudSaveTimestamp;
    private TextView displayLastAccessedTimestamp;
    private Button imposeSettings;

    private LocalSavedPrefs localSavedPrefs = null;
    private ExportToInternalPrefs exportToInternalCache;
    private ActivityStarter activityStarter;
    private PropertyReader properties;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityStarter = new ActivityStarter(context);
        try {
            localSavedPrefs = (LocalSavedPrefs) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
            if (localSavedPrefs == null) {
                ToasterUtil.shortToaster(context, "Failed to open Settings due to failure of internal cache read");
                return;
            }
            exportToInternalCache = new ExportToInternalPrefs(context, localSavedPrefs);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) LOGGER.error("Couldn't de-serialize data in Settings => ", e);
        }

        properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
        if (properties == null) {
            properties = new PropertyReader(context, getString(R.string.diurnal_properties));
        }

        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar_settings);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }

        displayUserName = findViewById(R.id.displayUserName);
        displayEmail = findViewById(R.id.displayEmail);
        displayMobile = findViewById(R.id.displayMobile);
        displayCurrencyText = findViewById(R.id.displayCurrencyText);
        displayCurrencySign = findViewById(R.id.displayCurrencySign);
        displayCurrency = findViewById(R.id.displayCurrency);
        premiumUserButton = findViewById(R.id.premiumUserButton);
        displayIsCloudSaveEnabled = findViewById(R.id.displayIsCloudSaveEnabled);
        placeHolderIsPremiumUser = findViewById(R.id.placeHolderIsPremiumUser);
        displayPaymentExpiryDate = findViewById(R.id.displayPaymentExpiryDate);
        displayLastLocalSaveTimestamp = findViewById(R.id.displayLastLocalSaveTimestamp);
        displayLastCloudSaveTimestamp = findViewById(R.id.displayLastCloudSaveTimestamp);
        displayLastAccessedTimestamp = findViewById(R.id.displayLastAccessedTimestamp);
        displayClickableLogoutText = findViewById(R.id.displayClickableLogoutText);
        imposeSettings = findViewById(R.id.imposeSettings);

        premiumUserButton.setOnClickListener(this);
        imposeSettings.setOnClickListener(this);

        displayPaymentExpiryDate.setOnClickListener(this);
        displayIsCloudSaveEnabled.setOnClickListener(this);

        ArrayAdapter<UserMappingProto.Currency> currencyArrayAdapter = SignInUpUtil.generateAndPruneCurrencyListArrayAdapter(context);
        displayCurrency.setAdapter(currencyArrayAdapter);
        displayCurrency.setOnItemSelectedListener(this);

        UserMappingProto.Currency userCurrency = CurrencyUtil.currencyToApply;
        int currencySelectionIndex = 0;
        try {
            for (int i = 0; i < currencyArrayAdapter.getCount(); i++) {
                if (currencyArrayAdapter.getItem(i).name().equals(userCurrency.name())) {
                    currencySelectionIndex = i;
                    break;
                }
            }
        } catch (NullPointerException ignored) {
        }
        displayCurrency.setSelection(currencySelectionIndex, true); //to iterate over and display the set currency

        populateDisplayData();
    }

    private static class DisplayTextAndColor {
        private final String displayTimestamp;
        private Integer textColor = null;

        public DisplayTextAndColor(long displayTimestamp) {
            if (displayTimestamp == DEFAULT_TIMESTAMP) {
                textColor = RED;
                this.displayTimestamp = String.valueOf(DEFAULT_TIMESTAMP);
            } else {
                this.displayTimestamp = TimingUtil.parseTimestampForSettingsDisplay(displayTimestamp);
            }
        }

        public String getDisplayTimestamp() {
            return displayTimestamp;
        }

        public Integer getTextColor() {
            return textColor;
        }
    }

    private void populateDisplayData() {
        displayUserName.setText(localSavedPrefs.getUserName());
        displayEmail.setText(localSavedPrefs.getUserEmail());
        displayMobile.setText(String.valueOf(localSavedPrefs.getUserMobile()));

        if (localSavedPrefs.isPremiumUser()) {
            premiumUserButton.setText(getString(R.string.settings_is_premium_user_yes));
            premiumUserButton.setTextColor(GREEN_DARK);
            premiumUserButton.setBackgroundColor(LT_YELLOW);

            displayIsCloudSaveEnabled.setText(getString(R.string.settings_is_cloud_save_enabled_yes));
            displayIsCloudSaveEnabled.setTextColor(GREEN_DARK);

            premiumUserButton.setEnabled(false);
        } else {
            premiumUserButton.setText(getString(R.string.settings_is_premium_user_no));
            premiumUserButton.setTextColor(RED);
            premiumUserButton.setBackgroundColor(BLACK);

            displayIsCloudSaveEnabled.setText(getString(R.string.settings_is_cloud_save_enabled_no));
            displayIsCloudSaveEnabled.setTextColor(RED);

            premiumUserButton.setEnabled(true);
        }

        DisplayTextAndColor displayTextAndColor = new DisplayTextAndColor(localSavedPrefs.getPaymentExpiryTimestamp());
        displayPaymentExpiryDate.setText(displayTextAndColor.getDisplayTimestamp());
        if (displayTextAndColor.getTextColor() != null) displayPaymentExpiryDate.setTextColor(displayTextAndColor.getTextColor());

        displayTextAndColor = new DisplayTextAndColor(localSavedPrefs.getLastSavedTimeStamp());
        displayLastLocalSaveTimestamp.setText(displayTextAndColor.getDisplayTimestamp());
        if (displayTextAndColor.getTextColor() != null) displayLastLocalSaveTimestamp.setTextColor(displayTextAndColor.getTextColor());

        displayTextAndColor = new DisplayTextAndColor(localSavedPrefs.getLastCloudUploadTimeStamp());
        displayLastCloudSaveTimestamp.setText(displayTextAndColor.getDisplayTimestamp());
        if (displayTextAndColor.getTextColor() != null) displayLastCloudSaveTimestamp.setTextColor(displayTextAndColor.getTextColor());

        displayTextAndColor = new DisplayTextAndColor(localSavedPrefs.getLastUsedTimeStamp());
        displayLastAccessedTimestamp.setText(displayTextAndColor.getDisplayTimestamp());
        if (displayTextAndColor.getTextColor() != null) displayLastAccessedTimestamp.setTextColor(displayTextAndColor.getTextColor());
    }

    private void displayTransitionToPremiumAlert() {
        String destEmail = getString(R.string.app_contact_email);
        double upiAmount = Double.parseDouble(getString(R.string.app_premium_user_monthly_rate));
        String upiId = getString(R.string.app_premium_user_upi_amazon);
        String upiCurrency = CURRENCY_SIGN.get(Helper.getUserCurrency());

        Payment payment = Helper.payment;
        if (BuildConfig.DEBUG) LOGGER.info("Payment details received: {}", payment);
        if (payment != null && payment.getUpi() != null) {
            Upi upi = payment.getUpi();
            if (upi.getId() != null)
                upiId = upi.getId();
            if (upi.getAmount() != null) {
                upiAmount = upi.getAmount();
                double fxDivisor = -1.0;
                String fx = Helper.getUserCurrency().name();
                for (int i = -1; ++i < upi.getFxRates().size(); ) {
                    if (fx.equals(upi.getFxRates().get(i).getCurr())) {
                        fxDivisor = upi.getFxRates().get(i).getRate();
                        break;
                    }
                }
                if (fxDivisor == -1.0) fxDivisor = 1.0;
                upiAmount /= fxDivisor;
            }
        }
        new AlerterForTxToPremium(context,
                getString(R.string.app_premium_user_tx_alert_title),
                String.format(getString(R.string.app_premium_user_tx_body_html),
                        upiCurrency,
                        upiAmount,
                        upiId,
                        destEmail,
                        getString(R.string.app_settings_email_subject_query),
                        destEmail))
                .showAlert("NOTED");
    }

    @Override
    public void onClick(View view) {
        if (view == premiumUserButton) {
            if (premiumUserButton.isEnabled()) {
                displayTransitionToPremiumAlert();
            }
        } else if (view == displayPaymentExpiryDate || view == displayIsCloudSaveEnabled || view == placeHolderIsPremiumUser) {
            displayTransitionToPremiumAlert();
        } else if (view == imposeSettings) {
            if (!SignInUpUtil.verifyUserName(displayUserName.getText())) {
                ToasterUtil.longToaster(context, "Enter min 3 characters for name");
                return;
            }
            if (!SignInUpUtil.verifyMobileWithIsd(displayMobile.getText())) {
                ToasterUtil.longToaster(context, "Invalid Mobile with ISD!");
                return;
            }
            String userName = displayUserName.getText().toString();
            String email = displayEmail.getText().toString();
            Long mobile = Long.valueOf(displayMobile.getText().toString());
            UserMappingProto.Currency currency = UserMappingProto.Currency.valueOf(displayCurrencyText.getText().toString().trim());

            lockAllFields();
            AsyncImposeUpdatedSettings asyncImposeUpdatedSettings = new AsyncImposeUpdatedSettings(context, userName, email, mobile, currency,
                    properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO),
                    properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO_TIMEOUT, 90L),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                    properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
            );
            asyncImposeUpdatedSettings.execute();
        }
    }

    //For currency spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        UserMappingProto.Currency selectedCurrency = (UserMappingProto.Currency) parent.getItemAtPosition(position);
        if (selectedCurrency != null && selectedCurrency != UserMappingProto.Currency.UNRECOGNIZED) {
            displayCurrencyText.setText(selectedCurrency.name());
            displayCurrencySign.setText(CURRENCY_SIGN.get(selectedCurrency));
        }
    }

    //For currency spinner
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void routeToLogout(View view) {
        if (view == displayClickableLogoutText) {
            localSavedPrefs.setUserCredHashExpiryTimestamp(USER_SIGN_IN_EXPIRED);
            exportToInternalCache.writeUserCredHashExpiryTime();
            ToasterUtil.shortToaster(context, "Logging out!");
            activityStarter.restartApp();
        }
    }

    private void opOnFields(boolean targetState) {
        displayUserName.setEnabled(targetState);
        displayMobile.setEnabled(targetState);
        displayCurrency.setEnabled(targetState);
        displayCurrencyText.setEnabled(targetState);
        displayCurrencySign.setEnabled(targetState);
        imposeSettings.setEnabled(targetState);
    }

    void lockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Locking all fields");
        opOnFields(false);
    }

    void unlockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Unlocking all fields");
        opOnFields(true);
    }

    void clearAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Clearing all fields");
        displayUserName.setText(EMPTY_STR);
        displayMobile.setText(EMPTY_STR);
        displayCurrency.setSelection(0);
    }

    private class AsyncImposeUpdatedSettings extends AsyncSignInUpModule {
        private final UserMappingProto.UserMapping updatedUserInfo;
        private final PingServerWithData<UserMappingProto.UserMapping> pingServer;
        private boolean isUserUpdateSuccessful;

        public AsyncImposeUpdatedSettings(Context context, String updatedUserName, String email, Long updatedMobileWithIsd, UserMappingProto.Currency updatedCurrency,
                                          String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
            updatedUserInfo = UserMappingProto.UserMapping.newBuilder()
                    .setUsername(updatedUserName)
                    .setEmail(email)
                    .setMobile(updatedMobileWithIsd)
                    .setCurrency(updatedCurrency)
                    .build();
            if (BuildConfig.DEBUG) LOGGER.info("AsyncImposeUpdatedSettings isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
            ToasterUtil.shortToaster(context, "Initiating server contact!");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            try {
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, updatedUserInfo));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                isUserUpdateSuccessful = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getBoolResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), isUserUpdateSuccessful);
                packageResponseDetail.close();
                return isUserUpdateSuccessful;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact user-info-update module for DBI. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while contacting user-info-update DBI. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean isUserUpdateSuccessful) {
            if (isUserUpdateSuccessful == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
            } else if (isUserUpdateSuccessful) {
                ToasterUtil.longToaster(context, "New Settings applied, restarting app now");
                localSavedPrefs.setUserName(updatedUserInfo.getUsername());
                localSavedPrefs.setCurrency(updatedUserInfo.getCurrency());
                localSavedPrefs.setUserMobile(updatedUserInfo.getMobile());
                exportToInternalCache.writeUserMobile();
                exportToInternalCache.writeUserName();
                exportToInternalCache.writeCurrency();

                activityStarter.restartApp();
            } else {
                ToasterUtil.longToaster(context, "Couldn't apply settings properly, contact developer!");
                clearAllFields();
                unlockAllFields();
            }
        }
    }
}
