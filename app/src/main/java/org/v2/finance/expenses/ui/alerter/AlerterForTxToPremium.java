package org.v2.finance.expenses.ui.alerter;

import android.content.Context;

/**
 * @author Vivek Verma
 * @since 1/5/21
 */
public class AlerterForTxToPremium extends AbstractAlerter {

    public AlerterForTxToPremium(Context context, String title, String messageForDisplay) {
        super(context, title, messageForDisplay, true);
    }

}
