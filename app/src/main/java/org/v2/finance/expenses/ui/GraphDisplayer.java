package org.v2.finance.expenses.ui;

import android.content.Context;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.exception.GraphingException;
import org.v2.finance.expenses.graphs.Graphing;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.util.Helper;

import static org.v2.finance.expenses.constants.Constants.RADIUS_DATA_POINT_GRAPH;
import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 16-02-2019
 * @since 16-02-2019
 */
public class GraphDisplayer extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private final Logger logger = LoggerFactory.getLogger(GraphDisplayer.class);

    protected final Context context = this;

    private RadioGroup radioGroupForAmountTypes;
    private RadioButton radioNormalAmount;
    private RadioButton radioInvertedAmount;
    private GraphView graph;

    private Graphing graphing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_graph);
        deactivateHeavyImportingFlag();

        graph = findViewById(R.id.expense_graphing);
        radioGroupForAmountTypes = findViewById(R.id.radio_group_graph);
        radioNormalAmount = findViewById(R.id.radio_normal_amount);
        radioInvertedAmount = findViewById(R.id.radio_inverted_amount);

        radioInvertedAmount.setChecked(true);
        radioGroupForAmountTypes.setOnCheckedChangeListener(this);

        try {
            graphing = new Graphing(graph, Helper.monthlyExpenseList, context);
            graphing.invertAmounts();
            createExpenseGraph(); //first time call goes here - with inverted setting for amount graphing (in the above line)
        } catch (GraphingException e) {
            if (BuildConfig.DEBUG) logger.error("Halting graphing because of the exception : " + e);
            new ActivityStarter(context).fireUpMainActivity();
        }
    }

    private void createExpenseGraph() {
        graph.removeAllSeries();

        double[] x = graphing.getxCoOrd();
        double[] y = graphing.getyCoOrd();

        if (BuildConfig.DEBUG) logger.info("Plotting points now!");
        DataPoint[] dataPoints = new DataPoint[x.length];
        for (int i = 0; i < x.length; i++) {
            if (BuildConfig.DEBUG) logger.info("[{} : {}]", x[i], y[i]);
            dataPoints[i] = new DataPoint(x[i], y[i]);
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPoints);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(RADIUS_DATA_POINT_GRAPH);
        graph.addSeries(series);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId) {
        if (radioGroup.getId() == radioGroupForAmountTypes.getId()) {
            graphing.invertAmounts();
            createExpenseGraph();
        }
    }
}
