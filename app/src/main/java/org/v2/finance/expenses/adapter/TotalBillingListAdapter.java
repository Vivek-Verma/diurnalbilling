package org.v2.finance.expenses.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;

import java.util.List;
import java.util.Locale;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_AMOMUNT;
import static org.v2.finance.expenses.constants.Constants.GRAY;
import static org.v2.finance.expenses.constants.Constants.GREEN_DARK;
import static org.v2.finance.expenses.constants.Constants.RED;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 22-09-2018
 */
public class TotalBillingListAdapter extends ArrayAdapter<EntryOfADay> {

    private final Logger logger = LoggerFactory.getLogger(TotalBillingListAdapter.class);

    private final Activity context;
    private final List<EntryOfADay> entireBillingList;

    public TotalBillingListAdapter(Activity context, List<EntryOfADay> entireBillingList) {
        super(context, R.layout.total_billing_list, entireBillingList);

        this.context = context;
        this.entireBillingList = entireBillingList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.total_billing_list, null, true);

        final TextView displayDate = rowView.findViewById(R.id.displayDate);
        final TextView displayTitle = rowView.findViewById(R.id.displayTitle);
        final TextView displayAmountSign = rowView.findViewById(R.id.displayAmountSign);
        final TextView displayCurrency = rowView.findViewById(R.id.displayCurrency);
        final TextView displayAmount = rowView.findViewById(R.id.displayAmount);
        final TextView displayDayOfWeek = rowView.findViewById(R.id.day_of_week_main);
        final String dateString = entireBillingList.get(position).getDisplayDate();

        String humanReadableDateActualDisplayDate = Helper.renderDateToReadableDate(dateString);
        if (BuildConfig.DEBUG) logger.info("Rendering {} to {}", dateString, humanReadableDateActualDisplayDate);
        displayDate.setText(humanReadableDateActualDisplayDate);
        //displayTitle.setText("#########"); //7
        displayTitle.setText(entireBillingList.get(position).getTitle());
        displayAmountSign.setText(entireBillingList.get(position).getDisplayAmountSign());
        displayCurrency.setText(CurrencyUtil.currencySignToDisplay);
        //displayAmount.setText(Double.toString(amountList[position]));
        displayAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, entireBillingList.get(position).getDisplayAmount()));

        int colorToBeAssigned = RED;
        if (ENTRY_TYPE_POSITIVE.equals(entireBillingList.get(position).getDisplayAmountSign())) {
            if (entireBillingList.get(position).getDisplayAmount() == 0) colorToBeAssigned = GRAY;
            else colorToBeAssigned = GREEN_DARK;
        }

        displayAmountSign.setTextColor(colorToBeAssigned);
        displayCurrency.setTextColor(colorToBeAssigned);
        displayAmount.setTextColor(colorToBeAssigned);

        String dayOfWeek = Helper.mapOfDisplayDateAndDayOfWeek.get(dateString);
        displayDayOfWeek.setText(dayOfWeek);
        if (Helper.isDayWeekend(dayOfWeek))
            rowView.setBackgroundColor(context.getResources().getColor(R.color.lightOrange));

        if (BuildConfig.DEBUG) logger.info("Replying from the getView function, with the date : {}", entireBillingList.get(position).getDisplayDate());
        return rowView;
    }
}
