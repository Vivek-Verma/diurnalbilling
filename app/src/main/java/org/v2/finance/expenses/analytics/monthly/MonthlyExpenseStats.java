package org.v2.finance.expenses.analytics.monthly;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.v2.finance.expenses.constants.Constants.MAX_DOUBLE;
import static org.v2.finance.expenses.constants.Constants.MIN_DOUBLE;
import static org.v2.finance.expenses.constants.Constants.ZERO_DOUBLE;

/**
 * @author Vivek Verma
 * @since 28/4/20
 */
public class MonthlyExpenseStats {
    private static final Logger logger = LoggerFactory.getLogger(MonthlyExpenseStats.class);

    public static final Map<String, Double> mapOfMonthlyExpenseMonthStringAndExpense = new LinkedHashMap<>();

    private double avgMonthlyExpense;
    private double maxMonthlyExpense;
    private double minMonthlyExpense;
    private int numberOfMonths;

    public static void clearMonthlyExpenseMap() {
        mapOfMonthlyExpenseMonthStringAndExpense.clear();
    }

    public static void insertEntryInMonthlyExpenseMap(String monthRecordAsDisplayed, double monthlyExpense) {
        mapOfMonthlyExpenseMonthStringAndExpense.put(monthRecordAsDisplayed, monthlyExpense);
    }

    public MonthlyExpenseStats() {
        computeExpenseVariants();
    }

    private void computeExpenseVariants() {
        double avg = ZERO_DOUBLE, max = MIN_DOUBLE, min = MAX_DOUBLE;
        setNumberOfMonths(mapOfMonthlyExpenseMonthStringAndExpense.values().size());
        for (Double exp : mapOfMonthlyExpenseMonthStringAndExpense.values()) {
            avg += exp;
            if (exp > max) max = exp;
            if (exp < min) min = exp;
        }
        if (didValueChange(avg, ZERO_DOUBLE)) avg /= numberOfMonths;
        max = getFinalValue(max, MIN_DOUBLE, ZERO_DOUBLE);
        min = getFinalValue(min, MAX_DOUBLE, ZERO_DOUBLE);
        setAvgMonthlyExpense(avg);
        setMaxMonthlyExpense(max);
        setMinMonthlyExpense(min);
    }

    private double getFinalValue(double newValue, double originalValue, double defaultValue) {
        return didValueChange(newValue, originalValue) ? newValue : defaultValue;
    }

    private boolean didValueChange(double newValue, double originalValue) {
        return Double.compare(newValue, originalValue) != 0;
    }

    public double getAvgMonthlyExpense() {
        return avgMonthlyExpense;
    }

    public void setAvgMonthlyExpense(double avgMonthlyExpense) {
        this.avgMonthlyExpense = avgMonthlyExpense;
    }

    public double getMaxMonthlyExpense() {
        return maxMonthlyExpense;
    }

    public void setMaxMonthlyExpense(double maxMonthlyExpense) {
        this.maxMonthlyExpense = maxMonthlyExpense;
    }

    public double getMinMonthlyExpense() {
        return minMonthlyExpense;
    }

    public void setMinMonthlyExpense(double minMonthlyExpense) {
        this.minMonthlyExpense = minMonthlyExpense;
    }

    public int getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(int numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }
}
