package org.v2.finance.expenses.permissions;

import android.Manifest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 20-07-2019
 * @since 20-07-2019
 */
public class PermissionConstants {

    public static final int DND_TOLERANCE_COUNT = 3;

    public static final String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final int CODE_PERMISSION_READ_EXTERNAL_STORAGE = 101;
    public static final String PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final int CODE_PERMISSION_WRITE_EXTERNAL_STORAGE = 102;

    public static final Map<String, Integer> PERMISSION_AND_CORRESPONDING_CODE_MAP = new HashMap<>();

    static {
        PERMISSION_AND_CORRESPONDING_CODE_MAP.put(PERMISSION_READ_EXTERNAL_STORAGE, CODE_PERMISSION_READ_EXTERNAL_STORAGE);
        PERMISSION_AND_CORRESPONDING_CODE_MAP.put(PERMISSION_WRITE_EXTERNAL_STORAGE, CODE_PERMISSION_WRITE_EXTERNAL_STORAGE);
    }

    public static final Map<String, String> PERMISSION_AND_THE_MESSAGE_TO_BE_DISPLAYED_MAP = new HashMap<>();

    static {
        PERMISSION_AND_THE_MESSAGE_TO_BE_DISPLAYED_MAP.put(PERMISSION_READ_EXTERNAL_STORAGE, "Permission to read file system state is required to give the app the ability to read the backup / restore file for restoring the app data."); //this permission is automatically granted (from write)
        PERMISSION_AND_THE_MESSAGE_TO_BE_DISPLAYED_MAP.put(PERMISSION_WRITE_EXTERNAL_STORAGE, "Permission to write to file system is required to give the app the ability to create a backup file.");
    }
}
