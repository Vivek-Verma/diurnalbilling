package org.v2.finance.expenses.port.export.ping;

import android.os.AsyncTask;

import com.vv.personal.diurnal.client.exposed.contracts.PingServer;
import com.vv.personal.diurnal.client.exposed.http.PingAndForget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;

/**
 * @author Vivek Verma
 * @since 19/3/21
 */
public class AsyncPingAndForget extends AsyncTask<Void, Void, Void> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncPingAndForget.class);

    private final String serverHost;
    private final String serverPingEndpoint;
    private final long serverRequestTimeout;
    private final String serverUser;
    private final String serverCred;
    private final PingServer pingServer;

    public AsyncPingAndForget(String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
        this.serverPingEndpoint = serverPingEndpoint;
        this.serverRequestTimeout = serverRequestTimeout;
        this.serverUser = serverUser;
        this.serverCred = serverCred;

        if (BuildConfig.DEBUG) LOGGER.info("AsyncPingAndForget isHttp: {}", isHttp);
        this.serverHost = isHttp ? httpServerHost : httpsServerHost;
        this.pingServer = isHttp ? new PingAndForget() : new com.vv.personal.diurnal.client.exposed.https.PingAndForget();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (BuildConfig.DEBUG) LOGGER.info("Pinging server [{}]", serverHost);
        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
        return null;
    }
}
