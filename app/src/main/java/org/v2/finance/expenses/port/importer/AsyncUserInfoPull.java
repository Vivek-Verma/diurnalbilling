package org.v2.finance.expenses.port.importer;

import android.content.Context;

import com.vv.personal.diurnal.artifactory.generated.DataTransitProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.contracts.CallBackWithUserMappingContract;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.USER_NON_EXISTENT_CODE;

/**
 * @author Vivek Verma
 * @since 1/5/21
 */
public class AsyncUserInfoPull extends AsyncSignInUpModule {
    private final Logger LOGGER = LoggerFactory.getLogger(AsyncUserInfoPull.class);

    private final LocalSavedPrefs localSavedPrefs;
    private final ExportToInternalPrefs exportToInternalCache;
    private final PingServerWithData<DataTransitProto.DataTransit> pingServer;
    private final String userEmail;
    private final CallBackWithUserMappingContract callBackWithUserMappingContract;
    private UserMappingProto.UserMapping retrievedUserMapping = UserMappingProto.UserMapping.newBuilder().build();
    private boolean isUserNonExistent = false;

    public AsyncUserInfoPull(Context context, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache, CallBackWithUserMappingContract callBackWithUserMappingContract, String userEmail,
                             String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
        super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
        this.localSavedPrefs = localSavedPrefs;
        this.exportToInternalCache = exportToInternalCache;
        this.callBackWithUserMappingContract = callBackWithUserMappingContract;
        this.userEmail = userEmail;

        if (BuildConfig.DEBUG) LOGGER.info("User-Info-Pull isHttp: {}", isHttp);
        this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        StopWatch timer = new StopWatch();
        timer.start();
        final DataTransitProto.DataTransit dataTransit = DataTransitProto.DataTransit.newBuilder().setEmail(userEmail).build();
        timer.stop();
        if (BuildConfig.DEBUG) LOGGER.info("Took {} ms to complete data tx obj gen", timer.getTime());

        try {
            timer.reset();
            timer.start();
            if (areExecutorsDead()) jumpStartExecutors();
            Future<PackageResponseDetail> future = executorService.submit(() ->
                    pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, dataTransit));

            PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
            retrievedUserMapping = UserMappingProto.UserMapping.parseFrom(packageResponseDetail.getInputStream());
            timer.stop();
            if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), retrievedUserMapping);
            packageResponseDetail.close();
            if (retrievedUserMapping.getMobile() == USER_NON_EXISTENT_CODE) isUserNonExistent = true;
            return !retrievedUserMapping.getHashCred().isEmpty();
        } catch (InterruptedException | IOException | NullPointerException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Failed to contact user-info-pull module for DBI. ", e);
        } catch (ExecutionException | TimeoutException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while user-info-pull DBI. Server unreachable! ", e);
        } finally {
            if (!timer.isStopped()) timer.stop();
            killAllExecutors();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean isUserMappingRetrieved) {
        if (isUserMappingRetrieved) {
            Helper.writeBackUserInfoToCache(retrievedUserMapping, localSavedPrefs, exportToInternalCache);
            if (callBackWithUserMappingContract != null) {
                callBackWithUserMappingContract.callBack(retrievedUserMapping);
            }
        } else {
            if (isUserNonExistent) ToasterUtil.longToaster(context, "User doesn't exist for this email id!");
            else ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
        }
    }
}
