package org.v2.finance.expenses.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.core.VersionInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.v2.finance.expenses.constants.Constants.FORMAT_CHANGELOG_HEADER;
import static org.v2.finance.expenses.util.Helper.centralChangeLogs;
import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;

/**
 * @author Vivek Verma
 * @since 17-12-2019
 */
public class ChangeLog extends AppCompatActivity {

    private final Logger logger = LoggerFactory.getLogger(ChangeLog.class);
    private final Context context = this;

    private LinearLayout rootLayout;
    private TextView rootText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changelog);
        deactivateHeavyImportingFlag();

        rootLayout = findViewById(R.id.root_changelog_linear_layout);
        rootText = findViewById(R.id.root_changelog_default_text);

        Toolbar toolbar = findViewById(R.id.toolbar_changelog);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }

        crankUpTheChangeLogs();
    }

    private void crankUpTheChangeLogs() {
        final List<VersionInfo> localChangeLogs = new ArrayList<>(centralChangeLogs);
        if (!localChangeLogs.isEmpty()) {
            rootText.setVisibility(View.GONE);
            List<TextView> textViewsPerVersion = generateTextViewsPerVersion(localChangeLogs);
            for (TextView textView : textViewsPerVersion) rootLayout.addView(textView);
        }
    }

    private List<TextView> generateTextViewsPerVersion(List<VersionInfo> logs) {
        List<TextView> textViews = new ArrayList<>();
        for (VersionInfo versionInfo : logs) {
            textViews.add(generateHeaderText(versionInfo.getVersion(), versionInfo.getDate()));
            textViews.add(generateDataText(versionInfo.getInfo()));
            textViews.add(generateBreaker());
        }
        return textViews;
    }

    private TextView generateHeaderText(String version, String date) {
        TextView textView = generateRawTextView();
        textView.setText(String.format(FORMAT_CHANGELOG_HEADER, version, date));
        textView.setTextColor(getResources().getColor(R.color.primaryEnd));
        textView.setTextSize(20);
        textView.setTypeface(textView.getTypeface(), Typeface.BOLD_ITALIC);
        return textView;
    }

    private TextView generateDataText(String info) {
        TextView textView = generateRawTextView();
        textView.setText(info);
        textView.setTextColor(getResources().getColor(R.color.green));
        textView.setTextSize(14);
        return textView;
    }

    private TextView generateBreaker() {
        TextView textView = generateRawTextView();
        textView.getLayoutParams().height = 2;
        textView.setBackgroundColor(getResources().getColor(R.color.gray));
        return textView;
    }

    private TextView generateRawTextView() {
        TextView view = new TextView(context);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.setMargins(5, 5, 5, 3);
        view.setLayoutParams(params);
        return view;
    }
}
