package org.v2.finance.expenses.port;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import static org.v2.finance.expenses.constants.Constants.BILLING_DIRECTORY_NAME;
import static org.v2.finance.expenses.constants.Constants.BILLING_FILE_EXTENSION;
import static org.v2.finance.expenses.constants.Constants.BILLING_FILE_NAME_PREFIX;
import static org.v2.finance.expenses.constants.Constants.FILE_DIRECTORY_HIERARCHY;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;
import static org.v2.finance.expenses.util.Helper.getFilePathPrefixInternalStorage;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 24-09-2018
 * @since 24-09-2018
 *
 * A single file exporter, but never used -- need need to visit the requirement of this class, till then, Deprecated
 */
@Deprecated
public class Exporter {

    private final Logger logger = LoggerFactory.getLogger(Exporter.class);

    public Exporter(String data, String date) {
        StringBuilder fp = new StringBuilder();
        fp.append(getFilePathPrefixInternalStorage()).append(FILE_DIRECTORY_HIERARCHY).append(BILLING_DIRECTORY_NAME);
        String filePath = fp.toString();
        File file = new File(filePath);
        if (!file.exists()) {
            if (!file.mkdir()) {
                logger.error("Failed to export due to failure of creation of file {}", file);
                return;
            }
        }

        fp.append(FILE_DIRECTORY_HIERARCHY).append(BILLING_FILE_NAME_PREFIX).append(date).append(BILLING_FILE_EXTENSION);
        filePath = fp.toString();
        logger.info("Destination filepath : {}", filePath);

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(filePath))) {
            printWriter.write(data);
            printWriter.write(NEW_LINE);
        } catch (Exception e1) {
            logger.error("Exception occurred whilst writing, writing failed... ", e1);
        }
    }
}
