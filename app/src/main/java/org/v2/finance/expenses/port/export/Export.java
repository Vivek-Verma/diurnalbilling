package org.v2.finance.expenses.port.export;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-11-2018
 * @since 17-11-2018
 */
public class Export {

    private final Logger logger = LoggerFactory.getLogger(Export.class);

    private final String dataToBeExported;
    private final String pathOfFileToBeWrittenTo;
    private final File fileToBeWrittenTo;

    public Export(String pathOfFileToBeWrittenTo, String dataToBeExported) {
        this.pathOfFileToBeWrittenTo = pathOfFileToBeWrittenTo;
        this.dataToBeExported = dataToBeExported;

        this.fileToBeWrittenTo = new File(pathOfFileToBeWrittenTo);
    }

    public Export(File fileToBeWrittenTo, String dataToBeExported) {
        this.fileToBeWrittenTo = fileToBeWrittenTo;
        this.dataToBeExported = dataToBeExported;

        this.pathOfFileToBeWrittenTo = fileToBeWrittenTo.getAbsolutePath();
    }

    public boolean export() {
        //delete existing file first
        if (fileToBeWrittenTo.exists()) {
            if (!fileToBeWrittenTo.delete()) {
                if (BuildConfig.DEBUG) logger.error("Failed to delete file {} before exporting to it...", fileToBeWrittenTo);
            }
        }
        if (BuildConfig.DEBUG) logger.info("Exporting to {} ", fileToBeWrittenTo);
        //now proceed to creating new file
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileToBeWrittenTo))) {
            printWriter.write(dataToBeExported.trim());
            printWriter.close();
            return true;
        } catch (IOException e) {
            if (BuildConfig.DEBUG) logger.error("Error occurred while writing the file. Error : ", e);
        }
        return false;
    }
}
