package org.v2.finance.expenses.ui;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_DESCRIPTION;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_AMOMUNT;
import static org.v2.finance.expenses.constants.Constants.KEY_TO_DAILY_ENTRY_DATE_FROM_UP;
import static org.v2.finance.expenses.constants.Constants.ROUTING_FROM_ENTRY;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_NEW_ENTRY;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_VIEW_ENTRY;
import static org.v2.finance.expenses.util.Helper.getLayOverEntry;
import static org.v2.finance.expenses.util.Helper.getLayOverEntryOfADay;
import static org.v2.finance.expenses.util.Helper.getRoutingPath;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 25-09-2018
 */
public class ViewEntryRecord extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private final Logger LOGGER = LoggerFactory.getLogger(ViewEntryRecord.class);

    TextView displayEntryDate;
    TextView displayEntryDayOfWeek;
    RadioGroup radioGroupEntryType;
    RadioButton radioPositive;
    RadioButton radioNegative;
    RadioButton radioComment;
    TextView entryDisplayCurrencySign;
    EditText textEntryAmount;
    EditText textEntryDescription;
    Button buttonOk;
    Button buttonCancel;
    Button buttonReset;

    private final Map<Integer, String> radioEntryIntNameMapping = new HashMap<>();
    static Entry entry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_record);

        displayEntryDate = findViewById(R.id.displayEntryDate);
        displayEntryDayOfWeek = findViewById(R.id.displayEntryDayOfWeek);

        entryDisplayCurrencySign = findViewById(R.id.entryDisplayCurrencySign);
        textEntryAmount = findViewById(R.id.textEntryAmount);
        textEntryDescription = findViewById(R.id.textEntryDescription);

        radioGroupEntryType = findViewById(R.id.entryTypeGroup);
        radioPositive = findViewById(R.id.radioPositive);
        radioNegative = findViewById(R.id.radioNegative);
        radioComment = findViewById(R.id.radioComment);

        populateRadioEntryIntNameMapping();

        switchOnNegativeSwitch();
        radioNegative.setOnCheckedChangeListener(this);
        radioPositive.setOnCheckedChangeListener(this);
        radioComment.setOnCheckedChangeListener(this);

        buttonOk = findViewById(R.id.buttonOk);
        buttonCancel = findViewById(R.id.buttonCancel);
        buttonReset = findViewById(R.id.buttonReset);

        buttonOk.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        buttonReset.setOnClickListener(this);

        entryDisplayCurrencySign.setText(CurrencyUtil.currencySignToDisplay);

        int receivedRoutingPath = getRoutingPath();
        //entry = (Entry) getIntent().getSerializableExtra(KEY_TO_DAILY_ENTRY);
        entry = getLayOverEntry();
        if (entry == null) {
            entry = new Entry();
            if (BuildConfig.DEBUG) LOGGER.info("Creating new entry object... entry : {}", entry);
        }

        if (receivedRoutingPath == ROUTING_TO_NEW_ENTRY) {

        } else if (receivedRoutingPath == ROUTING_TO_VIEW_ENTRY) {
            textEntryAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, entry.getAmount()));
            textEntryDescription.setText(entry.getDescription());

            if (ENTRY_TYPE_POSITIVE.equals(entry.getEntryType())) {
                switchOnPositiveSwitch();
            } else if (ENTRY_TYPE_COMMENT.equals(entry.getEntryType())) {
                switchOnCommentSwitch();
            }
        }

        String date = (String) getIntent().getSerializableExtra(KEY_TO_DAILY_ENTRY_DATE_FROM_UP);
        displayEntryDate.setText(date);
        String dayOfWeek = Helper.mapOfDisplayDateAndDayOfWeek.get(date);
        if (dayOfWeek == null || dayOfWeek.isEmpty())
            dayOfWeek = Helper.updateMapOfDisplayDateAndDayOfWeek(date);
        displayEntryDayOfWeek.setText(dayOfWeek);
    }

    private void populateRadioEntryIntNameMapping() {
        radioEntryIntNameMapping.put(radioNegative.getId(), String.valueOf(radioNegative.getText()));
        radioEntryIntNameMapping.put(radioPositive.getId(), String.valueOf(radioPositive.getText()));
        radioEntryIntNameMapping.put(radioComment.getId(), String.valueOf(radioComment.getText()));
    }

    private boolean isEditTextInvalid(Editable description) {
        //logger.info("From inside the invalidate function, description : {}", description);
        return description == null || description.toString() == null || description.toString().trim().length() == 0;
    }

    @Override
    public void onClick(View view) {
        if (view == buttonOk) {
            //apply verification filters here
            String currency = entryDisplayCurrencySign.getText().toString();
            String entryType = radioEntryIntNameMapping.get(radioGroupEntryType.getCheckedRadioButtonId());

            Editable amtStr = textEntryAmount.getText();
            if (BuildConfig.DEBUG) LOGGER.info("amtstr : {}", amtStr);
            if (!ENTRY_TYPE_COMMENT.equals(entryType) && isEditTextInvalid(amtStr)) {
                ToasterUtil.shortToaster(this, "Amount cannot be 0!");
                return;
            }

            Editable description = textEntryDescription.getText();
            String descriptionTxt;
            if (isEditTextInvalid(description)) {
                LOGGER.warn("Suppressing empty description error with default value '{}'!", DEFAULT_DESCRIPTION);
                descriptionTxt = DEFAULT_DESCRIPTION;
            } else {
                descriptionTxt = description.toString();
            }

            double amount = Double.parseDouble(amtStr.toString());
            if (ENTRY_TYPE_COMMENT.equals(entryType)) {
                amount = DEFAULT_AMOUNT;
            }

            if (BuildConfig.DEBUG)
                LOGGER.info("Entry val : {}, currency: {}, descr: {}, amt in edit text: {}", entry, currency, descriptionTxt, amount);

            entry.setCurrency(currency);
            entry.setEntryType(entryType);
            entry.setDescription(descriptionTxt);
            entry.setAmount(Double.parseDouble(textEntryAmount.getText().toString()));

            if (!ENTRY_TYPE_COMMENT.equals(entry.getEntryType()))
                entry.setEntryNormal(true);

            if (getLayOverEntry() == null) { //i.e. this was a new entry
                EntryOfADay localEntryOfADay = getLayOverEntryOfADay();
                LinkedList<Entry> entryLinkedList = localEntryOfADay.getDailyEntries();
                entryLinkedList.add(entry);
                localEntryOfADay.setDailyEntries(entryLinkedList);
                Helper.layOverEntryOfADay = localEntryOfADay;
            }

            ToasterUtil.shortToaster(this, "Completed the details uploading!");
            finish();
        } else if (view == buttonCancel) {
            finish();
        } else if (view == buttonReset) {
            switchOnNegativeSwitch();
            textEntryAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, DEFAULT_AMOUNT));
            textEntryDescription.setText(EMPTY_STR);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Helper.ROUTING_PATH = ROUTING_FROM_ENTRY;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if ((buttonView == radioNegative || buttonView == radioPositive) && (radioNegative.isChecked() || radioPositive.isChecked())) {
            textEntryAmount.setEnabled(true);
            entry.setEntryNormal(true);
        } else if (buttonView == radioComment && radioComment.isChecked()) {
            textEntryAmount.setEnabled(false);
            textEntryAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, DEFAULT_AMOUNT));
            entry.setEntryNormal(false);
        }
    }

    private void switchOnPositiveSwitch() {
        radioPositive.setChecked(true);
    }

    private void switchOnNegativeSwitch() {
        radioNegative.setChecked(true);
    }

    private void switchOnCommentSwitch() {
        radioComment.setChecked(true);
    }
}
