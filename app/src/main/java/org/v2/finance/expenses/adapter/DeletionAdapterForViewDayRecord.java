package org.v2.finance.expenses.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.ui.ViewDayRecord;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 12-06-2019
 * @since 12-06-2019
 */
public class DeletionAdapterForViewDayRecord extends AbstractDeletionAdapter {

    private final Logger logger = LoggerFactory.getLogger(DeletionAdapterForViewDayRecord.class);

    public <T> DeletionAdapterForViewDayRecord(Context context, int layoutToBeFollowed, T[] resource) {
        super(context, layoutToBeFollowed, resource);
        if (BuildConfig.DEBUG) logger.info("Resource's size as received in view day rec's custom adapter : {}", resource.length);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (BuildConfig.DEBUG) logger.info("Entering the getView part of the DeletionAdapterForViewDayRecord");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        convertView = inflater.inflate(layoutToBeFollowed, parent, false);

        CheckBox checkBox = convertView.findViewById(R.id.list_check_row);
        checkBox.setText(modelItems[position].getName());

        checkBox.setOnCheckedChangeListener((compoundButton, isSelected) -> {
            if (isSelected) {
                ViewDayRecord.getItemPositionsToBeDeleted().add(position);
            } else {
                ViewDayRecord.getItemPositionsToBeDeleted().remove(position);
            }
        });
        checkBox.setChecked(modelItems[position].isCheckboxActive());
        return convertView;
    }
}
