package org.v2.finance.expenses.port.importer;

import android.content.Context;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.PortPrefs;

import static android.content.Context.MODE_PRIVATE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_CURRENCY;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_PREMIUM_USER_STATUS;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_CRED_HASH;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_EMAIL;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_ISD_CODE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_MOBILE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_NAME;
import static org.v2.finance.expenses.constants.Constants.KEY_FLOAT_CURRENT_MONTH_EXPENSE;
import static org.v2.finance.expenses.constants.Constants.KEY_INT_USER_ISD;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_CLOUD_UPLOAD_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_LAUNCHED_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_SAVED_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_PAYMENT_EXPIRY_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_USER_CRED_HASH_EXPIRY_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_USER_MOBILE;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_CURRENCY;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_PREMIUM_USER;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_CRED_HASH;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_EMAIL;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_NAME;
import static org.v2.finance.expenses.constants.Constants.MY_PREFS_NAME;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 03-06-2019
 * Acts as the populator for local internalSharedPrefs from the actual internal cache -- to be used for setting data in wrapper (local shared pref)
 */
public class ImportToLocalPrefs extends PortPrefs {

    public ImportToLocalPrefs(Context context, LocalSavedPrefs localSavedPrefs) {
        super(context, context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE), localSavedPrefs);
    }

    public void populateLocalPrefsFromActualInternalPrefs() {
        localSavedPrefs.setLastUsedTimeStamp(actualInternalCache.getLong(KEY_LONG_LAST_LAUNCHED_TIMESTAMP, DEFAULT_TIMESTAMP));
        localSavedPrefs.setCurrentMonthExpense(actualInternalCache.getFloat(KEY_FLOAT_CURRENT_MONTH_EXPENSE, (float) DEFAULT_AMOUNT));
        localSavedPrefs.setCurrency(UserMappingProto.Currency.valueOf(
                actualInternalCache.getString(KEY_STR_CURRENCY, DEFAULT_CURRENCY.name())));

        localSavedPrefs.setUserName(actualInternalCache.getString(KEY_STR_USER_NAME, DEFAULT_USER_NAME));
        localSavedPrefs.setPremiumUserStatus(actualInternalCache.getBoolean(KEY_STR_PREMIUM_USER, DEFAULT_PREMIUM_USER_STATUS));
        localSavedPrefs.setUserEmail(actualInternalCache.getString(KEY_STR_USER_EMAIL, DEFAULT_USER_EMAIL));
        localSavedPrefs.setUserIsdCode(actualInternalCache.getInt(KEY_INT_USER_ISD, DEFAULT_USER_ISD_CODE));
        localSavedPrefs.setUserMobile(actualInternalCache.getLong(KEY_LONG_USER_MOBILE, DEFAULT_USER_MOBILE));
        localSavedPrefs.setUserCredHash(actualInternalCache.getString(KEY_STR_USER_CRED_HASH, DEFAULT_USER_CRED_HASH));
        localSavedPrefs.setUserCredHashExpiryTimestamp(actualInternalCache.getLong(KEY_LONG_USER_CRED_HASH_EXPIRY_TIMESTAMP, DEFAULT_TIMESTAMP));
        localSavedPrefs.setLastSavedTimeStamp(actualInternalCache.getLong(KEY_LONG_LAST_SAVED_TIMESTAMP, DEFAULT_TIMESTAMP));
        localSavedPrefs.setLastCloudUploadTimeStamp(actualInternalCache.getLong(KEY_LONG_LAST_CLOUD_UPLOAD_TIMESTAMP, DEFAULT_TIMESTAMP));
        localSavedPrefs.setPaymentExpiryTimestamp(actualInternalCache.getLong(KEY_LONG_PAYMENT_EXPIRY_TIMESTAMP, DEFAULT_TIMESTAMP));
    }

}
