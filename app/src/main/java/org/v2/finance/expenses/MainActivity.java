package org.v2.finance.expenses;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.adapter.TotalBillingListAdapter;
import org.v2.finance.expenses.ads.InterstitialAd;
import org.v2.finance.expenses.connectivity.SettingsPinger;
import org.v2.finance.expenses.connectivity.TestConnectivity;
import org.v2.finance.expenses.connectivity.VersionCheckPinger;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.contracts.DatePickerContract;
import org.v2.finance.expenses.contracts.DeletionContract;
import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;
import org.v2.finance.expenses.contracts.TestConnectivityConnector;
import org.v2.finance.expenses.core.EntireBilling;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.core.VersionCheck;
import org.v2.finance.expenses.core.settings.SettingsFromGh;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.monthly.MonthlyExpenseCalculator;
import org.v2.finance.expenses.notification.Notifier;
import org.v2.finance.expenses.port.export.ExportAll;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.importer.HeavyImporter;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.transporters.TransportBillingAmount;
import org.v2.finance.expenses.ui.Settings;
import org.v2.finance.expenses.ui.alerter.AlerterForAdClick;
import org.v2.finance.expenses.ui.alerter.AlerterForFastHelp;
import org.v2.finance.expenses.ui.alerter.AlerterForRestore;
import org.v2.finance.expenses.ui.dialog.AbstractDialog;
import org.v2.finance.expenses.ui.dialog.DeletionDialog;
import org.v2.finance.expenses.ui.dialog.MonthlyExpenseDialog;
import org.v2.finance.expenses.ui.fragment.picker.DatePickerFragment;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ModelForDeletion;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import static org.v2.finance.expenses.constants.Constants.ADS_ACTIVATE;
import static org.v2.finance.expenses.constants.Constants.ADS_ADMOB_ID_INTERSTITIAL;
import static org.v2.finance.expenses.constants.Constants.ADS_INTERSTITIAL_SCHEDULED_POLLING_FOR_AD_REQ;
import static org.v2.finance.expenses.constants.Constants.AD_SPREAD_INTERVAL;
import static org.v2.finance.expenses.constants.Constants.DATEPICKER_NAME;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_HOST_FOR_PINGING;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_INT_VALUE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_LONG_VALUE;
import static org.v2.finance.expenses.constants.Constants.DOWNLOADED_APK_NAME;
import static org.v2.finance.expenses.constants.Constants.DOWNLOAD_LOCATION_EXTERNAL;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.ERR_WHILE_GDRIVE_CHECK;
import static org.v2.finance.expenses.constants.Constants.FILE_HEADER_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_AMOMUNT;
import static org.v2.finance.expenses.constants.Constants.FORMAT_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.GRAY;
import static org.v2.finance.expenses.constants.Constants.GREEN_DARK;
import static org.v2.finance.expenses.constants.Constants.ID_SITE_REMOTE_SETTINGS;
import static org.v2.finance.expenses.constants.Constants.ID_SITE_VERSION_CHECK;
import static org.v2.finance.expenses.constants.Constants.INTERNAL_FOLDER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.MAX_SAVES_ALLOWED_BEFORE_ROLL;
import static org.v2.finance.expenses.constants.Constants.RED;
import static org.v2.finance.expenses.constants.Constants.TERSE_HELP_MSG;
import static org.v2.finance.expenses.util.Helper.crackUpTheMonthRow;
import static org.v2.finance.expenses.util.Helper.createAndFireApkCheckAlarm;
import static org.v2.finance.expenses.util.Helper.createAndFireDailyEntryAlarm;
import static org.v2.finance.expenses.util.Helper.extractEOADInOrder;
import static org.v2.finance.expenses.util.Helper.extractValuesFromMapInOrder;
import static org.v2.finance.expenses.util.Helper.fireUpBannerAd;
import static org.v2.finance.expenses.util.Helper.getCurrentDate;
import static org.v2.finance.expenses.util.Helper.getCurrentTime;
import static org.v2.finance.expenses.util.Helper.getIndexFromDateForList;
import static org.v2.finance.expenses.util.Helper.getLayOverIndexFromMain;
import static org.v2.finance.expenses.util.Helper.getRoutingPath;
import static org.v2.finance.expenses.util.Helper.handleChangeLogUpdate;
import static org.v2.finance.expenses.util.Helper.handleRemoteSettingsUpdate;
import static org.v2.finance.expenses.util.Helper.isAlarmActiveForApkCheck;
import static org.v2.finance.expenses.util.Helper.isAlarmActiveForDailyEntryCheck;
import static org.v2.finance.expenses.util.Helper.isNewDay;
import static org.v2.finance.expenses.util.Helper.isSaveDoneAtleastOnce;
import static org.v2.finance.expenses.util.Helper.mapOfDateAndDailyEntries;
import static org.v2.finance.expenses.util.Helper.mapOfSavesVsAdShown;
import static org.v2.finance.expenses.util.Helper.monthlyExpenseList;
import static org.v2.finance.expenses.util.Helper.numberOfSavesPerformed;
import static org.v2.finance.expenses.util.Helper.performHeavyImporting;
import static org.v2.finance.expenses.util.Helper.scheduledInsttAdExecutorService;
import static org.v2.finance.expenses.util.Helper.wasAdNotDisplayedOnTime;
import static org.v2.finance.expenses.util.ToasterUtil.longToaster;
import static org.v2.finance.expenses.util.ToasterUtil.shortToaster;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 20-09-2018
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, TestConnectivityConnector, GDriveConnector, DeletionContract, DatePickerContract {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainActivity.class);

    public final Activity context = this;
    private final ActivityStarter activityStarter = new ActivityStarter(context);

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    private TextView grandTotal;
    private TextView displayGrandTotalSign;
    private TextView displayGrandTotalCurrency;
    private TextView displayGrandTotal;
    private ImageButton addNewRecord;
    private ImageButton grandDeleteRecords;
    private ListView billingListView;

    private FloatingActionButton fabAddNewRecord;

    private String innerDirectoryPath = "";

    private TreeMap<Integer, String> mapOfIndexAndDate; //map of index and date (dd-MM-yyyy format)
    private Map<String, Integer> reverseMapOfIndexAndDate; //reverse map of mapOfIndexAndDate; created as the BiMap for the same couldn't be created because the order of the key was important for the map 'mapOfIndexAndDate'
    private EntireBilling entireBilling;

    private LocalSavedPrefs localSavedPrefs;
    private ImportToLocalPrefs importToLocalPrefs;
    private ExportToInternalPrefs exportToInternalCache;
    private PropertyReader properties;

    private ViewGroup defViewGroup;

    private static long resumeHits = 0;
    private PublisherInterstitialAd interstitialAd;

    private Notifier notifier;

    private TextView navigationSidebarTitle;
    private TextView navigationSidebarEmail;

    //for deletion part
    private static final Set<Integer> grandDateRecordsToBePurged = new TreeSet<>();

    private static boolean decideToDisplayAd() {
//        return mapOfSavesVsAdShown.containsKey(numberOfSavesPerformed) && !mapOfSavesVsAdShown.get(numberOfSavesPerformed)
//                || (wasAdNotDisplayedOnTime.get() && overrideAndDisplayAd.get());
        if (BuildConfig.DEBUG) {
            LOGGER.info("Ad map : {}", mapOfSavesVsAdShown.toString());
            LOGGER.info("wasAdNotDisplayedOnTime : {}", wasAdNotDisplayedOnTime.get());
        }
        return wasAdNotDisplayedOnTime.get() || (mapOfSavesVsAdShown.containsKey(numberOfSavesPerformed) && !mapOfSavesVsAdShown.get(numberOfSavesPerformed));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LOGGER.info("STARTING POINT OF THE MAIN ACTIVITY!!!!");
        //Helper.properties = new PropertyReader(context, getString(R.string.diurnal_properties)); //TODO -- remove this later and consume the one from InitLoader / SignIn
        properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
        if (properties == null) { // if MainActivity not launched from BAU flow
            properties = new PropertyReader(context, getString(R.string.diurnal_properties));
        }
        if (BuildConfig.DEBUG) LOGGER.info("Helper properties: {}", properties);

        initAdmobAds();
        crankUpTheAds();
        notifier = new Notifier(context, getDummyIntent());
        fireUpAlarmBasedNotificationPatrolling();

        localSavedPrefs = (LocalSavedPrefs) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
        boolean runPopulate = false;
        if (localSavedPrefs == null) { // if MainActivity not launched from BAU flow
            localSavedPrefs = new LocalSavedPrefs();
            runPopulate = true;
        }
        importToLocalPrefs = new ImportToLocalPrefs(context, localSavedPrefs);
        exportToInternalCache = new ExportToInternalPrefs(context, localSavedPrefs);
        if (runPopulate) { // if MainActivity not launched from BAU flow
            importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();
        } else if (BuildConfig.DEBUG) {
            LOGGER.info("De-serialized saved preferences from InitLoader => {}", localSavedPrefs.getLastUsedTimeStamp());
        }
        CurrencyUtil.currencyToApply = localSavedPrefs.getCurrency();
        CurrencyUtil.currencySignToDisplay = CurrencyUtil.CURRENCY_SIGN.get(CurrencyUtil.currencyToApply); //run init on curr sign to display
        Helper.setIsPremiumUser(localSavedPrefs.isPremiumUser());
        Helper.setUserCurrency(localSavedPrefs.getCurrency());

        mapOfIndexAndDate = new TreeMap<>();
        reverseMapOfIndexAndDate = new HashMap<>();
        Helper.mapOfDisplayDateAndFileTitleDate = new HashMap<>();
        Helper.mapOfDisplayDateAndDayOfWeek = new HashMap<>();

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(context, drawerLayout, R.string.app_name, R.string.app_name);
        navigationView = findViewById(R.id.navigation_view);

        grandTotal = findViewById(R.id.grand_total_text);
        displayGrandTotalSign = findViewById(R.id.displayGrandTotalSign);
        displayGrandTotalCurrency = findViewById(R.id.displayGrandTotalCurrency);
        displayGrandTotal = findViewById(R.id.displayGrandTotal);
        addNewRecord = findViewById(R.id.addNewRecord);
        grandDeleteRecords = findViewById(R.id.grand_delete_date_records);
        billingListView = findViewById(R.id.billingList);

        defViewGroup = findViewById(android.R.id.content);

        fabAddNewRecord = findViewById(R.id.fabAddNewRecord);
        fabAddNewRecord.setOnClickListener(this);

        addNewRecord.setOnClickListener(this);
        grandDeleteRecords.setOnClickListener(this);

        View headerView = navigationView.getHeaderView(0);
        navigationSidebarTitle = headerView.findViewById(R.id.navigationSidebarTitle);
        navigationSidebarTitle.setText(String.format("Hi, %s", localSavedPrefs.getUserName().trim()));
        navigationSidebarEmail = headerView.findViewById(R.id.navigationSidebarEmail);
        navigationSidebarEmail.setText(localSavedPrefs.getUserEmail());

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        try {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException ignored) {
        }
        handleNavigationClick();

        Helper.setFilePathPrefixInternalStorage(getFilesDir().toString());
        innerDirectoryPath = INTERNAL_FOLDER;

        //renameFile();
        connectorToFullHeavyImport();
        fireDiurnalTasks();
        if (isAppLaunchedForFirstTime()) actionFirstTimeLaunch();
        else getMonthlyExpense(true);
    }

    private void runUpdaterSteps() {
        checkInternetAndConnectToUpdateCheckerAndSettingsImporter();
        deleteExistingApk();
    }

    private void fireUpAlarmBasedNotificationPatrolling() {
        //For apk check
        try {
            if (!isAlarmActiveForApkCheck(context)) createAndFireApkCheckAlarm(context);
            else {
                if (BuildConfig.DEBUG) LOGGER.info("Not setting up alarm, as it already exists!");
            }
        } catch (Exception e) {
            LOGGER.error("Failed to set up the alarm activity for apk version checking! Error : ", e);
        }

        //For checking for daily entry
        try {
            if (!isAlarmActiveForDailyEntryCheck(context)) createAndFireDailyEntryAlarm(context);
            else {
                if (BuildConfig.DEBUG)
                    LOGGER.info("Not setting up daily entry check alarm, as it already exists!");
            }
        } catch (Exception e) {
            LOGGER.error("Failed to set up the alarm activity for daily entry checking! Error : ", e);
        }
    }

    /**
     * Contains tasks that need to run only once a day!
     */
    private void fireDiurnalTasks() {
        runUpdaterSteps();
        if (!isAppLaunchedForFirstTime()) {
            if (isNewDay(localSavedPrefs.getLastUsedTimeStamp())) {
                if (BuildConfig.DEBUG)
                    LOGGER.info("Registered a new day when the app was called in! Will be performing the daily tasks!");
                //runUpdaterSteps(); //removing from one time only to every time the app is launched
                new AlerterForAdClick(context, "Prompt", Constants.AD_CLICK_REQ_MSG);
            } else {
                if (BuildConfig.DEBUG)
                    LOGGER.info("Same day act, diurnal tasks will NOT be fired!");
            }
        }
    }

    private void crankUpTheAds() {
        Helper.areAdsToBeDisplayed = properties.getBooleanProperty(ADS_ACTIVATE, true);
        if (BuildConfig.DEBUG) LOGGER.info("areAdsToBeDisplayed => {}", Helper.areAdsToBeDisplayed);

        InterstitialAd interstitialAd = new InterstitialAd(context,
                properties.getProperty(ADS_ADMOB_ID_INTERSTITIAL),
                properties.getIntProperty(ADS_INTERSTITIAL_SCHEDULED_POLLING_FOR_AD_REQ, 40));
        this.interstitialAd = interstitialAd.getInterstitialAd();
        populateTheSavesVsAdsMap();

        fireUpBannerAd(context, R.id.banner_ad);
        if (Helper.areAdsToBeDisplayed) {
            interstitialAd.assignAd(); //Not firing interstitial ads if switched off from config
        }
    }

    public void fireNewRecord(String requiredDate) {
        LOGGER.info("Firing new record for date : {}", requiredDate);
        activityStarter.createNewDayRecord(requiredDate, localSavedPrefs);
    }

    private Intent getDummyIntent() {
        return new Intent(String.valueOf(Constants.NotificationCaller.APK_UPDATE_CHECK));
    }

    private void populateTheSavesVsAdsMap() {
        mapOfSavesVsAdShown.clear();
        mapOfSavesVsAdShown.put(1, false);
        for (int i = AD_SPREAD_INTERVAL; i <= MAX_SAVES_ALLOWED_BEFORE_ROLL; i += AD_SPREAD_INTERVAL)
            mapOfSavesVsAdShown.put(i, false);
    }

    private void deleteExistingApk() {
        try {
            File file = new File(DOWNLOAD_LOCATION_EXTERNAL + DOWNLOADED_APK_NAME);
            if (file.exists()) {
                if (file.delete()) {
                    if (BuildConfig.DEBUG) LOGGER.info("Apk from Download deleted!");
                } else {
                    if (BuildConfig.DEBUG) LOGGER.warn("Could not delete Apk from Download!");
                }
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG)
                LOGGER.warn("Couldn't delete the existing apk from Download/. Error : ", e);
        }
    }

    private void initAdmobAds() {
        try {
            MobileAds.initialize(context, initializationStatus -> LOGGER.info("Init completed:: " + initializationStatus.toString()));
            LOGGER.info("Mobile Ads initialized.");

        } catch (Exception e) {
            LOGGER.error("The mobile ads init failed!! Error : ", e);
        }
    }

    private void performMapSafetyCheck() {
        if (numberOfSavesPerformed > MAX_SAVES_ALLOWED_BEFORE_ROLL) {
            LOGGER.warn("Will be modding the number of saves performed now! Dependencies will be affected!");
            numberOfSavesPerformed = numberOfSavesPerformed % MAX_SAVES_ALLOWED_BEFORE_ROLL;
            populateTheSavesVsAdsMap();
        }
    }

    private void fireInterstitialIfRequired() {
        LOGGER.info("Number of SAVES performed : {} ", numberOfSavesPerformed);
        performMapSafetyCheck(); //this ensures that the map will never give NPE
        if (decideToDisplayAd()) {
            LOGGER.info("Going to display interstitial ad if loaded");
            if (interstitialAd.isLoaded()) {
                wasAdNotDisplayedOnTime.set(false);
                mapOfSavesVsAdShown.put(numberOfSavesPerformed, true); // might get updated with some non original keys, shouldn't be a problem
                interstitialAd.show();
            } else {
                if (BuildConfig.DEBUG) LOGGER.warn("Interstitial ad not loaded yet!");
                wasAdNotDisplayedOnTime.set(true);
            }
            if (BuildConfig.DEBUG)
                LOGGER.info("Flag wasAdNotDisplayedOnTime is set to '{}'", wasAdNotDisplayedOnTime);
        } else {
            if (BuildConfig.DEBUG) LOGGER.info("Not displaying ad for the above save count!");
        }
    }

    private void closeCentralDrawer() {
        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeCentralDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        closeCentralDrawer();
        updateLastUsedTimeStamp();
        resumeHits++;
        if (BuildConfig.DEBUG) {
            LOGGER.info("Resume hits is now : {}, The routing path is : {}", resumeHits, getRoutingPath());
        }

        //if (getRoutingPath() == ROUTING_FROM_DAY_RECORD) {
        if (BuildConfig.DEBUG) LOGGER.info("Perform heavy importing : {}", performHeavyImporting());
        if (performHeavyImporting()) {
            if (BuildConfig.DEBUG) LOGGER.info("Proceeding with heavy import from onresume");
            connectorToFullHeavyImport();
            getMonthlyExpense(false);

            if (getLayOverIndexFromMain() != -1) {
                if (BuildConfig.DEBUG)
                    LOGGER.info("The layover index inside the onResume function : {}", getLayOverIndexFromMain());
                billingListView.setSelection(getLayOverIndexFromMain());
            }
        }
        fireInterstitialIfRequired();
        //runUpdaterSteps();
        deleteExistingApk();
    }

    private void updateLastUsedTimeStamp() {
        localSavedPrefs.setLastUsedTimeStamp(getCurrentTime());
        exportToInternalCache.writeLastUsedTime();
    }

    private void updateCurrentMonthExpense(String currentMonthRow) {
        TransportBillingAmount transportBillingAmount = crackUpTheMonthRow(currentMonthRow);
        try {
            localSavedPrefs.setCurrentMonthExpense(transportBillingAmount.getActualAmount());
            exportToInternalCache.writeCurrentMonthExpense();
        } catch (Exception e) {
            LOGGER.error("Failed to deserialize Month row for updation 1!");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isSaveDoneAtleastOnce) {
            if (BuildConfig.DEBUG) LOGGER.info("Pumping up the backup procedure in onDestroy!!");
            pumpOutTheBackup();
        }
        internalMapClearer(true);
        updateLastUsedTimeStamp();
        scheduledInsttAdExecutorService.shutdownNow();
        resumeHits = 0;
        Helper.layOverIndexFromMain = DEFAULT_INT_VALUE;
        //Helper.searchAndKillApkCheckAlarm(context); //TODO -- to be removed before final push
    }

    // for the list view clicking
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (BuildConfig.DEBUG)
            LOGGER.info("Shouting from the index click of MainActivity position : {}", position);
        activityStarter.viewExistingDayRecord(position, getEntryOfADayFromPosition(position), true, localSavedPrefs);
    }

    public static Set<Integer> getGrandDateRecordsToBePurged() {
        return grandDateRecordsToBePurged;
    }

    private EntryOfADay getEntryOfADayFromPosition(int position) {
        return mapOfDateAndDailyEntries.get(mapOfIndexAndDate.get(position));
    }

    @Override
    public void handleSelectedDate(String date) {
        if (!mapOfDateAndDailyEntries.containsKey(date)) {
            fireNewRecord(date);
        } else {
            Integer requiredIndex = getIndexFromDateForList(date, reverseMapOfIndexAndDate);
            if (requiredIndex != null) {
                activityStarter.viewExistingDayRecord(requiredIndex, getEntryOfADayFromPosition(requiredIndex), true, localSavedPrefs);
            } else {
                if (BuildConfig.DEBUG)
                    LOGGER.warn("Routing to new record for above as reverseMapOfIndexAndDate didn't contain {}!", date);
                fireNewRecord(date);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == addNewRecord) {
            DialogFragment datePickerFragment = new DatePickerFragment(context, Constants.Caller.CALLER_MAIN_ACTIVITY);
            datePickerFragment.show(getFragmentManager(), DATEPICKER_NAME);
        } else if (view == fabAddNewRecord) { //fab based
            handleSelectedDate(getCurrentDate());
        } else if (view == grandDeleteRecords) {
            //central deletion logic goes here
            centralProcessorForDeletion();
        }
    }

    private void handleNavigationClick() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            int id = menuItem.getItemId();
            switch (id) {
                case R.id.action_settings:
                    activityStarter.launchGenericScreenWithNoPropsIntactStack(localSavedPrefs, Settings.class);
                    return true;
                case R.id.action_search:
                    activityStarter.fireSearchQuery(localSavedPrefs, properties);
                    break;
                case R.id.action_showMonthly:
                    getMonthlyExpense(true);
                    return true;
                case R.id.action_showMonthlyGraph:
                    activityStarter.fireExpenseGraph();
                    return true;
                case R.id.action_backup:
                    pumpOutTheBackup();
                    closeCentralDrawer();
                    break;
                case R.id.action_restore_using_bk:
                    new AlerterForRestore(context, "Restore (Everything will be overwritten from Backup!)", null, localSavedPrefs, exportToInternalCache)
                            .showAlert("YES", EMPTY_STR, "NO", "Emergency restore denied by user!");
                    //yet to test the scenario in which it is unable to find the backup file on the file sys (not permission denied)
                    break;
                case R.id.action_about:
                    activityStarter.fireAbout(properties);
                    return true;
                case R.id.action_updater:
                    Helper.tempMapOfIndexAndDate = new TreeMap<>(mapOfIndexAndDate);
                    activityStarter.fireUpdater();
                    return true;
                case R.id.action_help:
                    activityStarter.fireHelp();
                    return true;
                case R.id.action_changelogs:
                    activityStarter.fireChangeLog();
                    return true;
            }
            return true;
        });
    }

    private void pumpOutTheBackup() {
        new ExportAll(context, extractValuesFromMapInOrder(mapOfIndexAndDate), localSavedPrefs, exportToInternalCache, properties)
                .fireHeavyExport();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (drawerToggle.onOptionsItemSelected(item)) return true;
        return super.onOptionsItemSelected(item);
    }

    private void renameFile() {
        File file = new File("/data/user/0/org.v2.outlet.dailyTrackExpz/files/billingRes/bill_2018_11_9");
        File file2 = new File("/data/user/0/org.v2.outlet.dailyTrackExpz/files/billingRes/bill_2018_11_09");
        if (BuildConfig.DEBUG) {
            if (file.renameTo(file2)) {
                LOGGER.info("File renamed successfully!!");
            } else LOGGER.error("File renaming failed!!");
        }
    }

    private boolean isAppLaunchedForFirstTime() {
        return localSavedPrefs.getLastUsedTimeStamp().equals(DEFAULT_LONG_VALUE);
    }

    private void actionFirstTimeLaunch() {
        updateLastUsedTimeStamp();
        LOGGER.info("First time loaded the app!");
        longToaster(context, "Welcome to the app!");

        drawerLayout.openDrawer(GravityCompat.START);

        //as it's a first time launch, so short help -- the handler of OK needs to be defined in mainUI thread, hence cannot put in the below thread
        new AlerterForFastHelp(context, "Super Jump Starter", TERSE_HELP_MSG);

        //not used post D1.0.9.2
        new Thread(() -> {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                if (BuildConfig.DEBUG)
                    LOGGER.warn("sleep of 2 minutes interrupted while touring for 1st time!");
            }
            //activityStarter.fireHelp(); //as it's a first time launch, so directing to the help page
        }).start();
    }

    private void internalMapClearer(boolean clearAdMap) {
        //clearing all maps here
        try {
            mapOfIndexAndDate.clear();
            reverseMapOfIndexAndDate.clear();
            mapOfDateAndDailyEntries.clear();
            Helper.mapOfDateAndDataOfDailyEntries.clear();
            Helper.mapOfDisplayDateAndFileTitleDate.clear();
            Helper.mapOfDisplayDateAndDayOfWeek.clear();
            if (clearAdMap) Helper.mapOfSavesVsAdShown.clear();
            LOGGER.warn("All maps cleared!");
        } catch (Exception e) {
            if (BuildConfig.DEBUG)
                LOGGER.error("Failed to complete the internal map clearing op! Error : ", e);
        }
    }

    public void connectorToFullHeavyImport() {
        internalMapClearer(false);

        if (BuildConfig.DEBUG) LOGGER.info("source file path : {}", innerDirectoryPath);
        entireBilling = new HeavyImporter(context, innerDirectoryPath, mapOfIndexAndDate, reverseMapOfIndexAndDate, localSavedPrefs)
                .proceedWithHeavyImport();

        if (entireBilling != null) { //ideal case
            updateTotalListViewAndData(entireBilling);
        } else { //should not be happening
            if (!isAppLaunchedForFirstTime()) {
                LOGGER.warn("WARNING! Either no entries yet or something is wrong!!");
                longToaster(context, "WARNING! Either no entries yet OR something is wrong!! Try Emergency restoring if backed up earlier");
            }
        }
    }

    public void updateTotalListViewAndData(EntireBilling entireBilling) {
        TotalBillingListAdapter adapter = new TotalBillingListAdapter(this, extractEOADInOrder(mapOfIndexAndDate));
        billingListView.setAdapter(adapter);
        billingListView.setOnItemClickListener(this);

        int colorToBeUsed = RED;
        double grandTotal = entireBilling.getTotalExpense();
        String grandTotalSign = ENTRY_TYPE_NEGATIVE;

        if (grandTotal > 0) {
            grandTotalSign = ENTRY_TYPE_POSITIVE;
            colorToBeUsed = GREEN_DARK;
        } else if (grandTotal == 0) {
            colorToBeUsed = GRAY;
        }
        grandTotal = Math.abs(grandTotal);
        displayGrandTotalSign.setText(grandTotalSign);
        displayGrandTotalCurrency.setText(CurrencyUtil.currencySignToDisplay);
        displayGrandTotal.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, grandTotal));

        displayGrandTotalSign.setTextColor(colorToBeUsed);
        displayGrandTotalCurrency.setTextColor(colorToBeUsed);
        displayGrandTotal.setTextColor(colorToBeUsed);
    }

    private void getMonthlyExpense(boolean withUi) {
        //Data part
        monthlyExpenseList = new MonthlyExpenseCalculator(entireBilling.getMapOfIndexAndDate()).getMonthlyExpense();
        if (monthlyExpenseList.length > 0)
            updateCurrentMonthExpense(monthlyExpenseList[0]); //else defaulting to 0.00

        if (withUi) {
            //UI part
            final AbstractDialog dialog = new MonthlyExpenseDialog(context, getString(R.string.headingForMonthlyExpense), R.layout.monthly_expense_layout);
            dialog.fillUpDialog(monthlyExpenseList);
            dialog.fireUpDialog();
        }
    }

    private void centralProcessorForDeletion() {
        if (mapOfIndexAndDate.isEmpty()) {
            LOGGER.info("Nothing to delete..");
            shortToaster(context, "Nothing to delete");
            return;
        }

        //generating data to be fed
        ModelForDeletion[] modelItems = new ModelForDeletion[mapOfIndexAndDate.size()];
        for (int i = 0; i < mapOfIndexAndDate.size(); i++)
            modelItems[i] = new ModelForDeletion(getEntryOfADayFromPosition(i).toString());

        grandDateRecordsToBePurged.clear();
        final AbstractDialog dialog = new DeletionDialog(context, this, "Day Entries present:--", R.layout.deletion_prompt);
        dialog.fillUpDialog(modelItems);
        dialog.fireUpDialog();
    }

    private boolean purgeDayEntry(String date) {
        String fileToBeDeleted = String.format(FORMAT_INTERNAL_FILE, FILE_HEADER_INTERNAL_FILE, Helper.mapOfDisplayDateAndFileTitleDate.get(date));
        if (BuildConfig.DEBUG) LOGGER.info("Attempting to delete '{}'", fileToBeDeleted);
        File file = new File(fileToBeDeleted);
        return file.delete();
    }

    private void checkInternetAndConnectToUpdateCheckerAndSettingsImporter() {
        if (BuildConfig.DEBUG) LOGGER.info("Querying version number for app from central repo!");
        TestConnectivity testConnectivity = new TestConnectivity(this, DEFAULT_HOST_FOR_PINGING);
        testConnectivity.execute();
    }

    @Override
    public void isConnectedToInternet(boolean internetConnectivity) {
        if (internetConnectivity) {
            //going for second hit for actual version checking from here
            if (BuildConfig.DEBUG)
                LOGGER.warn("Connected to internet! Proceeding for update checkin!");
            VersionCheckPinger pingerService = new VersionCheckPinger(this, ID_SITE_VERSION_CHECK);
            pingerService.execute();
            SettingsPinger settingsPinger = new SettingsPinger(this, ID_SITE_REMOTE_SETTINGS);
            settingsPinger.execute();
        }
    }

    @Override
    public void readRemoteGDriveData(String remoteData, GDriveLocators gDriveLocators) {
        switch (gDriveLocators) {
            case VERSION:
                executeVersionCheckRemoteDataAcquisition(remoteData);
                break;
            case SETTINGS:
                executeSettingsRemoteDataAcquisition(remoteData);
                break;
        }
    }

    private void executeVersionCheckRemoteDataAcquisition(String versionHitText) {
        if (BuildConfig.DEBUG)
            LOGGER.info("Rx version hit text from main act : {}", versionHitText);
        if (String.format(ERR_WHILE_GDRIVE_CHECK, GDriveLocators.VERSION).equals(versionHitText)) {
            if (BuildConfig.DEBUG) LOGGER.info("Couldn't retrieve version check file!");
        } else {
            VersionCheck versionCheck = VersionCheck.generateVersionCheckFromVersionHitText(versionHitText);
            handleChangeLogUpdate(versionCheck.getChangeLog()); //to be performed only once to avoid conc issues on the list
            if (versionCheck.isNewVersionAvailable(BuildConfig.VERSION_NAME)) {
                //Helper.shortToaster(context, "Update available!"); // needs to go up on the notification panel
                if (BuildConfig.DEBUG) LOGGER.info("Discovered new version!");
                notifier.fireNotification();
            } else {
                if (versionCheck.getUpdate().isEmpty()) {
                    if (BuildConfig.DEBUG) LOGGER.warn("Couldn't query the version!");
                } else {
                    //Helper.shortToaster(context, "No Update available!"); //TODO -- to be removed before final push
                    if (BuildConfig.DEBUG) LOGGER.info("No Update available!");
                }
            }
        }
    }

    private void executeSettingsRemoteDataAcquisition(String remoteSettings) {
        if (BuildConfig.DEBUG)
            LOGGER.info("Rx remote settings hit text from main act : {}", remoteSettings);
        if (String.format(ERR_WHILE_GDRIVE_CHECK, GDriveLocators.SETTINGS).equals(remoteSettings)) {
            if (BuildConfig.DEBUG) LOGGER.info("Couldn't retrieve remote settings file!");
        } else {
            SettingsFromGh settingsFromGh = SettingsFromGh.populatePaymentDetails(remoteSettings);
            handleRemoteSettingsUpdate(settingsFromGh.getPayment());
        }
    }

    @Override
    public void handleDialogOk() {
        if (!grandDateRecordsToBePurged.isEmpty()) {
            AtomicInteger deletedRecordsCount = new AtomicInteger(0);
            for (Integer index : grandDateRecordsToBePurged) {
                if (BuildConfig.DEBUG)
                    LOGGER.info("Removing entry of '{}' at index {}", mapOfIndexAndDate.get(index), index);
                boolean wasPurgeSuccessful = purgeDayEntry(mapOfIndexAndDate.get(index));
                if (wasPurgeSuccessful) {
                    LOGGER.info("Entries for date '{}' deleted successfully!", mapOfIndexAndDate.get(index));
                    deletedRecordsCount.incrementAndGet();
                } else {
                    LOGGER.error("Couldn't delete entries for date '{}'", mapOfIndexAndDate.get(index));
                }
            }
            shortToaster(context, deletedRecordsCount.get() + " record(s) deleted!");
            //refresh the maps here
            connectorToFullHeavyImport();
            isSaveDoneAtleastOnce = true;
            if (BuildConfig.DEBUG)
                LOGGER.info("Grand Billing list size now : {}", mapOfIndexAndDate.size());
        } else {
            LOGGER.info("Nothing selected to delete from the day record...");
        }
    }

    @Override
    public void handleDialogCancel() {

    }
}
