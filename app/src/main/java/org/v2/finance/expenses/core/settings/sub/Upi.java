package org.v2.finance.expenses.core.settings.sub;

import java.util.List;

public class Upi {
    private String id;
    private Double amount;
    private String baseCurrency;
    private List<FxRate> fxRates;

    public Upi() {
    }

    public Upi(String id, Double amount, String baseCurrency, List<FxRate> fxRates) {
        this.id = id;
        this.amount = amount;
        this.baseCurrency = baseCurrency;
        this.fxRates = fxRates;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Upi{");
        sb.append("id='").append(id).append('\'');
        sb.append(", amount=").append(amount);
        sb.append(", baseCurrency='").append(baseCurrency).append('\'');
        sb.append(", fxRates=").append(fxRates);
        sb.append('}');
        return sb.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public List<FxRate> getFxRates() {
        return fxRates;
    }

    public void setFxRates(List<FxRate> fxRates) {
        this.fxRates = fxRates;
    }
}
