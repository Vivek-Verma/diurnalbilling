package org.v2.finance.expenses.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.InitLoader;
import org.v2.finance.expenses.MainActivity;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.ui.About;
import org.v2.finance.expenses.ui.ChangeLog;
import org.v2.finance.expenses.ui.Discoverer;
import org.v2.finance.expenses.ui.GraphDisplayer;
import org.v2.finance.expenses.ui.HelpCentral;
import org.v2.finance.expenses.ui.Updater;
import org.v2.finance.expenses.ui.ViewDayRecord;
import org.v2.finance.expenses.ui.ViewEntryRecord;
import org.v2.finance.expenses.ui.signinup.SignUp;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.Serializable;

import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.KEY_TO_DAILY_ENTRY_DATE_FROM_UP;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_NEW_DAY_RECORD;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_NEW_ENTRY;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_VIEW_DAY_RECORD;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_VIEW_ENTRY;

/**
 * Internal class for launching various activities... Kind of a Control Centre
 *
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 29-01-2019
 * @since 29-01-2019
 */
public class ActivityStarter {

    private final Logger logger = LoggerFactory.getLogger(ActivityStarter.class);

    private final Context context;

    public ActivityStarter(Context context) {
        this.context = context;
    }

    private void launchActivity(Class classToBeLaunched) {
        if (BuildConfig.DEBUG) logger.info("Going to launch the class {}", classToBeLaunched.getSimpleName());
        launchActivity(generateIntent(classToBeLaunched));
    }

    private void launchActivity(Intent intent) {
        context.startActivity(intent);
    }

    private void fireUpMainActivity(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (BuildConfig.DEBUG) logger.info("Restarting the app!!");
        launchActivity(intent);

        logger.warn("Trickled through after invocation of main activity start... will get killed now!");
        if (context instanceof Activity) ((Activity) context).finish();
        Runtime.getRuntime().exit(0);
    }

    public void fireUpMainActivity() {
        fireUpMainActivity(generateMainActivityIntent());
    }

    public void fireUpMainActivity(LocalSavedPrefs localSavedPrefs, PropertyReader propertyReader) {
        final Intent intent = generateMainActivityIntent();
        if (localSavedPrefs != null)
            intent.putExtra(KEY_EXTRAS_INTENT_TX_DATA, localSavedPrefs);
        if (propertyReader != null)
            intent.putExtra(KEY_EXTRAS_INTENT_PROPERTY_READER, propertyReader);
        fireUpMainActivity(intent);
    }

    public void createNewDayRecord(String requiredDate, LocalSavedPrefs localSavedPrefs) {
        Helper.ROUTING_PATH = ROUTING_TO_NEW_DAY_RECORD;

        EntryOfADay newEntryOfADay = new EntryOfADay();
        newEntryOfADay.setDisplayDate(requiredDate);
        Helper.layOverEntryOfADay = newEntryOfADay;
        //entireBillingList.add(newEntryOfADay); //this might cause trouble -- not sure about it though - 25-09-2018
        Helper.mapOfDateAndDailyEntries.put(requiredDate, newEntryOfADay); //this might cause trouble -- not sure about it though - 25-09-2018
        launchGenericScreenWithNoPropsIntactStack(localSavedPrefs, ViewDayRecord.class);
    }

    public void viewExistingDayRecord(int position, EntryOfADay entryOfADay, Boolean isDayViewEditable, LocalSavedPrefs localSavedPrefs) {
        Helper.ROUTING_PATH = ROUTING_TO_VIEW_DAY_RECORD;
        Helper.layOverEntryOfADay = new EntryOfADay(entryOfADay); //cannot pass the golden src of day record, as this caused the bug DIB-57
        Helper.isDayRecordEditable = isDayViewEditable;
        Helper.layOverIndexFromMain = position;
        if (BuildConfig.DEBUG) logger.info("trying to invoke the entry of date : {}", Helper.layOverEntryOfADay.getDisplayDate());
        launchGenericScreenWithNoPropsIntactStack(localSavedPrefs, ViewDayRecord.class);
    }

    public void fireSearchQuery(LocalSavedPrefs localSavedPrefs, PropertyReader properties) {
        launchGenericScreenWithIntactStack(localSavedPrefs, properties, Discoverer.class);
    }

    public void createNewEntryToDay(String displayDate, EntryOfADay entryOfADay) {
        Helper.ROUTING_PATH = ROUTING_TO_NEW_ENTRY;
        Helper.layOverEntry = null;
        Helper.layOverEntryOfADay = entryOfADay;

        Intent intent = new Intent(context, ViewEntryRecord.class);
        Bundle bundle = new Bundle();
        //bundle.putSerializable(KEY_TO_DAILY_ENTRY, newEntry);
        bundle.putString(KEY_TO_DAILY_ENTRY_DATE_FROM_UP, displayDate);
        intent.putExtras(bundle);
        //entriesList.add(newEntry); // for later updation autowiring
        context.startActivity(intent);
    }

    public void viewEntryOfADay(String displayDate, Entry entry, EntryOfADay entryOfADay) {
        Helper.ROUTING_PATH = ROUTING_TO_VIEW_ENTRY;

        Intent intent = new Intent(context, ViewEntryRecord.class);
        Bundle bundle = new Bundle();
        //bundle.putSerializable(KEY_TO_DAILY_ENTRY, entriesList.get(position)); //don't uncomment
        bundle.putString(KEY_TO_DAILY_ENTRY_DATE_FROM_UP, displayDate);
        intent.putExtras(bundle);
        Helper.layOverEntry = entry;
        Helper.layOverEntryOfADay = entryOfADay;
        context.startActivity(intent);
    }

    public void fireExpenseGraph() {
        if (Helper.monthlyExpenseList.length > 1)
            launchActivity(GraphDisplayer.class);
        else {
            ToasterUtil.shortToaster(context, "Cannot display graph for less than 1 month!");
            if (BuildConfig.DEBUG) logger.warn("Cannot display graph for less than 1 month!");
        }
    }

    public void launchGenericScreen(Intent intent, boolean cleanStackAndLaunch) {
        if (cleanStackAndLaunch) intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchActivity(intent);
    }

    public void launchGenericScreen(Intent intent) {
        launchGenericScreen(intent, true);
    }

    public <T extends Serializable> void launchGenericScreenWithNoPropsIntactStack(T txData, Class classz) {
        launchGenericScreen(txData, null, classz, false);
    }

    public <T extends Serializable> void launchGenericScreenWithIntactStack(T txData, PropertyReader propertyReader, Class classz) {
        launchGenericScreen(txData, propertyReader, classz, false);
    }

    public <T extends Serializable> void launchGenericScreen(T txData, PropertyReader propertyReader, Class classz, boolean cleanStackAndLaunch) {
        final Intent intent = generateIntent(classz);
        if (txData != null)
            intent.putExtra(KEY_EXTRAS_INTENT_TX_DATA, txData);
        if (propertyReader != null)
            intent.putExtra(KEY_EXTRAS_INTENT_PROPERTY_READER, propertyReader);
        launchGenericScreen(intent, cleanStackAndLaunch);
    }

    public <T extends Serializable> void launchGenericScreen(T txData, PropertyReader propertyReader, Class classz) {
        launchGenericScreen(txData, propertyReader, classz, true);
    }

    public void restartApp() {
        launchGenericScreen(null, null, InitLoader.class);
    }

    public void launchSignUpScreen() {
        launchGenericScreen(generateSignUpIntent());
    }

    public void fireAbout(PropertyReader propertyReader) {
        final Intent intent = generateIntent(About.class);
        if (propertyReader != null)
            intent.putExtra(KEY_EXTRAS_INTENT_PROPERTY_READER, propertyReader);
        launchActivity(intent);
    }

    public void fireUpdater() {
        launchActivity(Updater.class);
    }

    public void fireHelp() {
        launchActivity(HelpCentral.class);
    }

    public void fireChangeLog() {
        launchActivity(ChangeLog.class);
    }

    private Intent generateMainActivityIntent() {
        return generateIntent(MainActivity.class);
    }

    private Intent generateSignUpIntent() {
        return generateIntent(SignUp.class);
    }

    private Intent generateIntent(Class classz) {
        return new Intent(context, classz);
    }

    public static void shutDownApp(Activity activity) {
        activity.finishAffinity();
    }
}
