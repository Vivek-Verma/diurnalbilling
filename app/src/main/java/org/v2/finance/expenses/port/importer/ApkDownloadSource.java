package org.v2.finance.expenses.port.importer;

/**
 * @author Vivek Verma
 * @since 1/6/21
 */
public enum ApkDownloadSource {
    GDRIVE, GITHUB
}
