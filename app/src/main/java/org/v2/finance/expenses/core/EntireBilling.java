package org.v2.finance.expenses.core;

import java.util.Map;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 23-12-2018
 * @since 20-09-2018
 *
 * Has the entire collection of EntryOfADay, and the grand processing
 */
public class EntireBilling {

    private final double totalExpense;

    private final Map<Integer, String> mapOfIndexAndDate; //this mapping is between the index position and the date only. The read data is in the Helper's mapOfDateAndDailyEntries.

    public EntireBilling(double totalExpense, Map<Integer, String> mapOfIndexAndDate) {
        this.totalExpense = totalExpense;
        this.mapOfIndexAndDate = mapOfIndexAndDate;
    }

    public double getTotalExpense() {
        return totalExpense;
    }

    public Map<Integer, String> getMapOfIndexAndDate() {
        return mapOfIndexAndDate;
    }

    /*
    //this will be a linked list, and thus insertions into this should be done in reverse manner
    //private LinkedList<EntryOfADay> entireBillingList;

    public LinkedList<EntryOfADay> getEntireBillingList() {
        return entireBillingList;
    }
    */
}
