package org.v2.finance.expenses.contracts.diurnal;

import android.content.Context;
import android.os.AsyncTask;

import org.v2.finance.expenses.util.ToasterUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Vivek Verma
 * @since 21/3/21
 */
public abstract class AsyncSignInUpModule extends AsyncTask<Void, String, Boolean> {
    protected final Context context;
    protected final String serverHost;
    protected final String serverPingEndpoint;
    protected final long serverRequestTimeout;
    protected final String serverUser;
    protected final String serverCred;
    protected ScheduledExecutorService scheduledExecutorService;
    protected ExecutorService executorService;
    protected int delayInProgressToast;
    protected String displayProgressMessage = "Waiting for server to come online!";

    public AsyncSignInUpModule(Context context, String serverHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred) {
        this(context, serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, 8);
    }

    public AsyncSignInUpModule(Context context, String serverHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred,
                               int delayInProgressToast) {
        this.context = context;
        this.serverHost = serverHost;
        this.serverPingEndpoint = serverPingEndpoint;
        this.serverRequestTimeout = serverRequestTimeout;
        this.serverUser = serverUser;
        this.serverCred = serverCred;
        this.delayInProgressToast = delayInProgressToast;
    }

    protected void jumpStartExecutors() {
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleWithFixedDelay(this::runWaitOnServer, 1, delayInProgressToast, TimeUnit.SECONDS);
        executorService = Executors.newSingleThreadExecutor();
    }

    protected boolean areExecutorsDead() {
        return (scheduledExecutorService == null || scheduledExecutorService.isShutdown()) && (executorService == null || executorService.isShutdown());
    }

    protected void killAllExecutors() {
        if (scheduledExecutorService != null && !scheduledExecutorService.isShutdown()) scheduledExecutorService.shutdownNow();
        if (executorService != null && !executorService.isShutdown()) executorService.shutdownNow();
    }

    protected void runWaitOnServer() {
        publishProgress(displayProgressMessage);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        if (!values[0].isEmpty()) ToasterUtil.shortToaster(context, values[0]);
    }

    public void setDisplayProgressMessage(String displayProgressMessage) {
        this.displayProgressMessage = displayProgressMessage;
    }

    public void setDelayInProgressToast(int delayInProgressToast) {
        this.delayInProgressToast = delayInProgressToast;
    }
}
