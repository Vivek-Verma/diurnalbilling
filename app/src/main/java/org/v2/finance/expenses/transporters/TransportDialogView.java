package org.v2.finance.expenses.transporters;

import android.app.Dialog;
import android.view.View;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 12-06-2019
 * @since 12-06-2019
 */
@Deprecated
public class TransportDialogView {

    private final Dialog dialog;
    private final View view;

    public TransportDialogView(Dialog dialog, View view) {
        this.dialog = dialog;
        this.view = view;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public View getView() {
        return view;
    }
}
