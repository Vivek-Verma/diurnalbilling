package org.v2.finance.expenses.ui.alerter;

import android.content.Context;

/**
 * @author Vivek Verma
 * @since 25/12/19
 */
abstract class AbstractAlerterOnlyOk extends AbstractAlerter {
    AbstractAlerterOnlyOk(Context context, String title, String messageForDisplay) {
        super(context, title, messageForDisplay);

        showAlert("OK");
    }

    @Override
    void initOkButton(String textForButton, String displayPositive) {
        alert.setPositiveButton(textForButton, (dialog, which) -> {}); //needs to be called on main UI thread only /-
    }
}
