package org.v2.finance.expenses.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.connectivity.TestConnectivity;
import org.v2.finance.expenses.connectivity.VersionCheckPinger;
import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;
import org.v2.finance.expenses.contracts.TestConnectivityConnector;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.core.VersionCheck;
import org.v2.finance.expenses.port.export.ExportAll;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_HOST_FOR_PINGING;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_LONG_VALUE;
import static org.v2.finance.expenses.constants.Constants.DIVIDER_FOR_MB;
import static org.v2.finance.expenses.constants.Constants.DOWNLOADED_APK_NAME;
import static org.v2.finance.expenses.constants.Constants.DOWNLOAD_LOCATION_EXTERNAL;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.ERR_WHILE_VERSION_CHECK;
import static org.v2.finance.expenses.constants.Constants.ESTIMATE_DOWNLOAD_SIZE;
import static org.v2.finance.expenses.constants.Constants.ID_SITE_VERSION_CHECK;
import static org.v2.finance.expenses.permissions.PermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE;
import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;
import static org.v2.finance.expenses.util.Helper.extractValuesFromMapInOrder;

public class Updater extends AppCompatActivity implements View.OnClickListener, TestConnectivityConnector, GDriveConnector {

    private final Logger LOGGER = LoggerFactory.getLogger(Updater.class);

    private final Context context = this;
    private Button checkNetConn;
    private Button verifyVersionDiff;
    private TextView noUpdateReq;
    private LinearLayout layoutIfDownloadRequired;
    private TextView downloadSize;
    private Button downloadYes;
    private Button downloadNo;
    private TextView downloadProgress;
    private ProgressBar progressDownload;
    private Button installYes;
    private Button installNo;
    private Button ok, cancel;

    private TestConnectivity testConnectivity = null;
    private VersionCheckPinger versionCheckPinger = null;
    private ApkDownloader apkDownloader = null;
    private File tmpApkLocation = null;

    private LocalSavedPrefs localSavedPrefs;
    private PropertyReader properties;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updater);
        deactivateHeavyImportingFlag();

        Toolbar toolbar = findViewById(R.id.toolbar_updater);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }

        checkNetConn = findViewById(R.id.btnCheckNetConn);
        verifyVersionDiff = findViewById(R.id.btnVerifyVersionDiff);

        noUpdateReq = findViewById(R.id.txtNoUpdateReq);
        layoutIfDownloadRequired = findViewById(R.id.layoutIfDownloadRequired);

        //downloadYes = findViewById(R.id.btnDownloadYes);
        //downloadNo = findViewById(R.id.btnDownloadNo);
        downloadProgress = findViewById(R.id.txtDownloadProgress);
        downloadSize = findViewById(R.id.txtDownloadSize);
        progressDownload = findViewById(R.id.progressDownload);


        installYes = findViewById(R.id.btnInstallYes);
        installNo = findViewById(R.id.btnInstallNo);
        installYes.setOnClickListener(this);
        installNo.setOnClickListener(this);

        ok = findViewById(R.id.btnUpdaterOk);
        cancel = findViewById(R.id.btnUpdaterCancel);
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);

        testConnectivity = new TestConnectivity(this, DEFAULT_HOST_FOR_PINGING);
        testConnectivity.execute();

        localSavedPrefs = new LocalSavedPrefs();
        ImportToLocalPrefs importToLocalPrefs = new ImportToLocalPrefs(context, localSavedPrefs);
        importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();

        properties = new PropertyReader(context, getString(R.string.diurnal_properties));
    }

    private void orchestratorFirstStaging(boolean isConnectedToInternet) {
        if (isConnectedToInternet) {
            checkNetConn.setBackgroundColor(Color.GREEN);
            ToasterUtil.shortToaster(context, "Internet connection is a go!");

            versionCheckPinger = new VersionCheckPinger(this, ID_SITE_VERSION_CHECK);
            versionCheckPinger.execute();
        } else {
            checkNetConn.setBackgroundColor(Color.RED);
            verifyVersionDiff.setBackgroundColor(Color.GRAY);
            //downloadYes.setBackgroundColor(Color.GRAY);
            //downloadNo.setBackgroundColor(Color.GRAY);
            installYes.setBackgroundColor(Color.GRAY);
            installNo.setBackgroundColor(Color.GRAY);

            ok.setEnabled(true);
            ToasterUtil.shortToaster(context, "No internet connection!");
        }
    }

    private void orchestratorSecondStaging(boolean isUpdateAvailable, String updateId) {
        if (BuildConfig.DEBUG) LOGGER.info("Update id  {} ", updateId);
        if (isUpdateAvailable) {
            verifyVersionDiff.setBackgroundColor(Color.GREEN);
            ToasterUtil.shortToaster(context, "Update available!");

            boolean isExternalDownloadAccessible = isExternalDownloadAccessible();
            if (isExternalDownloadAccessible) {
                boolean externalFolderExists = createExternalIfNotExists();
                if (!externalFolderExists) {
                    ToasterUtil.shortToaster(context, "Cannot find/create Download folder for apk. Grave error, update not possible!");
                    finish();
                }
                File tmpApk = new File(DOWNLOAD_LOCATION_EXTERNAL + DOWNLOADED_APK_NAME);
                apkDownloader = new ApkDownloader(updateId, tmpApk);
                apkDownloader.execute();
            } else {
                LOGGER.error("Cannot access external storage storage, Update cannot proceed!");
                ToasterUtil.shortToaster(context, "Kindly provide access to storage to download the update & then try again!");
                finish(); //EXIT POINT
            }
        } else {
            if (updateId.isEmpty()) {
                verifyVersionDiff.setBackgroundColor(Color.RED);
                ToasterUtil.shortToaster(context, "Couldn't query the version!");
            } else {
                verifyVersionDiff.setBackgroundColor(Color.GREEN);
                noUpdateReq.setVisibility(View.VISIBLE);
                //layoutIfDownloadRequired.setVisibility(View.GONE);
                layoutIfDownloadRequired.setEnabled(false);
                ok.setEnabled(true);
                ToasterUtil.shortToaster(context, "No update available!");
            }
        }
    }

    private boolean createExternalIfNotExists() {
        File file = new File(DOWNLOAD_LOCATION_EXTERNAL);
        if (!file.exists()) {
            if (file.mkdirs()) {
                if (BuildConfig.DEBUG) LOGGER.info("Created external downloads folder!");
            } else {
                if (BuildConfig.DEBUG) LOGGER.error("Unable to create the external downloads folder!");
                return false;
            }
        }
        return true;
    }

    private boolean isExternalDownloadAccessible() {
        final int permissionWriteExternalStorage = ContextCompat.checkSelfPermission(context, PERMISSION_WRITE_EXTERNAL_STORAGE);
        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
            if (BuildConfig.DEBUG) LOGGER.error("Not able to access the external downloads location '{}'. No permission!", DOWNLOAD_LOCATION_EXTERNAL);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (testConnectivity != null && !testConnectivity.isCancelled()) {
            testConnectivity.cancel(true);
        }
        if (versionCheckPinger != null && !versionCheckPinger.isCancelled()) {
            versionCheckPinger.cancel(true);
        }
        if (apkDownloader != null && !apkDownloader.isCancelled()) {
            apkDownloader.cancel(true);
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        if (view == ok || view == cancel) {
            onBackPressed();
        } else if (view == installYes) {
            if (tmpApkLocation != null) {
                try {
                    /*
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(FileProvider.getUriForFile(context, getApplicationContext().getPackageName()+".fileprovider",tmpApkLocation), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
                    context.startActivity(intent);
                     */
                    Intent intent;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Uri apkUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", tmpApkLocation);
                        intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                        intent.setData(apkUri);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } else {
                        Uri apkUri = Uri.fromFile(tmpApkLocation);
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    //taking backup now!!
                    try {
                        //will work only if called via the app's Update central and not the notification one
                        new ExportAll(context, extractValuesFromMapInOrder(Helper.tempMapOfIndexAndDate), localSavedPrefs, new ExportToInternalPrefs(context, localSavedPrefs), properties)
                                .fireHeavyExport();
                        Helper.tempMapOfIndexAndDate = null;
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) LOGGER.error("Failed to take backup. Will be proceeding to update w/o backin up. Error:", e);
                        ToasterUtil.longToaster(context, "Will be updating without backing up the data.. Take backup manually if you want");
                    }
                    try {
                        //required to make the first time screen come up post update
                        LocalSavedPrefs tempLocalSavedPrefs = new LocalSavedPrefs();
                        ExportToInternalPrefs tempExportToInternalPrefs = new ExportToInternalPrefs(context, tempLocalSavedPrefs);
                        tempLocalSavedPrefs.setLastUsedTimeStamp(DEFAULT_LONG_VALUE);
                        tempExportToInternalPrefs.writeLastUsedTime();
                        tempExportToInternalPrefs = null;
                        tempLocalSavedPrefs = null;
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) LOGGER.warn("Failed to default internal timestamp for new app update! err : ", e);
                    }
                    Helper.searchAndKillApkCheckAlarm(context);
                    context.startActivity(intent);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) LOGGER.error("Couldn't start apk installation! Error : ", e);
                    ToasterUtil.shortToaster(context, "Installation failed!");
                }
            } else {
                LOGGER.error("Not received tmp apk location, cannot install!");
                ToasterUtil.shortToaster(context, "Installation failed!");
            }
        } else if (view == installNo) {
            ToasterUtil.shortToaster(context, "Installation will not be performed, as denied by user!");
            finish();
        }
    }

    @Override
    public void isConnectedToInternet(boolean internetConnectivity) { //for the TestConnectivity
        orchestratorFirstStaging(internetConnectivity); //propagating the flow from here
    }

    @Override
    public void readRemoteGDriveData(String versionHitText, GDriveLocators gDriveLocators) {
        if (gDriveLocators == GDriveLocators.VERSION) {
            if (ERR_WHILE_VERSION_CHECK.equals(versionHitText)) {
                orchestratorSecondStaging(false, EMPTY_STR);
            } else {
                VersionCheck versionCheck = VersionCheck.generateVersionCheckFromVersionHitText(versionHitText);
                orchestratorSecondStaging(versionCheck.isNewVersionAvailable(BuildConfig.VERSION_NAME), versionCheck.getUpdate());
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ApkDownloader extends AsyncTask<Void, Long, Boolean> {
        private final String url;
        private final File tmpApk;

        ApkDownloader(String id, File tmpApk) {
            this.url = Helper.gitHubLinkGenerator(id); //this will lead to breaking of update logic in older versions attempting GH page
            this.tmpApk = tmpApk;
        }

        private Boolean downloadTheNewApk(String urlToHit) {
            final URL url;
            try {
                url = new URL(urlToHit);
            } catch (MalformedURLException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Malformed URL : {}, Error : ", urlToHit, e);
                return false;
            }

            final HttpsURLConnection httpsURLConnection;
            try {
                httpsURLConnection = (HttpsURLConnection) url.openConnection();
                if (BuildConfig.DEBUG) LOGGER.info("Downloading from: {}", url);
                httpsURLConnection.setReadTimeout(30000);
                httpsURLConnection.setConnectTimeout(30000);
                httpsURLConnection.connect();

                if (httpsURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try {
                        InputStream inputStream = httpsURLConnection.getInputStream();
                        OutputStream outputStream = new FileOutputStream(tmpApk);

                        byte[] buffer = new byte[8 * 1024];
                        int bytesRead;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                            publishProgress(tmpApk.length());
                            if (BuildConfig.DEBUG) LOGGER.info("Tmp apk size : {}", tmpApk.length());
                        }
                        outputStream.flush();
                        inputStream.close();
                        outputStream.close();
                        httpsURLConnection.disconnect();
                        if (tmpApk.length() < ESTIMATE_DOWNLOAD_SIZE * .80) return false;
                        publishProgress(ESTIMATE_DOWNLOAD_SIZE);
                        return true;
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) LOGGER.error("Error occurred while downloading the new apk : ", e);
                    }
                }
            } catch (IOException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Unable to establish internet connectivity. Error : ", e);
            }
            return false;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return downloadTheNewApk(url);
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);
            double progress = (values[0] / (ESTIMATE_DOWNLOAD_SIZE * 1.0)) * 100;
            if (progress > 100) progress = 100;
            downloadProgress.setText(Helper.decimalFormat.format(progress));
            downloadSize.setText(Helper.decimalFormat.format(values[0] / (DIVIDER_FOR_MB * 1.0)));
            progressDownload.setProgress((int) progress);
        }

        @Override
        protected void onPostExecute(Boolean downloadSuccessful) {
            super.onPostExecute(downloadSuccessful);
            if (downloadSuccessful) {
                ToasterUtil.shortToaster(context, "Download completed! Install now?");
                tmpApkLocation = tmpApk;
                installYes.setEnabled(true);
                installYes.setBackgroundColor(Color.parseColor("#679267")); //GREEN from colors.xml
                installNo.setEnabled(true);
                installNo.setBackgroundColor(Color.parseColor("#FF4081")); //ACCENT from colors.xml
            } else {
                ToasterUtil.shortToaster(context, "Download incomplete! CANNOT install!");
            }
        }
    }
}
