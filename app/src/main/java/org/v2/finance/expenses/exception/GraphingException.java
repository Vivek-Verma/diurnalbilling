package org.v2.finance.expenses.exception;

import org.v2.finance.expenses.constants.Constants;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 14-07-2019
 * @since 14-07-2019
 */
public class GraphingException extends Exception {

    private final String exception;
    private final Constants.GraphingExceptionCases exceptionCase;

    public GraphingException(String exception, Constants.GraphingExceptionCases exceptionCase) {
        this.exception = exception;
        this.exceptionCase = exceptionCase;
    }

    @Override
    public String toString() {
        return "GraphinException => " + exception + " :: " + exceptionCase;
    }

}
