package org.v2.finance.expenses.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Vivek Verma
 * @since 8/4/21
 */
public class TimingUtil {
    private static final SimpleDateFormat SDF_DISPLAY_TIMESTAMP_SETTINGS = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

    public static long extractCurrentUtcTimestamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return calendar.getTimeInMillis();
    }

    public static String parseTimestampForSettingsDisplay(long timestamp) {
        Date date = new Date(timestamp);
        return SDF_DISPLAY_TIMESTAMP_SETTINGS.format(date);
    }

    public static boolean hasTimestampExpired(Long inputTimestamp) {
        return System.currentTimeMillis() > inputTimestamp;
    }

    public static long computeExpiryTimestamp(int daysBeforeExpiry) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, daysBeforeExpiry * 1440);
        return calendar.getTimeInMillis();
    }

}
