package org.v2.finance.expenses.ui.signinup.otp;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vv.personal.diurnal.artifactory.generated.OtpMailProto;
import com.vv.personal.diurnal.artifactory.generated.ResponsePrimitiveProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.util.SignInUpUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_INTERACTION_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_INTERACTION_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_INTERACTION;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_INTERACTION;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;

/**
 * @author Vivek Verma
 * @since 21/3/21
 */
public class SignUpOtp extends AppCompatActivity implements View.OnClickListener {
    private final Logger LOGGER = LoggerFactory.getLogger(SignUpOtp.class);

    private final Context context = this;

    private EditText inputSignUpOtp;
    private TextView signupOtpIntermedText;
    private Button buttonSignUpOtpProceed;

    private ActivityStarter activityStarter;
    private PropertyReader properties;
    private String userEmail = EMPTY_STR;
    private UserMappingProto.UserMapping userSignUpDetails;
    private AsyncSignUpOtpGenerator asyncSignUpOtpGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_otp);

        activityStarter = new ActivityStarter(context);
        properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
        if (properties == null) {
            properties = new PropertyReader(context, getString(R.string.diurnal_properties));
        }
        userSignUpDetails = (UserMappingProto.UserMapping) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
        userEmail = userSignUpDetails.getEmail();

        inputSignUpOtp = findViewById(R.id.inputSignUpOtp);
        signupOtpIntermedText = findViewById(R.id.signupOtpIntermedText);
        buttonSignUpOtpProceed = findViewById(R.id.buttonSignUpOtpProceed);

        buttonSignUpOtpProceed.setOnClickListener(this);

        //launch generate otp request here!
        lockAllFields();
        asyncSignUpOtpGenerator = new AsyncSignUpOtpGenerator(context,
                properties.getProperty(DIURNAL_SERVER_HTTP_HOST_INTERACTION),
                properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_INTERACTION),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE),
                properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE_TIMEOUT, 90L),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_USER),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_CRED),
                properties.getBooleanProperty(DIURNAL_SERVER_HOST_INTERACTION_IS_HTTP)
        );
        asyncSignUpOtpGenerator.execute();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSignUpOtpProceed) {
            if (!SignInUpUtil.verifyOtpFromEmail(inputSignUpOtp.getText())) {
                ToasterUtil.longToaster(context, "Invalid OTP!");
                return;
            }

            Integer otp = Integer.parseInt(inputSignUpOtp.getText().toString());
            AsyncSignUpOtpVerifier asyncSignUpOtpVerifier = new AsyncSignUpOtpVerifier(context, otp,
                    properties.getProperty(DIURNAL_SERVER_HTTP_HOST_INTERACTION),
                    properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_INTERACTION),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY),
                    properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY_TIMEOUT, 65L),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_USER),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_INTERACTION_CRED),
                    properties.getBooleanProperty(DIURNAL_SERVER_HOST_INTERACTION_IS_HTTP)
            );
            asyncSignUpOtpVerifier.execute();
        }
    }

    private void opOnFields(boolean targetState) {
        inputSignUpOtp.setEnabled(targetState);
        buttonSignUpOtpProceed.setEnabled(targetState);
        signupOtpIntermedText.setVisibility(!targetState ? View.VISIBLE : View.INVISIBLE);
    }

    void lockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Locking all fields");
        opOnFields(false);
    }

    void unlockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Unlocking all fields");
        opOnFields(true);
    }

    void clearAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Clearing all fields");
        inputSignUpOtp.setText(EMPTY_STR);
    }

    private class AsyncSignUpOtpGenerator extends AsyncSignInUpModule {
        private final PingServerWithData<OtpMailProto.OtpMail> pingServer;
        private boolean otpGenerated = false;

        public AsyncSignUpOtpGenerator(Context context, String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, 11);

            if (BuildConfig.DEBUG) LOGGER.info("SignUpOtpGen isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
            setDisplayProgressMessage("Waiting for OTP to be sent to your email");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            final OtpMailProto.OtpMail otpMail = OtpMailProto.OtpMail.newBuilder().setEmail(userEmail).build();
            timer.stop();
            if (BuildConfig.DEBUG) LOGGER.info("Took {} ms to complete otp-mail obj gen", timer.getTime());

            try {
                timer.reset();
                timer.start();
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, otpMail));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                otpGenerated = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getBoolResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), otpGenerated);
                packageResponseDetail.close();
                return otpGenerated;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact generate-otp module for Interaction. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while contacting otp-gen in Interaction. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean otpGenerated) {
            if (otpGenerated == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
                ActivityStarter.shutDownApp(SignUpOtp.this);
                return;
            } else if (otpGenerated) {
                ToasterUtil.longToaster(context, "OTP generated and sent to registered e-mail");
            } else {
                ToasterUtil.longToaster(context, "Denied OTP generation, could be server error, please reach out to the developer");
            }
            clearAllFields();
            unlockAllFields();
        }
    }

    private class AsyncSignUpOtpVerifier extends AsyncSignInUpModule {
        private final PingServerWithData<OtpMailProto.OtpMail> pingServer;
        private final Integer userEnteredOtp;
        private boolean otpVerified = false;

        public AsyncSignUpOtpVerifier(Context context, Integer userEnteredOtp,
                                      String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
            this.userEnteredOtp = userEnteredOtp;

            if (BuildConfig.DEBUG) LOGGER.info("SignUp isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            final OtpMailProto.OtpMail otpMail = OtpMailProto.OtpMail.newBuilder().setEmail(userEmail).setOtp(userEnteredOtp).build();
            timer.stop();
            if (BuildConfig.DEBUG) LOGGER.info("Took {} ms to complete otp-mail-verif obj gen", timer.getTime());

            try {
                timer.reset();
                timer.start();
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, otpMail));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                otpVerified = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getBoolResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), otpVerified);
                packageResponseDetail.close();
                return otpVerified;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact verify-otp module for Interaction. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while verifying otp-gen in Interaction. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean otpVerified) {
            if (otpVerified == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
                return;
            }
            if (otpVerified) {
                ToasterUtil.longToaster(context, "OTP verified, saving data to server now");

                AsyncSignUp asyncSignUp = new AsyncSignUp(context, userSignUpDetails,
                        properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                        properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                        properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP),
                        properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_TIMEOUT, 90L),
                        properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                        properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                        properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
                );
                asyncSignUp.execute();
            } else {
                ToasterUtil.longToaster(context, "Registration failed!");
                clearAllFields();
                unlockAllFields();
            }
        }
    }

    private class AsyncSignUp extends AsyncSignInUpModule {

        private final UserMappingProto.UserMapping userSignUpDetails;
        private final PingServerWithData<UserMappingProto.UserMapping> pingServer;
        private boolean signUpResponse = false;

        public AsyncSignUp(Context context, UserMappingProto.UserMapping userSignUpDetails,
                           String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
            this.userSignUpDetails = userSignUpDetails;

            if (BuildConfig.DEBUG) LOGGER.info("SignUp isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            try {
                if (areExecutorsDead()) jumpStartExecutors();

                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, userSignUpDetails));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                signUpResponse = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getBoolResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), signUpResponse);
                packageResponseDetail.close();
                return signUpResponse;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact sign-up module for DBI. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while signing up in DBI. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean signUpResponse) {
            if (signUpResponse == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
                return;
            }
            if (signUpResponse) {
                ToasterUtil.longToaster(context, "Registration complete!");
                activityStarter.restartApp();
            } else {
                ToasterUtil.longToaster(context, "Registration failed!");
                clearAllFields();
                unlockAllFields();
            }
        }
    }
}
