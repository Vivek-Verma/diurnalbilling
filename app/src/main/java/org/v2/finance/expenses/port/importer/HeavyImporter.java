package org.v2.finance.expenses.port.importer;

import android.annotation.SuppressLint;
import android.content.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.core.EntireBilling;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.transporters.Transporter;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_LONG_VALUE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.util.Helper.convertDisplayDateToFileTitleDate;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 10-12-2018
 * @since 10-12-2018
 */
public class HeavyImporter {

    private final Logger LOGGER = LoggerFactory.getLogger(HeavyImporter.class);

    private final Context context;
    private final String outerDirectoryPath;
    private final Map<Integer, String> mapOfIndexAndDate;
    private final Map<String, Integer> reverseMapOfIndexAndDate;
    private final LocalSavedPrefs localSavedPrefs;

    public HeavyImporter(Context context, String outerDirectoryPath, Map<Integer, String> mapOfIndexAndDate, Map<String, Integer> reverseMapOfIndexAndDate, LocalSavedPrefs localSavedPrefs) {
        this.context = context;
        this.outerDirectoryPath = outerDirectoryPath;
        this.mapOfIndexAndDate = mapOfIndexAndDate;
        this.reverseMapOfIndexAndDate = reverseMapOfIndexAndDate;
        this.localSavedPrefs = localSavedPrefs;
    }

    @SuppressLint("DefaultLocale")
    public EntireBilling proceedWithHeavyImport() {
        File outerDir = new File(outerDirectoryPath);
        double grandTotal = 0;
        EntireBilling entireBilling = null;

        if (!outerDir.exists()) {
            if (DEFAULT_LONG_VALUE == localSavedPrefs.getLastUsedTimeStamp()) {
                if (BuildConfig.DEBUG) LOGGER.info("Outer filepath [{}] is not present, going on to create it, because of the first time...", outerDirectoryPath);
                if (!outerDir.mkdirs()) {
                    if (BuildConfig.DEBUG) LOGGER.error("For some unknown reason, was not able to create the internal folder... Something went terribly wrong... Halting and returning!");
                    return entireBilling; //null return
                }
            } else {
                if (BuildConfig.DEBUG)
                    LOGGER.error("Outer filepath [{}] is not present, something has gone wrong with the file structure, internal data is lost, hence halting here...", outerDirectoryPath);
                return entireBilling; //null return
            }
        }

        boolean superPass = true;
        File[] files = outerDir.listFiles();
        Arrays.sort(files);

        boolean anyBypassOccurrance = false;
        int total = files.length; //doing this helped in coming over a fault in title which was later handled
        for (int i = 0; i < total; i++) {
            if (BuildConfig.DEBUG) LOGGER.info("i : {}, file : {}", i, files[i].getName());
            try {
                Transporter transporter = UnitImporter.triggerImportFromSavedFile(files[i]);
                if (BuildConfig.DEBUG) LOGGER.info("\tdid data read from saved? : {}", transporter.isImportSuccessful());
                if (transporter.isBypassTransport()) {
                    anyBypassOccurrance = true;
                    continue;
                }
                if (transporter.isImportSuccessful()) {
                    EntryOfADay entryOfADay = processLineTypeEmptyLine(transporter.getDailyAmount(), transporter.getEntryOfADay(), transporter.getDailyEntries());
                    grandTotal += ENTRY_TYPE_NEGATIVE.equals(entryOfADay.getDisplayAmountSign()) ? entryOfADay.getDisplayAmount() * -1 : entryOfADay.getDisplayAmount();
                    ////entireBillingList.addFirst(entryOfADay);

                    String date = entryOfADay.getDisplayDate(); //dd-MM-yyyy format
                    if (BuildConfig.DEBUG) LOGGER.info("\t\tentry of a day date : {}", date);
                    Helper.mapOfDateAndDailyEntries.put(date, entryOfADay);
                    mapOfIndexAndDate.put(total - 1 - i, date); //inverse logic as the most recent entry will come at the beginning, because when the list of files is sorted, the oldest will be encountered first but will be at the end of the list.
                    reverseMapOfIndexAndDate.put(date, total - 1 - i);
                    Helper.mapOfDateAndDataOfDailyEntries.put(date, transporter.getDataOfEntryOfADay());
                    Helper.mapOfDisplayDateAndFileTitleDate.put(date, convertDisplayDateToFileTitleDate(date));
                    Helper.mapOfDisplayDateAndDayOfWeek.put(date, Helper.convertDisplayDateToDayOfWeek(date));
                } else {
                    superPass = false;
                    break;
                }
            } catch (Exception e1) {
                if (BuildConfig.DEBUG) LOGGER.error("Exception occurred while going through the loop of files retrieval : ", e1);
            }
        }

        if (anyBypassOccurrance) {
            if (BuildConfig.DEBUG) LOGGER.warn("Reimporting as found traces of transporter bypass, viz some empty ENTRIES' files were deleted!");
            return proceedWithHeavyImport();
        }

        if (superPass) {
            if (BuildConfig.DEBUG) LOGGER.info("entire map of index and date size : {}, grand-total : {}", mapOfIndexAndDate.size(), grandTotal);
            entireBilling = new EntireBilling(grandTotal, new HashMap<>(mapOfIndexAndDate)); //populated here
        } else {
            LOGGER.error("Data import failed!");
            ToasterUtil.longToaster(context, "Data import failed!"); //will lead to null return
        }
        return entireBilling;
    }

    /**
     * Sets the entryOfADay
     *
     * @param dailyAmount
     * @param entryOfADay
     * @param dailyEntries
     */
    protected EntryOfADay processLineTypeEmptyLine(double dailyAmount, EntryOfADay entryOfADay, LinkedList<Entry> dailyEntries) {
        String dailyAmountSign = ENTRY_TYPE_NEGATIVE;
        if (dailyAmount >= 0) dailyAmountSign = ENTRY_TYPE_POSITIVE;

        ////if (BuildConfig.DEBUG) logger.info("\t\t\t\tamt : {}, local amount sign : {}", dailyAmount, dailyAmountSign);
        entryOfADay.setDisplayAmountSign(dailyAmountSign);
        entryOfADay.setDisplayAmount(Math.abs(dailyAmount));
        entryOfADay.setDailyEntries(dailyEntries);
        return entryOfADay;
    }

}
