package org.v2.finance.expenses.core;

import com.google.gson.Gson;

import java.util.List;

/**
 * @author Vivek Verma
 * @since 13-10-2019
 */
public class VersionCheck {

    private String version;
    private double versionRemote;
    private final String update;
    private final List<VersionInfo> ChangeLog;

    public VersionCheck(String version, String update, List<VersionInfo> changeLog) {
        this.version = version;
        this.update = update;
        this.ChangeLog = changeLog;
        this.versionRemote = convertStringToDouble(version);
    }

    public void computeActualVersion() {
        this.versionRemote = convertStringToDouble(version);
    }

    double convertStringToDouble(String versionString) {
        String[] parts = versionString.split("\\.");
        int p0 = Integer.parseInt(parts[0]);
        if (parts.length == 4) { //1.0.6.5
            int p1 = Integer.parseInt(parts[1]);
            int p2 = Integer.parseInt(parts[2]);
            int p3 = Integer.parseInt(parts[3]); //decimal offset
            return p0 * 100 + p1 * 10 + p2 + 0.1 * p3;
        } else if (parts.length == 3) { //1.0.6
            int p1 = Integer.parseInt(parts[1]);
            int p2 = Integer.parseInt(parts[2]);
            return p0 * 100 + p1 * 10 + p2;
        } else if (parts.length == 2) { //1.6
            int p1 = Integer.parseInt(parts[1]);
            return p0 * 100 + p1 * 10;
        } else if (parts.length == 1) { //1
            return p0 * 100;
        }
        return -1L; //shouldn't happen, means ~ 0 length / >3 length
    }

    public boolean isNewVersionAvailable(String existingVersion) {
        double existingVer = convertStringToDouble(existingVersion);
        return this.versionRemote > existingVer;
    }

    public static VersionCheck generateVersionCheckFromVersionHitText(String versionHitText) {
        Gson gson = new Gson();
        VersionCheck versionCheck = gson.fromJson(versionHitText, VersionCheck.class);
        versionCheck.computeActualVersion();
        return versionCheck;
    }

    public String getVersion() {
        return version;
    }

    double getVersionRemote() {
        return versionRemote;
    }

    public String getUpdate() {
        return update;
    }

    public List<VersionInfo> getChangeLog() {
        return ChangeLog;
    }

    //ONLY FOR TESTING PURPOSE
    public void setVersion(String version) {
        this.version = version;
    }
}
