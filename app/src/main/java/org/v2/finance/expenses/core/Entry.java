package org.v2.finance.expenses.core;

import org.v2.finance.expenses.util.CurrencyUtil;

import java.io.Serializable;
import java.util.Locale;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.FORMAT_ENTRY_COMMENT;
import static org.v2.finance.expenses.constants.Constants.FORMAT_ENTRY_POS_NEG;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 20-09-2018
 * <p>
 * The cellular level class
 */
public class Entry implements Serializable {

    //no param is final, because anything can change later in the run, not from any other class, but by the user

    private String entryType;
    private String currency = CurrencyUtil.currencySignToDisplay;
    private double amount;
    private String description;

    private boolean isEntryNormal; //i.e. an entry and not a note

    //for the new record
    public Entry() {
        this.amount = DEFAULT_AMOUNT;

        isEntryNormal = true; //because this empty object is only called when creating a new entry on screen-3
    }

    //for the billing type of entry
    public Entry(String entryType, double amount, String description) {
        this.entryType = entryType;
        this.amount = amount;
        this.description = description;

        isEntryNormal = true;
        enrichDescription();
    }

    //for the comment type of entry
    public Entry(String description) {
        this.description = description.substring(description.indexOf(ENTRY_TYPE_COMMENT) + ENTRY_TYPE_COMMENT.length()).trim();
        entryType = ENTRY_TYPE_COMMENT;
        amount = DEFAULT_AMOUNT;

        isEntryNormal = false;
        enrichDescription();
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        enrichDescription();
    }

    private void enrichDescription() {
        this.description = this.description.replaceAll("::", " -- ");
        this.description = this.description.trim(); //to remove redundant new lines
    }

    public boolean isEntryNormal() {
        return isEntryNormal;
    }

    public void setEntryNormal(boolean entryNormal) {
        isEntryNormal = entryNormal;
    }

    @Override
    public String toString() {
        if (isEntryNormal)
            return String.format(Locale.getDefault(), FORMAT_ENTRY_POS_NEG, entryType, currency, amount, description);
        return String.format(Locale.getDefault(), FORMAT_ENTRY_COMMENT, description);
    }
}