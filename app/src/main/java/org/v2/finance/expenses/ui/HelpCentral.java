package org.v2.finance.expenses.ui;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.v2.finance.expenses.R;

import java.util.Objects;

import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;

/**
 * @author Vivek Verma
 * @since 14-10-2019
 */
public class HelpCentral extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        deactivateHeavyImportingFlag();

        Toolbar toolbar = findViewById(R.id.toolbar_help);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }
    }
}
