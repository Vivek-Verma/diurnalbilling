package org.v2.finance.expenses.transporters;

import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;

import java.util.LinkedList;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 10-12-2018
 * @since 10-12-2018
 * <p>
 * Used by the HeavyImporter to get data from the UnitImporter.
 */
public class Transporter {

    private final Boolean importSuccessful;
    private final double dailyAmount;
    private final EntryOfADay entryOfADay;
    private final LinkedList<Entry> dailyEntries;
    private final String dataOfEntryOfADay;
    private Boolean bypassTransport;

    public Transporter(Boolean importSuccessful, double dailyAmount, EntryOfADay entryOfADay, LinkedList<Entry> dailyEntries, String dataOfEntryOfADay) {
        this.importSuccessful = importSuccessful;
        this.dailyAmount = dailyAmount;
        this.entryOfADay = entryOfADay;
        this.dailyEntries = dailyEntries;
        this.dataOfEntryOfADay = dataOfEntryOfADay;
        this.bypassTransport = false;
    }

    public Transporter(Boolean bypassTransport) {
        this(false, Constants.DEFAULT_DOUBLE_VALUE, null, null, null);
        this.bypassTransport = bypassTransport;
    }

    public Boolean isImportSuccessful() {
        return importSuccessful;
    }

    public Boolean isBypassTransport() {
        return bypassTransport;
    }

    public double getDailyAmount() {
        return dailyAmount;
    }

    public EntryOfADay getEntryOfADay() {
        return entryOfADay;
    }

    public LinkedList<Entry> getDailyEntries() {
        return dailyEntries;
    }

    public String getDataOfEntryOfADay() {
        return dataOfEntryOfADay;
    }
}
