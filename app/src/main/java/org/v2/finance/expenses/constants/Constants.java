package org.v2.finance.expenses.constants;

import android.app.AlarmManager;
import android.graphics.Color;
import android.os.Environment;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import java.io.File;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 20-09-2018
 */
public class Constants {

    //LIVE CONSTANTS (Controllers)
    public final static int RADIUS_DATA_POINT_GRAPH = 10;
    public final static int MAX_MONTHS_ON_GRAPH = 12;
    public static final int MAX_SLEEP_TIME_WHILE_SEARCHING = 26; //milliseconds
    public static final int SEARCH_SLEEP_TWEAK_RECORDS_THRESHOLD = 365;
    public static final int MAX_SLEEP_TIME_WHILE_SEARCHING_MORE_THAN_THRESHOLD_RECS = 1; //milliseconds - downgraded from 3 to 1 in DIB-98

    //STRAIGHT FORWARD MAPPINGS
    public final static String NEW_LINE = "\n";
    public final static String EMPTY_STR = "";
    public final static String LINE_SPLITTER_SPACE = " ";
    public final static String REGEX_HYPHEN_DATE_SPLITTER = "[-]";
    public final static String REGEX_SPACE = "\\s+";
    public static final String NULL_STR_STR = "null";

    //DEFAULTS
    public static final int ZERO_INT = 0;
    public static final int DEFAULT_INT_VALUE = -1;
    public static final int DEFAULT_USER_ISD_CODE = 91;
    public static final long DEFAULT_LONG_VALUE = -1L;
    public static final boolean DEFAULT_BOOLEAN_VALUE = false;
    public static final double DEFAULT_DOUBLE_VALUE = Double.NaN;
    public static final double PRECISION = 1E-6;
    public static final Double MIN_DOUBLE = (double) Long.MIN_VALUE;
    public static final Double MAX_DOUBLE = (double) Long.MAX_VALUE;
    public static final Double ZERO_DOUBLE = 0.00;
    public final static UserMappingProto.Currency DEFAULT_CURRENCY = UserMappingProto.Currency.INR;
    public final static String DEFAULT_INPUT_TITLE_FROM_FAULTY_FILE = "###";
    public final static String DEFAULT_TITLE = "-TITLE-";
    public final static double DEFAULT_AMOUNT = 0.00;
    public static final String DEFAULT_DAY_OF_WEEK = "---";
    public static final String DEFAULT_DESCRIPTION = "NO-DESC";
    public static final long USER_NON_EXISTENT_CODE = -999;
    public static final long DEFAULT_TIMESTAMP = DEFAULT_LONG_VALUE;
    public static final String DEFAULT_USER_EMAIL = EMPTY_STR;
    public static final long DEFAULT_USER_MOBILE = DEFAULT_LONG_VALUE;
    public static final String DEFAULT_USER_CRED_HASH = EMPTY_STR;
    public static final Boolean DEFAULT_PREMIUM_USER_STATUS = false;
    public static final String DEFAULT_USER_NAME = EMPTY_STR;

    //STRING FORMATTER
    public static final String FORMAT_ENTRY_OF_A_DAY = "%s %s :: %s %s %.2f";
    public static final String FORMAT_ENTRY_POS_NEG = "%s %s %.2f : %s";
    public static final String FORMAT_ENTRY_COMMENT = "//%s";
    public final static String FORMAT_DISPLAY_AMOMUNT = "%.2f";
    public static final String FORMAT_INTERNAL_FILE_HEADER = "%s/%s/%s";
    public static final String FORMAT_INTERNAL_FILE = "%s%s"; //FILE_HEADER + date
    public final static String FORMAT_INTERNAL_FOLDER_PATH = "%s/%s/";
    public static final String FORMAT_EXTERNAL_BACKUP_FOLDER_LOCATION = "%s/%s/";
    public static final String FORMAT_EXTERNAL_BACKUP_FILE_LOCATION = "%s/%s";
    public static final String FORMAT_GRAPH_X_AXIS_DISPLAY = "%s/%s"; //11/19
    public static final String FORMAT_MONTH_EXPENSE_ROW_DIALOG_BOX = "\u2022 %s :   %s %s %.2f";
    public static final String FORMAT_MONTHLY_EXPENSE_KEY_INTERNAL = "%s-%s"; //2019-11
    public static final String FORMAT_MONTHLY_EXPENSE_KEY_DIALOG = "%s '%s"; //Nov '19
    public static final String FORMAT_GRAND_TOTAL_DISPLAY_DATE = "%s %s %s"; //20 Nov 19
    public static final String FORMAT_DATE_PICKER_DISPLAY_DATE = "%02d-%02d-%d";
    public static final String FORMAT_FILE_TITLE_DATE = "%s_%s_%s";
    public static final String FORMAT_DISPLAY_DATE = "dd-MM-yyyy";
    public static final String FORMAT_CHANGELOG_HEADER = "%s :: %s";

    //FOR RENDERING VARIOUS LISTS
    public static final String[] monthNames = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    //For Requesting Ad click
    public static final String AD_CLICK_REQ_MSG = "If you like this App, kindly click on one of your favorite Ads occasionally :)";

    //For displaying terse imp instructions
    public static final String TERSE_HELP_MSG = new StringBuffer()
            .append("1. For saving day entries, click on the floppy disk icon (2nd last in top right), otherwise the entries won't be saved.").append(NEW_LINE).append(NEW_LINE)
            .append("2. The entry type '//' is a comment - entry with no amount, but description can be added, like a note!").append(NEW_LINE).append(NEW_LINE)
            .append("3. Entire day record can be deleted from 1st screen via the garbage icon in top right.").append(NEW_LINE).append(NEW_LINE)
            .append("4. Entries can be deleted from Day Record screen via the garbage icon.").append(NEW_LINE).append(NEW_LINE)
            .append(NEW_LINE)
            .append("Detailed instructions in the Help and About section").toString();

    //Enum for notification types
    public enum NotificationCaller {
        APK_UPDATE_CHECK, DAILY_ENTRY_CHECK
    }

    //For notification of daily check up on entries to be done
    public static final String[] NOTIFICATION_ENTRY_PROMPT_SPACE = {
            "Got any expenses today? This month : %s%s%.2f",
            "Ooh, a no expense day? That a feat. This month : %s%s%.2f",
            "Received some money? Better keep track. This month : %s%s%.2f",
            "Traders love having flat position, zero risk at End of Day. You seem to be one of them. This month : %s%s%.2f",
            "Your money warden is thinking about you. This month : %s%s%.2f"
    };
    public static final int DAILY_ENTRY_PROMPT_HOUR = 21;
    public static final int DAILY_ENTRY_PROMPT_MINUTES = 30;
    public static final int DAILY_ENTRY_PROMPT_SECONDS = 0;

    //VARIOUS ENUMS
    public enum Caller {
        CALLER_MAIN_ACTIVITY, CALLER_VIEW_DAY_RECORD
    }

    public enum CallersForDeletion {
        DEL_VIEW_DAY_REC, DEL_MAIN_SCREEN
    }

    public enum GraphingExceptionCases {
        MONTH_LIST_NULL, MONTH_LIST_EMPTY, MONTH_LIST_SINGLE_PARAM_ONLY
    }

    public final static char DATE_SPLITTER_CHAR = '-';
    public final static String STRING_NULL_OBJ = null;

    public final static String KEY_TO_DAILY_RECORD = "KEY_TO_DAILY_RECORD";
    public final static String KEY_TO_DAILY_ENTRY = "KEY_TO_DAILY_ENTRY";
    public final static String KEY_TO_DAILY_ENTRY_DATE_FROM_UP = "KEY_TO_DAILY_ENTRY_DATE_FROM_UP";
    public final static String DATEPICKER_NAME = "datePicker";

    public final static int INITIAL_AD_BUFFER_MAP = 101;
    public final static int AD_SPREAD_INTERVAL = 3;
    public final static int MAX_SAVES_ALLOWED_BEFORE_ROLL = (INITIAL_AD_BUFFER_MAP - 1) * AD_SPREAD_INTERVAL;

    //FOR FILE SYS INTERACTION
    public final static File EXTERNAL_STORAGE = Environment.getExternalStorageDirectory();
    public final static String EXTERNAL_FOLDER_FOR_BACKUP = "Daily E-Billing";
    public final static String EXTERNAL_BACKUP_FILE_NAME = "backup_daily_ebilling.txt";
    public final static String BILLING_DIRECTORY_NAME = "billingRes";
    public final static String BILLING_FILE_NAME_PREFIX = "bill_";
    public final static String BILLING_FILE_EXTENSION = ".txt";
    public final static String FILE_DIRECTORY_HIERARCHY = "/";
    public static String FILE_PATH_PREFIX_INTERNAL_STORAGE = ""; //be careful to update this variable before the full execution is in place
    /*
        Updated in Helper from MainActivity
        The full path of the files usually be like :-
        1. Emulator: /data/user/0/org.v2.outlet.dailyTrackExpz/files/billingRes/bill_2019_06_12
        2. My phone:
        */
    public static String FILE_HEADER_INTERNAL_FILE = ""; ///data/user/0/org.v2.outlet.dailyTrackExpz/files/billingRes/bill_
    public static String INTERNAL_FOLDER = ""; ///data/user/0/org.v2.outlet.dailyTrackExpz/files/billingRes/
    public static final String EXTERNAL_BACKUP_FOLDER_LOCATION = String.format(FORMAT_EXTERNAL_BACKUP_FOLDER_LOCATION, EXTERNAL_STORAGE.getAbsolutePath(), EXTERNAL_FOLDER_FOR_BACKUP);
    public static final String EXTERNAL_BACKUP_FILE_LOCATION = String.format(FORMAT_EXTERNAL_BACKUP_FILE_LOCATION, EXTERNAL_BACKUP_FOLDER_LOCATION, EXTERNAL_BACKUP_FILE_NAME);

    public final static String MSG_IF_QUERIED_DATE_REC_EXISTS = "Entry in current date already exists. Loading...";

    //FOR INTERNAL PREFERNCES - SHARED PREFS
    public static final String MY_PREFS_NAME = "DIURNAL_BILLING_PREF";
    public static final String KEY_LONG_LAST_LAUNCHED_TIMESTAMP = "lastLaunchedTimeStamp";
    public static final String KEY_FLOAT_CURRENT_MONTH_EXPENSE = "currentMonthExpense";
    public static final String KEY_STR_CURRENCY = "currency";
    public static final String KEY_STR_USER_NAME = "userName";
    public static final String KEY_STR_PREMIUM_USER = "premiumUserStatus";
    public static final String KEY_STR_USER_EMAIL = "userEmail";
    public static final String KEY_INT_USER_ISD = "userIsd";
    public static final String KEY_LONG_USER_MOBILE = "userMobile";
    public static final String KEY_STR_USER_CRED_HASH = "userCredHash";
    public static final String KEY_LONG_USER_CRED_HASH_EXPIRY_TIMESTAMP = "userCredHashExpiryTimeStamp";
    public static final String KEY_LONG_LAST_SAVED_TIMESTAMP = "lastSavedTimeStamp";
    public static final String KEY_LONG_LAST_CLOUD_UPLOAD_TIMESTAMP = "lastCloudUploadTimeStamp";
    public static final String KEY_LONG_PAYMENT_EXPIRY_TIMESTAMP = "paymentExpiryTimeStamp";

    public static final String KEY_EXTRAS_INTENT_TX_DATA = "TX_DATA";
    public static final String KEY_EXTRAS_INTENT_PROPERTY_READER = "PROPERTY_READER";

    public static final long USER_SIGN_IN_EXPIRED = -13L;

    //COLORS
    public final static int RED = Color.RED;
    public final static int GREEN_DARK = Color.parseColor("#006400");
    public final static int GRAY = Color.GRAY;
    public final static int LT_YELLOW = Color.parseColor("#F6E2A8");
    public final static int BLACK = Color.BLACK;

    //EOAD HEADER
    //24-07-2018 ### :: - ₹ 60.0
    public final static int DATE_LINE_DATE = 0;
    public final static int DATE_LINE_TITLE = 1;
    public final static int DATE_LINE_CURRENCY = 4;

    //ENTRY LEVEL
    //- ₹ 50 : onion uttapum
    public final static int ENTRY_LINE_ENTRY_TYPE = 0;
    public final static int ENTRY_LINE_CURRENCY = 1;
    public final static int ENTRY_LINE_AMOUNT = 2;
    public final static String ENTRY_TYPE_COMMENT = "//";
    public final static String ENTRY_TYPE_POSITIVE = "+";
    public final static String ENTRY_TYPE_NEGATIVE = "-";
    public final static String CURRENCY_INR = "₹";
    public final static String CURRENCY_USD = "$";
    public final static String CURRENCY_CND = "C$";
    public final static String CURRENCY_YEN = "¥";
    public final static String CURRENCY_EUR = "€";
    public final static String CURRENCY_GBP = "£";
    public final static String CURRENCY_RUB = "₽";
    public final static String CURRENCY_FR = "₣";

    //MAIN ROUTING MECHANISM -- SUPER CRITICAL
    public final static int ROUTING_TO_NEW_DAY_RECORD = 1;
    public final static int ROUTING_TO_VIEW_DAY_RECORD = 2;
    public final static int ROUTING_TO_NEW_ENTRY = 3;
    public final static int ROUTING_TO_VIEW_ENTRY = 4;
    public final static int ROUTING_FROM_ENTRY = 5; //to tx from entry to daily rec from backPressed
    public final static int ROUTING_FROM_DAY_RECORD = 6; //to tx from daily rec to entire billing from backPressed

    //FOR THE UPDATER
    public final static String DEFAULT_HOST_FOR_PINGING = "https://www.google.com";
    public final static int CONNECTION_TIMEOUT_HOST_PINGING = 10 * 1000;
    public final static String ERR_WHILE_VERSION_CHECK = "ERR_WHILE_VERSION_CHECK";
    public final static String ERR_WHILE_GDRIVE_CHECK = "ERR_WHILE_GDRIVE_CHECK_%s";
    public final static String GDRIVE_SITE_FORMAT = "https://drive.google.com/uc?id=%s&export=download";
    public final static String GITHUB_SITE_FORMAT = "https://raw.githubusercontent.com/kaizen-samurai/supply-line/master/d-b/versions/%s";
    public final static String ID_SITE_VERSION_CHECK = "16AWJpUZpHUspb3wkfnOgXB6bmwZLjg8O"; //amaterasu_billz.json
    public final static String ID_SITE_REMOTE_SETTINGS = "13JgAdNssn3qPvZNsDa8n66Sxsc0oNCet"; //amaterasu_settings.json
    //public final static String ID_SITE_VERSION_CHECK = "1-P7A0lz_EDRFn6mBlqPN0ou2cKYUmkYi"; //amaterasu-testing.json
    //public final static String DOWNLOAD_LOCATION_EXTERNAL = "/mnt/sdcard/Download"; //not good to use direct path
    public final static String DOWNLOAD_LOCATION_EXTERNAL = EXTERNAL_STORAGE + "/" + Environment.DIRECTORY_DOWNLOADS + "/";
    public final static String DOWNLOADED_APK_NAME = "dailyEBilling.apk";
    public final static long DIVIDER_FOR_MB = 1024 * 1024;
    public final static long ESTIMATE_DOWNLOAD_SIZE = 6 * DIVIDER_FOR_MB; //update 'updater_download_approx_size' in strings.xml if this changes
    //#VERSION_UPDATE_SHOULD_UPDATE_THIS

    //FOR ALARM BASED NETWORK APK VERSION CHECKING
    public static final long REPEAT_INTERVAL_APK_CHECK = AlarmManager.INTERVAL_HOUR * 4;
    public static final long REPEAT_INTERVAL_DAILY_ENTRY_CHECK = AlarmManager.INTERVAL_DAY;
    public static final int START_HOUR_OF_DAY_APK_CHECK = 1; //1 AM

    //Property Keys
    public static final String APP_VERSION = "app.version";
    public static final String APP_NAME_SHORT = "app.name.short";
    public static final String APP_NAME_LONG = "app.name.long";
    public static final String APP_FB = "app.fb";
    public static final String APP_DEV = "app.dev";
    public static final String APP_NOTIF_ID_APK_UPDATE = "app.notif.idApkUpdate";
    public static final String APP_NOTIF_ID_DAILY_CHECK = "app.notif.idDailyCheck";
    public static final String APP_SIZE = "app.size";
    public static final String APP_BG_REPEAT_INTERVAL_APK_CHECK_HOURS = "app.bg.repeat.interval.apk.check.hours";
    public static final String APP_BG_REPEAT_INTERVAL_DAILY_CHECK_DAYS = "app.bg.repeat.interval.daily.check.days";
    public static final String APP_BG_DAILY_PROMPT_HOUR = "app.bg.daily.prompt.hour";
    public static final String APP_BG_DAILY_PROMPT_MINZ = "app.bg.daily.prompt.minz";
    public static final String APP_BG_DAILY_PROMPT_SECS = "app.bg.daily.prompt.secs";
    public static final String ADS_ACTIVATE = "ads.activate";
    public static final String ADS_BANNER_ACTIVATE = "ads.banner.activate";
    public static final String ADS_INTERSTITIAL_ACTIVATE = "ads.interstitial.activate";
    public static final String ADS_INTERSTITIAL_SCHEDULED_POLLING_FOR_AD_REQ = "ads.interstitial.scheduledPollingForAdReq";
    public static final String ADS_ADMOB_ID_APP = "ads.admob.id.app";
    public static final String ADS_ADMOB_ID_BANNER = "ads.admob.id.banner";
    public static final String ADS_ADMOB_ID_INTERSTITIAL = "ads.admob.id.interstitial";

    public static final String DIURNAL_SERVER_HOST_DBI_IS_HTTP = "diurnal.server.host.dbi.isHttp";
    public static final String DIURNAL_SERVER_HTTP_HOST_DBI = "diurnal.server.http.host.dbi";
    public static final String DIURNAL_SERVER_HTTPS_HOST_DBI = "diurnal.server.https.host.dbi";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PING = "diurnal.server.endpoint.dbi.ping";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PING_TIMEOUT = "diurnal.server.endpoint.dbi.ping.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN = "diurnal.server.endpoint.dbi.signin";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT = "diurnal.server.endpoint.dbi.signin.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL = "diurnal.server.endpoint.dbi.signup.check.email";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL_TIMEOUT = "diurnal.server.endpoint.dbi.signup.check.email.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP = "diurnal.server.endpoint.dbi.signup";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_TIMEOUT = "diurnal.server.endpoint.dbi.signup.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO = "diurnal.server.endpoint.dbi.update.user.info";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_UPDATE_USER_INFO_TIMEOUT = "diurnal.server.endpoint.dbi.update.user.info.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PUSH_LAST_LOCAL_SAVE_TIMESTAMP = "diurnal.server.endpoint.dbi.push.last.local.save.ts";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PUSH_LAST_LOCAL_SAVE_TIMESTAMP_TIMEOUT = "diurnal.server.endpoint.dbi.push.last.local.save.ts.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE = "diurnal.server.endpoint.dbi.push.backup.entire";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE_TIMEOUT = "diurnal.server.endpoint.dbi.push.backup.entire.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE = "diurnal.server.endpoint.dbi.retrieve.backup.entire";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_RETRIEVE_BACKUP_ENTIRE_TIMEOUT = "diurnal.server.endpoint.dbi.retrieve.backup.entire.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_USER = "diurnal.server.endpoint.dbi.user";
    public static final String DIURNAL_SERVER_ENDPOINT_DBI_CRED = "diurnal.server.endpoint.dbi.cred";

    public static final String DIURNAL_SERVER_HOST_INTERACTION_IS_HTTP = "diurnal.server.host.interaction.isHttp";
    public static final String DIURNAL_SERVER_HTTP_HOST_INTERACTION = "diurnal.server.http.host.interaction";
    public static final String DIURNAL_SERVER_HTTPS_HOST_INTERACTION = "diurnal.server.https.host.interaction";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_PING = "diurnal.server.endpoint.interaction.ping";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_PING_TIMEOUT = "diurnal.server.endpoint.interaction.ping.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE = "diurnal.server.endpoint.interaction.otp.generate";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_GENERATE_TIMEOUT = "diurnal.server.endpoint.interaction.otp.generate.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY = "diurnal.server.endpoint.interaction.otp.verify";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_OTP_VERIFY_TIMEOUT = "diurnal.server.endpoint.interaction.otp.verify.timeout";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_USER = "diurnal.server.endpoint.interaction.user";
    public static final String DIURNAL_SERVER_ENDPOINT_INTERACTION_CRED = "diurnal.server.endpoint.interaction.cred";

}

/*
 Super fast python code for the same =>

 for prop in props.split("\n"):
     prop = prop[:prop.index('=')]
     key = prop.replace(".","_").upper()
     print(f"public static final String {key} = \"{prop}\";")
 */
