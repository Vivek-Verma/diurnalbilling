package org.v2.finance.expenses.connectivity;

import org.v2.finance.expenses.contracts.GDriveConnector;
import org.v2.finance.expenses.contracts.GDriveLocators;

/**
 * @author Vivek Verma
 * @since 31-10-2019
 */
public class VersionCheckPinger extends AbstractGDriveDataImporter {

    public VersionCheckPinger(GDriveConnector connector, String id) {
        super(id, connector, GDriveLocators.VERSION);
    }

}