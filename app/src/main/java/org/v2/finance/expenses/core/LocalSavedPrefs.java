package org.v2.finance.expenses.core;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;

import java.io.Serializable;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_CURRENCY;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_PREMIUM_USER_STATUS;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_CRED_HASH;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_EMAIL;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_ISD_CODE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_MOBILE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_NAME;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 03-06-2019
 * Acts as the running wrapper over the shared preferences and is used for updating actual internal cache shared pref
 */
public class LocalSavedPrefs implements Serializable {

    private final Logger LOGGER = LoggerFactory.getLogger(LocalSavedPrefs.class);

    private Long lastUsedTimeStamp;
    private float currentMonthExpense;
    private UserMappingProto.Currency currency;

    private String userName;
    private Boolean premiumUser;
    private String userEmail;
    private Integer userIsdCode;
    private Long userMobile;
    private String userCredHash;
    private Long userCredHashExpiryTimestamp;
    private Long lastSavedTimeStamp;
    private Long lastCloudUploadTimeStamp;
    private Long paymentExpiryTimestamp;

    public LocalSavedPrefs() {
        this.lastUsedTimeStamp = DEFAULT_TIMESTAMP;
        this.currentMonthExpense = (float) DEFAULT_AMOUNT;
        this.currency = DEFAULT_CURRENCY;

        this.userName = DEFAULT_USER_NAME;
        this.premiumUser = DEFAULT_PREMIUM_USER_STATUS;
        this.userEmail = DEFAULT_USER_EMAIL;
        this.userIsdCode = DEFAULT_USER_ISD_CODE;
        this.userMobile = DEFAULT_USER_MOBILE;
        this.userCredHash = DEFAULT_USER_CRED_HASH;
        this.userCredHashExpiryTimestamp = DEFAULT_TIMESTAMP;
        this.lastSavedTimeStamp = DEFAULT_TIMESTAMP;
        this.lastCloudUploadTimeStamp = DEFAULT_TIMESTAMP;
        this.paymentExpiryTimestamp = DEFAULT_TIMESTAMP;
    }

    public Long getLastUsedTimeStamp() {
        return lastUsedTimeStamp;
    }

    public void setLastUsedTimeStamp(Long lastUsedTimeStamp) {
        this.lastUsedTimeStamp = lastUsedTimeStamp;
        if (BuildConfig.DEBUG) LOGGER.info("Updating last used time stamp to {}", this.lastUsedTimeStamp);
    }

    public float getCurrentMonthExpense() {
        return currentMonthExpense;
    }

    public void setCurrentMonthExpense(double currentMonthExpense) {
        this.currentMonthExpense = (float) currentMonthExpense;
        if (BuildConfig.DEBUG) LOGGER.info("Updating current month expense to {}", this.currentMonthExpense);
    }

    public UserMappingProto.Currency getCurrency() {
        return currency;
    }

    public void setCurrency(UserMappingProto.Currency currency) {
        this.currency = currency;
        if (BuildConfig.DEBUG) LOGGER.info("Updating currency to {}", this.currency);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        if (BuildConfig.DEBUG) LOGGER.info("Updating username to {}", this.userName);
    }

    public Boolean isPremiumUser() {
        return premiumUser;
    }

    public void setPremiumUserStatus(Boolean premiumUserStatus) {
        this.premiumUser = premiumUserStatus;
        if (BuildConfig.DEBUG) LOGGER.info("Updating premium user status to {}", this.premiumUser);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        if (BuildConfig.DEBUG) LOGGER.info("Updating user email to [{}]", this.userEmail);
    }

    public Integer getUserIsdCode() {
        return userIsdCode;
    }

    public void setUserIsdCode(Integer userIsdCode) {
        this.userIsdCode = userIsdCode;
        if (BuildConfig.DEBUG) LOGGER.info("Updating ISD code to {}", this.userIsdCode);
    }

    public Long getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(Long userMobile) {
        this.userMobile = userMobile;
        if (BuildConfig.DEBUG) LOGGER.info("Updating user mobile to [{}]", this.userMobile);
    }

    public String getUserCredHash() {
        return userCredHash;
    }

    public void setUserCredHash(String userCredHash) {
        this.userCredHash = userCredHash;
        if (BuildConfig.DEBUG) LOGGER.info("Updating user credential hash to [{}]", this.userCredHash);
    }

    public Long getUserCredHashExpiryTimestamp() {
        return userCredHashExpiryTimestamp;
    }

    public void setUserCredHashExpiryTimestamp(Long userCredHashExpiryTimestamp) {
        this.userCredHashExpiryTimestamp = userCredHashExpiryTimestamp;
        if (BuildConfig.DEBUG) LOGGER.info("Updating user credential hash expiry to [{}]", this.userCredHashExpiryTimestamp);
    }

    public Long getLastSavedTimeStamp() {
        return lastSavedTimeStamp;
    }

    public void setLastSavedTimeStamp(Long lastSavedTimeStamp) {
        this.lastSavedTimeStamp = lastSavedTimeStamp;
        if (BuildConfig.DEBUG) LOGGER.info("Updating last-saved timestamp to [{}]", this.lastSavedTimeStamp);
    }

    public Long getLastCloudUploadTimeStamp() {
        return lastCloudUploadTimeStamp;
    }

    public void setLastCloudUploadTimeStamp(Long lastCloudUploadTimeStamp) {
        this.lastCloudUploadTimeStamp = lastCloudUploadTimeStamp;
        if (BuildConfig.DEBUG) LOGGER.info("Updating last-cloud-upload timestamp to [{}]", this.lastCloudUploadTimeStamp);
    }

    public Long getPaymentExpiryTimestamp() {
        return paymentExpiryTimestamp;
    }

    public void setPaymentExpiryTimestamp(Long paymentExpiryTimestamp) {
        this.paymentExpiryTimestamp = paymentExpiryTimestamp;
        if (BuildConfig.DEBUG) LOGGER.info("Updating payment-expiry timestamp to [{}]", this.paymentExpiryTimestamp);
    }
}
