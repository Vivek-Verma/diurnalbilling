package org.v2.finance.expenses.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.MainActivity;
import org.v2.finance.expenses.R;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-06-2019
 * @since 17-06-2019
 */
public class DeletionAdapterForMainScreen extends AbstractDeletionAdapter {

    private final Logger logger = LoggerFactory.getLogger(DeletionAdapterForMainScreen.class);

    public <T> DeletionAdapterForMainScreen(Context context, int layoutToBeFollowed, T[] resource) {
        super(context, layoutToBeFollowed, resource);
        if (BuildConfig.DEBUG) logger.info("Resource's size as received in grand deletion's custom adapter : {}", resource.length);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (BuildConfig.DEBUG) logger.debug("Entering the getView part of the DeletionAdapterForMainScreen..");
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        convertView = inflater.inflate(layoutToBeFollowed, parent, false);

        CheckBox checkBox = convertView.findViewById(R.id.list_check_row);
        checkBox.setText(modelItems[position].getName());

        checkBox.setOnCheckedChangeListener((compoundButton, isSelected) -> {
            if (isSelected) {
                MainActivity.getGrandDateRecordsToBePurged().add(position);
            } else {
                MainActivity.getGrandDateRecordsToBePurged().remove(position);
            }
        });
        checkBox.setChecked(modelItems[position].isCheckboxActive());
        return convertView;
    }
}
