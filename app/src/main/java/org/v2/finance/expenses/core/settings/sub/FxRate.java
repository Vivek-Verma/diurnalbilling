package org.v2.finance.expenses.core.settings.sub;

public class FxRate {
    private String curr;
    private Double rate;

    public FxRate() {
    }

    public FxRate(String curr, Double rate) {
        this.curr = curr;
        this.rate = rate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FxRate{");
        sb.append("curr='").append(curr).append('\'');
        sb.append(", rate=").append(rate);
        sb.append('}');
        return sb.toString();
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
