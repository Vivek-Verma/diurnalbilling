package org.v2.finance.expenses.port.importer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.transporters.Transporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import static org.v2.finance.expenses.constants.Constants.DATE_LINE_DATE;
import static org.v2.finance.expenses.constants.Constants.DATE_LINE_TITLE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_LINE_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_LINE_ENTRY_TYPE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.LINE_SPLITTER_SPACE;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;

/**
 * @author : Vivek
 * @version : 1.0
 * @lastMod 08-04-2021
 * @since : 22-09-2018
 */
public class UnitImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnitImporter.class);

    public static String triggerStringImportFromSavedFile(String fileToBeRead) {
        return triggerStringImportFromSavedFile(new File(fileToBeRead));
    }

    public static String triggerStringImportFromSavedFile(File fileToBeRead) {
        StringBuilder data = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(fileToBeRead))) {
            String line;
            while ((line = in.readLine()) != null) data.append(line).append(NEW_LINE);
        } catch (IOException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Error encountered whilst reading in the file : '{}'. Error : ", fileToBeRead.getName(), e);
        }
        return data.toString();
    }

    public static Transporter triggerImportFromSavedFile(String fileToBeRead) {
        return triggerImportFromSavedFile(new File(fileToBeRead));
    }

    public static Transporter triggerImportFromSavedFile(File fileToBeRead) {
        double dailyAmount = 0;
        LinkedList<Entry> dailyEntries = new LinkedList<>();
        EntryOfADay entryOfADay = new EntryOfADay();
        StringBuilder data = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new FileReader(fileToBeRead))) {
            int entryLines = 0;
            String line;
            while ((line = in.readLine()) != null) {
                if (line.startsWith(ENTRY_TYPE_NEGATIVE) || line.startsWith(ENTRY_TYPE_POSITIVE) || line.startsWith(ENTRY_TYPE_COMMENT)) {
                    Entry entry = processLineTypeEntry(line);
                    dailyEntries.add(entry);

                    double localEntryAmount = entry.getAmount();
                    dailyAmount += ENTRY_TYPE_NEGATIVE.equals(entry.getEntryType()) ? localEntryAmount * -1 : localEntryAmount;
                    entryLines++;
                } else {
                    processLineTypeNewDate(line, entryOfADay);
                }
                data.append(line).append(NEW_LINE);
            }
            in.close();
            if (entryLines > 0) {
                return new Transporter(true, dailyAmount, entryOfADay, dailyEntries, data.toString().toLowerCase());
            } else { // the entry lines being 0 signifies no ENTRIES only title, which has been deprecated since DIB-103
                fileToBeRead.delete();
                return new Transporter(true);
            }
        } catch (Exception e1) {
            if (BuildConfig.DEBUG) LOGGER.error("error occurred in file reading : ", e1);
        }
        return new Transporter(false);
    }

    /**
     * Sets the ground for entryOfADay
     *
     * @param line
     * @param entryOfADay
     */
    static void processLineTypeNewDate(String line, EntryOfADay entryOfADay) {
        if (BuildConfig.DEBUG) LOGGER.info("Entry of new date : {}", line);
        String[] lineArr = line.split(LINE_SPLITTER_SPACE);

        //24-07-2018 ### :: - ₹ 60.0 --> 24-07-2018 null :: - ₹ 60.0
        String displayDate = lineArr[DATE_LINE_DATE];
        String title, displayCurrency;
        title = lineArr[DATE_LINE_TITLE];
        //displayCurrency = lineArr[DATE_LINE_CURRENCY];

        entryOfADay.setDisplayDate(displayDate);
        if (BuildConfig.DEBUG) LOGGER.info("Title received whilst reading the file: {}", title);
        entryOfADay.setTitle(title);
        //displayamount and the sign is set when the entire calculation is done / as the calc is ongoing
        //entryOfADay.setDisplayCurrency(displayCurrency);
    }

    /**
     * Returns the entry
     *
     * @param line
     * @return
     */
    static Entry processLineTypeEntry(String line) {
        if (line.startsWith(ENTRY_TYPE_COMMENT)) { //type - 2
            return new Entry(line);
        }

        //type - 1
        String[] lineArr = line.split(LINE_SPLITTER_SPACE);
        //- ₹ 50 : onion uttapam
        String entryType = lineArr[ENTRY_LINE_ENTRY_TYPE];
        //String currency = lineArr[ENTRY_LINE_CURRENCY];
        double entryAmount = Double.parseDouble(lineArr[ENTRY_LINE_AMOUNT]);
        String description = line.substring(line.indexOf(':') + 2);

        return new Entry(entryType, entryAmount, description);
    }

}
