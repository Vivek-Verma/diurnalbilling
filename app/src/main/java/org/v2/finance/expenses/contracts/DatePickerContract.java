package org.v2.finance.expenses.contracts;

public interface DatePickerContract {

    void handleSelectedDate(String date);
}