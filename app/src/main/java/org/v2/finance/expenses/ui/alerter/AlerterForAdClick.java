package org.v2.finance.expenses.ui.alerter;

import android.content.Context;

/**
 * @author Vivek Verma
 * @since 28-11-2019
 */
public class AlerterForAdClick extends AbstractAlerterOnlyOk {

    public AlerterForAdClick(Context context, String title, String messageForDisplay) {
        super(context, title, messageForDisplay);
    }

}
