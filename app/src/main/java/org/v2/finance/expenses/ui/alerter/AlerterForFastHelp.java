package org.v2.finance.expenses.ui.alerter;

import android.content.Context;

/**
 * @author Vivek Verma
 * @since 25/12/19
 */
public class AlerterForFastHelp extends AbstractAlerterOnlyOk {

    public AlerterForFastHelp(Context context, String title, String messageForDisplay) {
        super(context, title, messageForDisplay);
    }
}
