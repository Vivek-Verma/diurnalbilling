package org.v2.finance.expenses.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.InitLoader;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.ui.Updater;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.NOTIFICATION_ENTRY_PROMPT_SPACE;

/**
 * @author Vivek Verma
 * @since 31-10-2019
 */
public class Notifier {

    private final Logger logger = LoggerFactory.getLogger(Notifier.class);
    private final Context context;
    private final Intent intent;
    private LocalSavedPrefs localSavedPrefs = null;

    private final Map<Constants.NotificationCaller, Integer> notificationCallerToIdMap = new HashMap<>();
    private final Map<Constants.NotificationCaller, Intent> notificationCallerToIntentMap = new HashMap<>();
    private final Map<Constants.NotificationCaller, String> notificationCallerToContentTextMap = new HashMap<>();

    public Notifier(Context context, Intent intent) {
        this.context = context;
        this.intent = intent;
        initNotificationChannel(); //to be done only once, hence call this constructor carefully

        fillUpTheMaps();
    }

    private void fillUpTheMaps() {
        notificationCallerToIdMap.put(Constants.NotificationCaller.DAILY_ENTRY_CHECK, R.string.app_notif_id_daily_check);
        notificationCallerToIdMap.put(Constants.NotificationCaller.APK_UPDATE_CHECK, R.string.app_notif_id_apk_update);

        notificationCallerToIntentMap.put(Constants.NotificationCaller.DAILY_ENTRY_CHECK, new Intent(context, InitLoader.class));
        notificationCallerToIntentMap.put(Constants.NotificationCaller.APK_UPDATE_CHECK, new Intent(context, Updater.class));

        notificationCallerToContentTextMap.put(Constants.NotificationCaller.APK_UPDATE_CHECK, "New version discovered for Daily e-Billing");
    }

    private void lateFillForContextText() {
        notificationCallerToContentTextMap.put(Constants.NotificationCaller.DAILY_ENTRY_CHECK, mineContentTextForDailyEntryCheck());
    }

    public void fireNotification() {
        final int notifId = Integer.parseInt(context.getString(notificationCallerToIdMap.get(Constants.NotificationCaller.valueOf(this.intent.getAction()))));
        final String notificationText = notificationCallerToContentTextMap.get(Constants.NotificationCaller.valueOf(this.intent.getAction()));
        final Intent intent = notificationCallerToIntentMap.get(Constants.NotificationCaller.valueOf(this.intent.getAction()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        final PendingIntent pendingIntent = obtainPendingIntent(notifId, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_channel_id))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notif_large_icon))
                .setSmallIcon(R.drawable.ic_notif_small_icon)
                .setContentText(notificationText)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent) //for directly launching update checker screen
                .setOnlyAlertOnce(true)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notifId, builder.build());
        if (BuildConfig.DEBUG) logger.info("Notification task completed for {}!", this.intent.getAction());
    }

    //doesn't work properly
    @Deprecated
    private boolean isNotificationVisible(int notifId, Intent intent) {
        return obtainPendingIntent(notifId, intent, PendingIntent.FLAG_NO_CREATE) != null;
    }

    private PendingIntent obtainPendingIntent(int notifId, Intent intent, int flag) {
        return PendingIntent.getActivity(context, notifId, intent, flag);
    }

    private String mineContentTextForDailyEntryCheck() {
        if (localSavedPrefs == null) {
            logger.warn("{} was the invoker for this function()! Keep watch on this call", this.intent.getAction());
            return "Something went wrong in notification! Contact the dev ASAP!";
        }
        double currentMonthExpense = localSavedPrefs.getCurrentMonthExpense();
        String sign = currentMonthExpense < 0 ? ENTRY_TYPE_NEGATIVE : ENTRY_TYPE_POSITIVE;
        currentMonthExpense = Math.abs(currentMonthExpense);
        int randomIndex = Helper.getRandomValue(NOTIFICATION_ENTRY_PROMPT_SPACE.length);
        if (randomIndex < 0 || randomIndex >= NOTIFICATION_ENTRY_PROMPT_SPACE.length) randomIndex = 0;
        return String.format(NOTIFICATION_ENTRY_PROMPT_SPACE[randomIndex], sign,
                CurrencyUtil.CURRENCY_SIGN.get(localSavedPrefs.getCurrency()), //not coming via the MainActivity track
                currentMonthExpense);
    }

    private void initNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                final CharSequence name = context.getString(R.string.app_channel_id);
                final String description = context.getString(R.string.app_channel_description);
                final String id = context.getString(R.string.app_channel_id);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(id, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                final NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                if (Objects.nonNull(notificationManager)) {
                    notificationManager.createNotificationChannel(channel);
                    if (BuildConfig.DEBUG) logger.info("Notification channel created");
                } else {
                    if (BuildConfig.DEBUG) logger.error("Cannot create notification channel because of NPE");
                }
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) logger.error("Error in initializing the notification channel: ", e);
        }
    }

    public void setLocalSavedPrefs(LocalSavedPrefs localSavedPrefs) {
        this.localSavedPrefs = localSavedPrefs;
        lateFillForContextText();
    }
}
