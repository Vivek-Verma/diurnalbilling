package org.v2.finance.expenses.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.adapter.TotalBillingListAdapter;
import org.v2.finance.expenses.constants.Constants;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.transporters.TransportEditText;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ToasterUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.MAX_SLEEP_TIME_WHILE_SEARCHING;
import static org.v2.finance.expenses.constants.Constants.MAX_SLEEP_TIME_WHILE_SEARCHING_MORE_THAN_THRESHOLD_RECS;
import static org.v2.finance.expenses.constants.Constants.SEARCH_SLEEP_TWEAK_RECORDS_THRESHOLD;
import static org.v2.finance.expenses.util.Helper.fireUpBannerAd;
import static org.v2.finance.expenses.util.Helper.mapOfDisplayDateAndFileTitleDate;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 23-12-2018
 * @since 23-12-2018
 * <p>
 * This class deals with searching for text in the entire billing lists
 */
public class Discoverer extends AppCompatActivity implements View.OnClickListener {

    private final Logger logger = LoggerFactory.getLogger(Discoverer.class);

    protected final Activity context = this;

    private final int totalPlacesToLookIn = Helper.mapOfDateAndDataOfDailyEntries.size();
    private final String displaySearchStat_Searching = "Searching : %d / %d";
    private final String displaySearchStat_Found = "Found : %d occurrences";
    private final String displaySearchStat_Found_ONE = "Found : 1 occurrence";
    private final String displaySearchStat_NoRecords = "No records present to search in!";

    private EditText textSearch;
    private Button search;
    private TextView displaySearchStatus;
    private ListView listSearchResult;
    private final List<EntryOfADay> listingOfDemandedEOAD = new ArrayList<>();
    private TotalBillingListAdapter adapter = null;

    private ActualDiscoverer discoverer = null;
    private boolean areRecordsMoreThanThresholdForNormalSleep = false;
    private final ActivityStarter activityStarter = new ActivityStarter(context);
    private LocalSavedPrefs localSavedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        //deactivateHeavyImportingFlag(); //currently, search doesn't allow modification. If it starts allowing, then this needs to switched to activateHeavyImportingFlag();
        //no need of setting the flag for heavy importing here... is handled in the ViewDayRecord class

        Toolbar toolbar = findViewById(R.id.toolbar_search);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }

        localSavedPrefs = (LocalSavedPrefs) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
        boolean runPopulate = false;
        if (localSavedPrefs == null) { // if MainActivity not launched from BAU flow
            localSavedPrefs = new LocalSavedPrefs();
            runPopulate = true;
        }
        ImportToLocalPrefs importToLocalPrefs = new ImportToLocalPrefs(context, localSavedPrefs);
        if (runPopulate) { // if MainActivity not launched from BAU flow
            importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();
        }

        fireUpBannerAd(context, R.id.banner_ad_query);

        textSearch = findViewById(R.id.text_search);
        search = findViewById(R.id.button_search);
        displaySearchStatus = findViewById(R.id.display_search_status);
        listSearchResult = findViewById(R.id.list_view_search_result);

        search.setOnClickListener(this);

        if (totalPlacesToLookIn == 0) {
            disableSearching();
            displaySearchStatus.setText(displaySearchStat_NoRecords);
        } else {
            areRecordsMoreThanThresholdForNormalSleep = totalPlacesToLookIn > SEARCH_SLEEP_TWEAK_RECORDS_THRESHOLD;
        }
    }

    private void disableSearching() {
        search.setEnabled(false);
        textSearch.setEnabled(false);
        disableListView();
    }

    private void enableSearching() {
        search.setEnabled(true);
        textSearch.setEnabled(true);
        enableListView();
    }

    private void disableListView() {
        listSearchResult.setClickable(false);
        listSearchResult.setEnabled(false);
    }

    private void enableListView() {
        listSearchResult.setClickable(true);
        listSearchResult.setEnabled(true);
    }

    private void clearStage() {
        displaySearchStatus.setText(Constants.EMPTY_STR);
        listingOfDemandedEOAD.clear();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            if (BuildConfig.DEBUG) logger.info("Completed clearing up the stage for discover!");
        }
    }

    @Override
    public void onClick(View view) {
        if (view == search) {
            TransportEditText transportEditText = Helper.validateText(textSearch.getText());
            if (transportEditText.isValid()) {
                clearStage();
                discoverer = new ActualDiscoverer(this, transportEditText.getData().toLowerCase());
                discoverer.execute();
            } else {
                logger.warn("The search text is empty!");
                ToasterUtil.shortToaster(this, "The search text is empty!");
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (discoverer != null) {
            discoverer.cancel(true);
            logger.warn("Interrupting the search as the back button was pressed!");
        }
        super.onBackPressed();
    }

    @SuppressLint("StaticFieldLeak")
    public class ActualDiscoverer extends AsyncTask<Void, Integer, Void> implements AdapterView.OnItemClickListener {

        private final Activity activity;
        private final String query;
        private final Map<Integer, EntryOfADay> searchMappingForLV = new HashMap<>();

        ActualDiscoverer(Activity activity, String query) {
            this.activity = activity;
            this.query = query;
            logger.info("Query : {}", query);

            disableSearching();
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected Void doInBackground(Void... voids) {
            int searched = 0;
            //long sleepTime = (long) Math.ceil(Constants.MAX_SLEEP_TIME_WHILE_SEARCHING / (Helper.mapOfDateAndDailyEntries.size() * 1.0));

            for (String date : Helper.mapOfDateAndDataOfDailyEntries.keySet()) {
                if (isCancelled()) break;
                String localSearch = Helper.mapOfDateAndDataOfDailyEntries.get(date);
                if (localSearch == null) {
                    if (BuildConfig.DEBUG) logger.error("Should have never ever happened... How can string against a date be empty.");
                    return null;
                }
                if (localSearch.contains(query)) {
                    if (BuildConfig.DEBUG) logger.info("Found queried at date : {}", date);
                    listingOfDemandedEOAD.add(Helper.mapOfDateAndDailyEntries.get(date));
                }
                searched++;
                publishProgress(searched);
                int rand = areRecordsMoreThanThresholdForNormalSleep
                        ? MAX_SLEEP_TIME_WHILE_SEARCHING_MORE_THAN_THRESHOLD_RECS
                        : Helper.getRandomValue(MAX_SLEEP_TIME_WHILE_SEARCHING);
                try {
                    Thread.sleep(rand);
                } catch (InterruptedException e) {
                    if (BuildConfig.DEBUG) logger.warn("Search Sleep interrupted. Error : ", e);
                }
            }
            return null;
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected void onProgressUpdate(Integer... progress) {
            displaySearchStatus.setText(String.format(displaySearchStat_Searching, progress[0], totalPlacesToLookIn));
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected void onPostExecute(Void aVoid) {
            Collections.sort(listingOfDemandedEOAD, (t0, t1) -> mapOfDisplayDateAndFileTitleDate.get(t1.getDisplayDate()).compareTo(mapOfDisplayDateAndFileTitleDate.get(t0.getDisplayDate())));
            if (BuildConfig.DEBUG) logger.info(listingOfDemandedEOAD.toString());

            int hits = listingOfDemandedEOAD.size();
            if (hits != 1) displaySearchStatus.setText(String.format(displaySearchStat_Found, hits));
            else displaySearchStatus.setText(displaySearchStat_Found_ONE);

            int i = 0;
            for (EntryOfADay entryOfADay : listingOfDemandedEOAD) {
                searchMappingForLV.put(i++, entryOfADay);
            }

            adapter = new TotalBillingListAdapter(activity, listingOfDemandedEOAD);
            listSearchResult.setAdapter(adapter);
            listSearchResult.setOnItemClickListener(this);
            adapter.notifyDataSetChanged();
            //will have to handle the exception which occurs here i.e. while the app is searching through the contents for the enquiry, if the user presses back, it will cause a shut down of the app after the calc is complete, as the view to render it on is gone

            enableSearching();
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if (BuildConfig.DEBUG) logger.info("Shouting from the index click of query position : {}", position);
            try {
                activityStarter.viewExistingDayRecord(position, searchMappingForLV.get(position), true, localSavedPrefs); //switching the day view editable to TRUE as of DIB-58
            } catch (Exception e) {
                logger.error("Error encountered while launching search entry for position '{}'. Clearing up search now. Err: ", position, e);
                clearStage();
                enableSearching();
            }
        }
    }
}
