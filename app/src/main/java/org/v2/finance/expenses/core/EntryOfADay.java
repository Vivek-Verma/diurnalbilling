package org.v2.finance.expenses.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Locale;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_AMOUNT;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_INPUT_TITLE_FROM_FAULTY_FILE;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_TITLE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_ENTRY_OF_A_DAY;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;
import static org.v2.finance.expenses.constants.Constants.NULL_STR_STR;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * <p>
 * Collection of the Entries and has the overall unit of a day, as exhibited by the class variables
 * @since 20-09-2018
 */
public class EntryOfADay implements Serializable {

    private final Logger logger = LoggerFactory.getLogger(EntryOfADay.class);

    private String displayDate = Helper.getCurrentDate();
    private String title = DEFAULT_TITLE;
    private String displayAmountSign = ENTRY_TYPE_POSITIVE;
    private final String displayCurrency = CurrencyUtil.currencySignToDisplay; //to be controlled only by central helper
    private double displayAmount = DEFAULT_AMOUNT;
    private LinkedList<Entry> dailyEntries = new LinkedList<>();

    public EntryOfADay() {
    }

    public EntryOfADay(String displayDate, String title, LinkedList<Entry> dailyEntries) {
        this.displayDate = displayDate;
        this.title = title;
        this.dailyEntries = dailyEntries;

        this.calculateDayExpense(); //sets the display amount and the sign
    }

    public EntryOfADay(EntryOfADay remoteEntryOfADay) {
        this(remoteEntryOfADay.getDisplayDate(), remoteEntryOfADay.getTitle(), new LinkedList<>(remoteEntryOfADay.getDailyEntries()));
        //this had to create new linked list of daily entries as it was not resolving the bug DIB-57 otherwise
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title.isEmpty() || NULL_STR_STR.equals(title) || DEFAULT_INPUT_TITLE_FROM_FAULTY_FILE.equals(title))
            title = DEFAULT_TITLE;
        title = enrichTitle(title);
        if (BuildConfig.DEBUG) logger.info("The title being saved : {}", title);
        this.title = title;
    }

    private String enrichTitle(String title) {
        title = title.replaceAll("\\s+", "-");
        return title;
    }

    public String getDisplayAmountSign() {
        return displayAmountSign;
    }

    public void setDisplayAmountSign(String displayAmountSign) {
        this.displayAmountSign = displayAmountSign;
    }

    public String getDisplayCurrency() {
        return displayCurrency;
    }

    public double getDisplayAmount() {
        return displayAmount;
    }

    public void setDisplayAmount(double displayAmount) {
        this.displayAmount = displayAmount;
    }

    public LinkedList<Entry> getDailyEntries() {
        return dailyEntries;
    }

    public void setDailyEntries(LinkedList<Entry> dailyEntries) {
        this.dailyEntries = dailyEntries;
    }

    /**
     * This function does the heavy lifting of calculating the sum of all expenses of the day, and sets the sign and sum
     * <p>
     * This f() is called when :
     * - there is a constructor call to this class,
     * - from the ViewDayRecord's saveDayRecord method
     */
    public void calculateDayExpense() {
        double localEntryAmount, localDailyAmount = 0;
        String displayAmtSign = ENTRY_TYPE_POSITIVE;
        for (Entry entry : this.getDailyEntries()) { //calculating the daily amount before saving the file
            localEntryAmount = entry.getAmount();
            if (ENTRY_TYPE_NEGATIVE.equals(entry.getEntryType())) localEntryAmount *= -1;
            localDailyAmount += localEntryAmount;
        }

        if (localDailyAmount < 0) {
            displayAmtSign = ENTRY_TYPE_NEGATIVE;
            localDailyAmount *= -1;
        }
        this.setDisplayAmountSign(displayAmtSign);
        this.setDisplayAmount(localDailyAmount);
    }

    /**
     * @return String value of the entire Data for this day. (EOAD str + str of all the entries)
     */
    public String getEntireDataForSaving() {
        StringBuilder data = new StringBuilder();
        data.append(this.toString()).append(NEW_LINE);
        for (Entry saveEntry : this.getDailyEntries()) {
            data.append(saveEntry.toString()).append(NEW_LINE);
            if (BuildConfig.DEBUG) logger.info("saveentry being written : {}", saveEntry.toString());
        }
        return data.toString();
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), FORMAT_ENTRY_OF_A_DAY, displayDate, title, displayAmountSign, displayCurrency, displayAmount);
    }
}
