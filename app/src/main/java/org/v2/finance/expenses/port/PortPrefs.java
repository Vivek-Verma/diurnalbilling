package org.v2.finance.expenses.port;

import android.content.Context;
import android.content.SharedPreferences;

import org.v2.finance.expenses.core.LocalSavedPrefs;

import java.io.Serializable;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 03-06-2019
 * @since 03-06-2019
 * <p>
 * Houses the context, actual internal shared preference, and obj of class @class LocalSavedPrefs
 */
public abstract class PortPrefs implements Serializable {

    protected final Context context;
    protected final SharedPreferences actualInternalCache; //the actual internal shared preference
    protected final LocalSavedPrefs localSavedPrefs;

    public PortPrefs(Context context, SharedPreferences actualInternalCache, LocalSavedPrefs localSavedPrefs) {
        this.context = context;
        this.actualInternalCache = actualInternalCache;
        this.localSavedPrefs = localSavedPrefs;
    }

}
