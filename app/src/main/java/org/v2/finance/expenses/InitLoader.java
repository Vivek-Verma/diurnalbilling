package org.v2.finance.expenses;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.permissions.PermissionBox;
import org.v2.finance.expenses.permissions.PermissionConstants;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.export.ping.AsyncPingAndForget;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.ui.signinup.SignIn;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.v2.finance.expenses.constants.Constants.DEFAULT_CURRENCY;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_USER_MOBILE;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_PING;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_PING_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.USER_SIGN_IN_EXPIRED;
import static org.v2.finance.expenses.permissions.PermissionConstants.CODE_PERMISSION_READ_EXTERNAL_STORAGE;
import static org.v2.finance.expenses.permissions.PermissionConstants.CODE_PERMISSION_WRITE_EXTERNAL_STORAGE;
import static org.v2.finance.expenses.permissions.PermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE;
import static org.v2.finance.expenses.permissions.PermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE;

/**
 * Serves as the stepping stone for the application.
 * Does the following:
 * i. Check for the write to external storage permission
 * ii. Load the internal maps for MainActivity
 *
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 20-07-2019
 * @since 15-07-2019
 */
public class InitLoader extends AppCompatActivity {

    private final Logger LOGGER = LoggerFactory.getLogger(InitLoader.class);

    private final Context baseContext = this;
    private final InitLoader INIT_LOADER = this;
    private final AtomicInteger superController = new AtomicInteger(1);

    protected ProgressBar progressBar;
    protected TextView progressStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_loader);

        progressBar = findViewById(R.id.progressBarInitLoader);
        progressStatus = findViewById(R.id.progressStatusInitLoader);

        getThePermissionsPumping();
    }

    private void initiateSignIn() {
        PropertyReader properties = new PropertyReader(baseContext, getString(R.string.diurnal_properties)); //populate properties

        LocalSavedPrefs localSavedPrefs = new LocalSavedPrefs();
        ImportToLocalPrefs importToLocalPrefs = new ImportToLocalPrefs(baseContext, localSavedPrefs);
        ExportToInternalPrefs exportToInternalCache = new ExportToInternalPrefs(baseContext, localSavedPrefs);
        importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();

        if (TimingUtil.hasTimestampExpired(localSavedPrefs.getUserCredHashExpiryTimestamp())) {
            localSavedPrefs.setUserCredHashExpiryTimestamp(USER_SIGN_IN_EXPIRED);
            localSavedPrefs.setUserCredHash(EMPTY_STR);
            localSavedPrefs.setUserEmail(EMPTY_STR);
            localSavedPrefs.setUserMobile(DEFAULT_USER_MOBILE);
            localSavedPrefs.setCurrency(DEFAULT_CURRENCY);
            exportToInternalCache.writeUserCredHashExpiryTime();
            exportToInternalCache.writeUserCredHash();
            exportToInternalCache.writeUserEmail();
            exportToInternalCache.writeUserMobile();
            exportToInternalCache.writeCurrency();

            AsyncPingAndForget asyncPingAndForget = new AsyncPingAndForget(
                    properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_PING),
                    properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_PING_TIMEOUT, 35L),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                    properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
            );
            asyncPingAndForget.execute(); //fire and forget
        }

        new ActivityStarter(baseContext).launchGenericScreen(localSavedPrefs, properties, SignIn.class);
    }

    public void getThePermissionsPumping() {
        final int permissionReadExternalStorage = ContextCompat.checkSelfPermission(baseContext, PERMISSION_READ_EXTERNAL_STORAGE);
        final int permissionWriteExternalStorage = ContextCompat.checkSelfPermission(baseContext, PERMISSION_WRITE_EXTERNAL_STORAGE);
        final List<PermissionBox> listPermissionsNeeded = new ArrayList<>();
        /*
        //write permission implicitly grants read permission
        if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(new PermissionBox(PERMISSION_READ_EXTERNAL_STORAGE));
        }
        */
        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(new PermissionBox(PERMISSION_WRITE_EXTERNAL_STORAGE));
        }
        if (listPermissionsNeeded.isEmpty()) {
            if (BuildConfig.DEBUG) LOGGER.info("All permissions are a go!");
            initiateSignIn();
        } else {
            permissionChecker(INIT_LOADER, listPermissionsNeeded);
        }
    }

    private void permissionChecker(final Activity activity, List<PermissionBox> permissionList) {
        for (final PermissionBox permissionBox : permissionList) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionBox.getPermission())) {
                if (BuildConfig.DEBUG) LOGGER.warn("Permission is needed for the app to function!");

                new AlertDialog.Builder(activity)
                        .setTitle("Permission required")
                        .setMessage(PermissionConstants.PERMISSION_AND_THE_MESSAGE_TO_BE_DISPLAYED_MAP.get(permissionBox.getPermission()))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                permissionRequestorCaller(activity, permissionBox);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ToasterUtil.shortToaster(baseContext, "Exiting app because the permission is absolutely required!");
                                dialog.dismiss();
                                shutDownInitiator();
                            }
                        })
                        .create()
                        .show();
            } else {
                permissionRequestorCaller(activity, permissionBox); //normal way of asking for permission - first time
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CODE_PERMISSION_READ_EXTERNAL_STORAGE:
            case CODE_PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (BuildConfig.DEBUG) LOGGER.info(PERMISSION_WRITE_EXTERNAL_STORAGE + " has been granted already!");
                    ToasterUtil.shortToaster(baseContext, "Permission granted :)");
                    initiateSignIn();

                } else {
                    if (BuildConfig.DEBUG) LOGGER.warn(PERMISSION_WRITE_EXTERNAL_STORAGE + " has been been denied!");
                    ToasterUtil.shortToaster(baseContext, "Permission denied :(");
                    if (superController.get() >= PermissionConstants.DND_TOLERANCE_COUNT) //to handle the case in which the user does a "Don't ask again" in a loop
                        shutDownInitiator();
                    superController.incrementAndGet();

                    final List<PermissionBox> permissionsToBeObtained = new ArrayList<>();
                    for (String permission : permissions)
                        permissionsToBeObtained.add(new PermissionBox(permission));
                    permissionChecker(INIT_LOADER, permissionsToBeObtained);
                }
                break;
        }
    }

    private void permissionRequestorCaller(Activity activity, PermissionBox permissionBox) {
        ActivityCompat.requestPermissions(activity, new String[]{permissionBox.getPermission()}, permissionBox.getReturnCode());
    }

    private void shutDownInitiator() {
        if (BuildConfig.DEBUG) LOGGER.warn("Shutting down the app by calling finish and then system.exit(0)");
        ToasterUtil.longToaster(baseContext, "Shutting down due to lack of permission!");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            if (BuildConfig.DEBUG) LOGGER.error("'Sleeping before exiting' was interrupted!");
        }
        finish();
        System.exit(0);
    }

}
