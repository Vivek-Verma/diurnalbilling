package org.v2.finance.expenses.port.export;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.PortPrefs;

import static android.content.Context.MODE_PRIVATE;
import static org.v2.finance.expenses.constants.Constants.KEY_FLOAT_CURRENT_MONTH_EXPENSE;
import static org.v2.finance.expenses.constants.Constants.KEY_INT_USER_ISD;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_CLOUD_UPLOAD_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_LAUNCHED_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_LAST_SAVED_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_PAYMENT_EXPIRY_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_USER_CRED_HASH_EXPIRY_TIMESTAMP;
import static org.v2.finance.expenses.constants.Constants.KEY_LONG_USER_MOBILE;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_CURRENCY;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_PREMIUM_USER;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_CRED_HASH;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_EMAIL;
import static org.v2.finance.expenses.constants.Constants.KEY_STR_USER_NAME;
import static org.v2.finance.expenses.constants.Constants.MY_PREFS_NAME;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-03-2021
 * @since 03-06-2019
 * Export data from local shared preference to the device's internal shared preference, to be used later on
 */
public class ExportToInternalPrefs extends PortPrefs {

    private SharedPreferences.Editor editor;

    public ExportToInternalPrefs(Context context, LocalSavedPrefs localSavedPrefs) {
        super(context, context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE), localSavedPrefs);
    }

    public void writeLastUsedTime() {
        activateEditor();
        editor.putLong(KEY_LONG_LAST_LAUNCHED_TIMESTAMP, localSavedPrefs.getLastUsedTimeStamp());
        closeEditor();
    }

    public void writeCurrentMonthExpense() {
        activateEditor();
        editor.putFloat(KEY_FLOAT_CURRENT_MONTH_EXPENSE, localSavedPrefs.getCurrentMonthExpense());
        closeEditor();
    }

    public void writeCurrency() {
        activateEditor();
        editor.putString(KEY_STR_CURRENCY, localSavedPrefs.getCurrency().name());
        closeEditor();
    }

    public void writeUserName() {
        activateEditor();
        editor.putString(KEY_STR_USER_NAME, localSavedPrefs.getUserName());
        closeEditor();
    }

    public void writePremiumUserStatus() {
        activateEditor();
        editor.putBoolean(KEY_STR_PREMIUM_USER, localSavedPrefs.isPremiumUser());
        closeEditor();
    }

    public void writeUserEmail() {
        activateEditor();
        editor.putString(KEY_STR_USER_EMAIL, localSavedPrefs.getUserEmail());
        closeEditor();
    }

    public void writeUserIsdCode() {
        activateEditor();
        editor.putInt(KEY_INT_USER_ISD, localSavedPrefs.getUserIsdCode());
        closeEditor();
    }

    public void writeUserMobile() {
        activateEditor();
        editor.putLong(KEY_LONG_USER_MOBILE, localSavedPrefs.getUserMobile());
        closeEditor();
    }

    public void writeUserCredHash() {
        activateEditor();
        editor.putString(KEY_STR_USER_CRED_HASH, localSavedPrefs.getUserCredHash());
        closeEditor();
    }

    public void writeUserCredHashExpiryTime() {
        activateEditor();
        editor.putLong(KEY_LONG_USER_CRED_HASH_EXPIRY_TIMESTAMP, localSavedPrefs.getUserCredHashExpiryTimestamp());
        closeEditor();
    }

    public void writeLastSavedTime() {
        activateEditor();
        editor.putLong(KEY_LONG_LAST_SAVED_TIMESTAMP, localSavedPrefs.getLastSavedTimeStamp());
        closeEditor();
    }

    public void writeLastCloudUploadTime() {
        activateEditor();
        editor.putLong(KEY_LONG_LAST_CLOUD_UPLOAD_TIMESTAMP, localSavedPrefs.getLastCloudUploadTimeStamp());
        closeEditor();
    }

    public void writePaymentExpiryTime() {
        activateEditor();
        editor.putLong(KEY_LONG_PAYMENT_EXPIRY_TIMESTAMP, localSavedPrefs.getPaymentExpiryTimestamp());
        closeEditor();
    }


    @SuppressLint("CommitPrefEdits")
    private void activateEditor() {
        editor = actualInternalCache.edit();
    }

    private void closeEditor() {
        editor.apply();
    }

}
