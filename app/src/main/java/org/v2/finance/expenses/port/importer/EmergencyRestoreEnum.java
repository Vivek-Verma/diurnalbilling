package org.v2.finance.expenses.port.importer;

/**
 * @author Vivek Verma
 * @since 24/4/21
 */
public enum EmergencyRestoreEnum {
    LOCAL(0), CLOUD(1);

    private final int restoreSequence;

    EmergencyRestoreEnum(int restoreSequence) {
        this.restoreSequence = restoreSequence;
    }

    public int getRestoreSequence() {
        return restoreSequence;
    }
}
