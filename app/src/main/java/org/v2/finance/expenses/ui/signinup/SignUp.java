package org.v2.finance.expenses.ui.signinup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vv.personal.diurnal.artifactory.generated.DataTransitProto;
import com.vv.personal.diurnal.artifactory.generated.ResponsePrimitiveProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.auth.AuthUtil;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.ui.signinup.otp.SignUpOtp;
import org.v2.finance.expenses.util.SignInUpUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_PROPERTY_READER;
import static org.v2.finance.expenses.util.CurrencyUtil.CURRENCY_SIGN;

/**
 * @author Vivek Verma
 * @since 14/3/21
 */
public class SignUp extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignUp.class);

    private final Context context = this;

    private EditText inputSignUpUsername;
    private EditText inputSignUpEmailAddress;
    private EditText inputSignUpISDCode;
    private EditText inputSignUpMobile;
    private EditText inputSignUpNewPassword;
    private EditText inputSignUpNewPasswordReEnter;
    private TextView signUpDisplayCurrencyText;
    private TextView signUpDisplayCurrencySign;
    private Spinner signUpDisplayCurrency;
    private Button buttonSignUpProceed;

    private ActivityStarter activityStarter;
    private PropertyReader properties;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        activityStarter = new ActivityStarter(context);
        properties = (PropertyReader) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_PROPERTY_READER);
        if (properties == null) {
            properties = new PropertyReader(context, getString(R.string.diurnal_properties));
        }

        inputSignUpUsername = findViewById(R.id.inputSignUpUsername);
        inputSignUpEmailAddress = findViewById(R.id.inputSignUpEmailAddress);
        inputSignUpISDCode = findViewById(R.id.inputSignUpISDCode);
        inputSignUpMobile = findViewById(R.id.inputSignUpMobile);
        inputSignUpNewPassword = findViewById(R.id.inputSignUpNewPassword);
        inputSignUpNewPasswordReEnter = findViewById(R.id.inputSignUpNewPasswordReEnter);
        signUpDisplayCurrencyText = findViewById(R.id.signUpDisplayCurrencyText);
        signUpDisplayCurrencySign = findViewById(R.id.signUpDisplayCurrencySign);
        signUpDisplayCurrency = findViewById(R.id.signUpDisplayCurrency);
        buttonSignUpProceed = findViewById(R.id.buttonSignUpProceed);

        buttonSignUpProceed.setOnClickListener(this);

        ArrayAdapter<UserMappingProto.Currency> currencyArrayAdapter = SignInUpUtil.generateAndPruneCurrencyListArrayAdapter(context);
        signUpDisplayCurrency.setAdapter(currencyArrayAdapter);
        signUpDisplayCurrency.setOnItemSelectedListener(this);
        signUpDisplayCurrency.setSelection(0, true);
    }


    @Override
    public void onClick(View view) {
        if (view == buttonSignUpProceed) {
            if (!SignInUpUtil.verifyUserName(inputSignUpUsername.getText())) {
                ToasterUtil.longToaster(context, "Enter min 3 characters for name");
                return;
            }
            if (!SignInUpUtil.verifyUserEmail(inputSignUpEmailAddress.getText())) {
                ToasterUtil.longToaster(context, "Invalid email address entered!");
                return;
            }
            if (!SignInUpUtil.verifyUserCred(inputSignUpNewPassword.getText())) {
                ToasterUtil.longToaster(context, "Invalid password entered!");
                return;
            }
            if (!inputSignUpNewPasswordReEnter.getText().toString().equals(inputSignUpNewPassword.getText().toString())) {
                if (BuildConfig.DEBUG) LOGGER.info("pwd: '{}', re-pwd: '{}'", inputSignUpNewPassword.getText(), inputSignUpNewPasswordReEnter.getText());
                ToasterUtil.longToaster(context, "Passwords mismatch!");
                return;
            }
            if (!SignInUpUtil.verifyIsdCode(inputSignUpISDCode.getText())) {
                ToasterUtil.longToaster(context, "Invalid ISD!");
                return;
            }
            if (!SignInUpUtil.verifyMobile(inputSignUpMobile.getText())) {
                ToasterUtil.longToaster(context, "Invalid Mobile!");
                return;
            }

            String userName = inputSignUpUsername.getText().toString();
            String userEmail = inputSignUpEmailAddress.getText().toString();
            String userCred = inputSignUpNewPassword.getText().toString();
            UserMappingProto.Currency currency = UserMappingProto.Currency.valueOf(signUpDisplayCurrencyText.getText().toString().trim());
            int isdCode = inputSignUpISDCode.getText().toString().isEmpty() ? 0 : Integer.parseInt(inputSignUpISDCode.getText().toString());
            long mobile = Long.parseLong(isdCode + (inputSignUpMobile.getText().toString().isEmpty() ? "0" : inputSignUpMobile.getText().toString())); //need to combine isd code here to store in db.
            if (BuildConfig.DEBUG) LOGGER.info("Read-in user-email [{}] x [{}] x [{}] x [{}] x [{}] x [{}]", userName, userEmail, userCred, isdCode, mobile, currency);

            lockAllFields();
            AsyncSignUpPayload asyncSignUpPayload = new AsyncSignUpPayload(userName, userEmail, mobile, userCred, currency);
            asyncSignUpPayload.execute(); //driving point passed onto Async
        }
    }

    private void opOnFields(boolean targetState) {
        inputSignUpUsername.setEnabled(targetState);
        inputSignUpEmailAddress.setEnabled(targetState);
        inputSignUpISDCode.setEnabled(targetState);
        inputSignUpMobile.setEnabled(targetState);
        inputSignUpNewPassword.setEnabled(targetState);
        inputSignUpNewPasswordReEnter.setEnabled(targetState);
        signUpDisplayCurrency.setEnabled(targetState);
        buttonSignUpProceed.setEnabled(targetState);
    }

    void lockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Locking all fields");
        opOnFields(false);
    }

    void unlockAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Unlocking all fields");
        opOnFields(true);
    }

    void clearAllFields() {
        if (BuildConfig.DEBUG) LOGGER.info("Clearing all fields");
        inputSignUpUsername.setText(EMPTY_STR);
        inputSignUpEmailAddress.setText(EMPTY_STR);
        inputSignUpISDCode.setText(EMPTY_STR);
        inputSignUpMobile.setText(EMPTY_STR);
        inputSignUpNewPassword.setText(EMPTY_STR);
        inputSignUpNewPasswordReEnter.setText(EMPTY_STR);
        signUpDisplayCurrency.setSelection(0, true);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        UserMappingProto.Currency selectedCurrency = (UserMappingProto.Currency) parent.getItemAtPosition(position);
        if (selectedCurrency != null && selectedCurrency != UserMappingProto.Currency.UNRECOGNIZED) {
            signUpDisplayCurrencyText.setText(selectedCurrency.name());
            signUpDisplayCurrencySign.setText(CURRENCY_SIGN.get(selectedCurrency));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class AsyncSignUpPayload extends AsyncTask<Void, String, UserMappingProto.UserMapping> {

        private final String userName;
        private final String userEmail;
        private final Long userMobile;
        private final String userCred;
        private final UserMappingProto.Currency currency;

        public AsyncSignUpPayload(String userName, String userEmail, Long userMobile, String userCred, UserMappingProto.Currency currency) {
            this.userName = userName;
            this.userEmail = userEmail;
            this.userMobile = userMobile;
            this.userCred = userCred;
            this.currency = currency;

            ToasterUtil.shortToaster(context, "Payload generation started");
        }

        @Override
        protected UserMappingProto.UserMapping doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            final UserMappingProto.UserMapping signUpUserData = UserMappingProto.UserMapping.newBuilder()
                    .setEmail(userEmail)
                    .setUsername(userName)
                    .setMobile(userMobile)
                    .setCurrency(currency)
                    .setHashCred(AuthUtil.encode(userCred)) //essential to perform this here, as cpu intensive, so needs to be done in BG
                    .build();
            timer.stop();
            if (BuildConfig.DEBUG) LOGGER.info("Took {} ms to complete sign-up tx obj gen", timer.getTime());
            return signUpUserData;
        }

        @Override
        protected void onPostExecute(UserMappingProto.UserMapping userSignUpDetails) {
            ToasterUtil.shortToaster(context, "Payload generated");
            AsyncSignUpCheckEmail asyncSignUpCheckEmail = new AsyncSignUpCheckEmail(context, userSignUpDetails.getEmail(), userSignUpDetails,
                    properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL),
                    properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNUP_CHECK_EMAIL_TIMEOUT, 35L),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                    properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                    properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
            );
            asyncSignUpCheckEmail.execute();
        }
    }

    private class AsyncSignUpCheckEmail extends AsyncSignInUpModule {
        private final String userEmail;
        private final UserMappingProto.UserMapping userSignUpDetails;
        private final PingServerWithData<DataTransitProto.DataTransit> pingServer;
        private boolean isEmailAlreadyTaken = false;

        public AsyncSignUpCheckEmail(Context context, String userEmail, UserMappingProto.UserMapping userSignUpDetails,
                                     String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
            this.userEmail = userEmail;
            this.userSignUpDetails = userSignUpDetails;

            if (BuildConfig.DEBUG) LOGGER.info("AsyncSignUpCheckEmail isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            final DataTransitProto.DataTransit dataTransit = DataTransitProto.DataTransit.newBuilder().setEmail(userEmail).build();
            timer.stop();
            if (BuildConfig.DEBUG) LOGGER.info("Took {} ms to complete data-tx obj gen", timer.getTime());

            try {
                timer.reset();
                timer.start();
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, dataTransit));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                isEmailAlreadyTaken = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream()).getBoolResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), isEmailAlreadyTaken);
                packageResponseDetail.close();
                return isEmailAlreadyTaken;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact sign-up-check-email module for DBI. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while contacting sign-up-check-email DBI. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean isEmailAlreadyTaken) {
            if (isEmailAlreadyTaken == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Contact developer.");
            } else if (isEmailAlreadyTaken) {
                ToasterUtil.longToaster(context, "Entered email already in use! Use a different one!");
                clearAllFields();
                unlockAllFields();
            } else {
                activityStarter.launchGenericScreen(userSignUpDetails, properties, SignUpOtp.class);
            }
        }
    }
}
