package org.v2.finance.expenses.util;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 12-06-2019
 * @since 12-06-2019
 */
public class ModelForDeletion {

    private final String name;
    private final boolean checkboxActive; // 0 - checkbox disabled, 1 - checkbox active...

    public ModelForDeletion(String name) {
        this.name = name;
        this.checkboxActive = false;
    }

    public ModelForDeletion(String name, boolean checkboxActive) {
        this.name = name;
        this.checkboxActive = checkboxActive;
    }

    public String getName() {
        return this.name;
    }

    public boolean isCheckboxActive() {
        return checkboxActive;
    }
}
