package org.v2.finance.expenses.ads;

import android.app.Activity;

/**
 * @author Vivek Verma
 * @since 08-09-2019
 */
public abstract class Ad {

    final Activity context;

    public Ad(Activity context) {
        this.context = context;
    }

    public abstract void assignAd();
}
