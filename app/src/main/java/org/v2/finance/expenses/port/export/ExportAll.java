package org.v2.finance.expenses.port.export;

import android.content.Context;

import com.vv.personal.diurnal.artifactory.generated.DataTransitProto;
import com.vv.personal.diurnal.artifactory.generated.ResponsePrimitiveProto;
import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;
import com.vv.personal.diurnal.client.exposed.contracts.PingServerWithData;
import com.vv.personal.diurnal.client.exposed.tx.PackageResponseDetail;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.contracts.CallBackWithUserMappingContract;
import org.v2.finance.expenses.contracts.diurnal.AsyncSignInUpModule;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.importer.AsyncUserInfoPull;
import org.v2.finance.expenses.port.importer.UnitImporter;
import org.v2.finance.expenses.properties.PropertyReader;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_CRED;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_ENDPOINT_DBI_USER;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HOST_DBI_IS_HTTP;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTPS_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.DIURNAL_SERVER_HTTP_HOST_DBI;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.EXTERNAL_BACKUP_FILE_LOCATION;
import static org.v2.finance.expenses.constants.Constants.EXTERNAL_BACKUP_FOLDER_LOCATION;
import static org.v2.finance.expenses.constants.Constants.FILE_HEADER_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_INTERNAL_FILE;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;
import static org.v2.finance.expenses.util.Helper.mapOfDisplayDateAndFileTitleDate;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 23-12-2018
 * @since 13-11-2018
 * <p>
 * currently this class is reading all the files one by one from their src and then writing then concatenating all to the string builder, before writing it to external file
 */
public class ExportAll implements CallBackWithUserMappingContract {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportAll.class);

    //private final Map<Integer, EntryOfADay> mapOfDailyEntriesFromEntireBillingList;
    private final List<String> listOfDatesInDescOrder;
    private final Context context;
    private final Boolean isPremiumUser;
    private final String email;
    private final PropertyReader properties;
    private final LocalSavedPrefs localSavedPrefs;
    private final ExportToInternalPrefs exportToInternalCache;
    private String allEntries = EMPTY_STR;

    public ExportAll(Context context, List<String> setOfDatesInDescOrder, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache, PropertyReader propertyReader) {
        this.context = context;
        this.listOfDatesInDescOrder = setOfDatesInDescOrder;
        this.localSavedPrefs = localSavedPrefs;
        this.exportToInternalCache = exportToInternalCache;
        this.isPremiumUser = localSavedPrefs.isPremiumUser();
        this.email = localSavedPrefs.getUserEmail();
        this.properties = propertyReader;
    }

    public void fireHeavyExport() {
        String allEntries = fireHeavyLocalExport();
        if (allEntries.isEmpty() || !isPremiumUser) {
            LOGGER.warn("Not going for cloud backup");
            allEntries = null; //for GC
            return;
        }
        this.allEntries = allEntries;

        AsyncUserInfoPull asyncSignIn = new AsyncUserInfoPull(context, localSavedPrefs, exportToInternalCache, this, email,
                properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN),
                properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT, 90L),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
        );
        asyncSignIn.setDelayInProgressToast(11);
        asyncSignIn.execute(); //driving point passed onto Async
    }

    public String fireHeavyLocalExport() {
        //creates the Diurnal folder in the external file system
        File outputFolder = new File(EXTERNAL_BACKUP_FOLDER_LOCATION);
        if (!outputFolder.exists()) {
            LOGGER.info("Outer folder path : {} -- doesn't exist", EXTERNAL_BACKUP_FOLDER_LOCATION);
            if (outputFolder.mkdirs()) {
                LOGGER.info("Successfully created outer folder for backup!");
            } else {
                LOGGER.error("Failed to create the output folder {}. Backup cannot proceed!", EXTERNAL_BACKUP_FOLDER_LOCATION);
                ToasterUtil.shortToaster(context, "Cannot create backup file because of no file access permission!");
                return EMPTY_STR;
            }
        }

        outputFolder = new File(EXTERNAL_BACKUP_FILE_LOCATION);
        LOGGER.info("The output file -- {}", EXTERNAL_BACKUP_FILE_LOCATION);
        if (BuildConfig.DEBUG) LOGGER.info("The internal file base : " + FILE_HEADER_INTERNAL_FILE);
        boolean errorFreePassage = true, proceedToWrite = true;
        final StringBuilder inputData = new StringBuilder();

        //reads the files from internal storage and keeps on appending to the outer text file
        for (String date : listOfDatesInDescOrder) {
            final String fileTitleDate = mapOfDisplayDateAndFileTitleDate.get(date);
            final String inputFile = String.format(FORMAT_INTERNAL_FILE, FILE_HEADER_INTERNAL_FILE, fileTitleDate);
            if (BuildConfig.DEBUG) LOGGER.info("Reading in -- {}", inputFile);

            //importing and appending the string of the inputFile
            String rxData = UnitImporter.triggerStringImportFromSavedFile(inputFile);
            if (!rxData.isEmpty()) inputData.append(rxData).append(NEW_LINE);
            else {
                proceedToWrite = false;
                if (BuildConfig.DEBUG) LOGGER.error("The reading of above file returned empty string... Something went wrong. Check logs!");
                break;
            }
        }

        if (proceedToWrite) {
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(outputFolder, false))) {
                printWriter.write(inputData.toString());
            } catch (IOException e) {
                errorFreePassage = false;
                LOGGER.error("Error encountered whilst writing the backup file. Error : ", e);
                ToasterUtil.shortToaster(context, "Please grant permission to access file system to backup the data");
            }
        } else {
            errorFreePassage = false;
        }

        if (errorFreePassage) {
            ToasterUtil.shortToaster(context, "Local BackUp Complete -> " + outputFolder.getAbsolutePath());
            localSavedPrefs.setLastSavedTimeStamp(TimingUtil.extractCurrentUtcTimestamp());
            exportToInternalCache.writeLastSavedTime();
            return inputData.toString();
        } else {
            ToasterUtil.longToaster(context, "BackUp Failed!!");
        }
        return EMPTY_STR;
    }

    @Override
    public void callBack(UserMappingProto.UserMapping retrievedUserMapping) {
        if (TimingUtil.hasTimestampExpired(retrievedUserMapping.getPaymentExpiryTimestamp())) {
            Helper.implementPostPaymentExpirySteps(context);
            return;
        }

        AsyncPushBackupToCloud asyncPushBackupToCloud = new AsyncPushBackupToCloud(context, email, allEntries,
                properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE),
                properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_PUSH_BACKUP_ENTIRE_TIMEOUT, 90L),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
        );
        asyncPushBackupToCloud.setDisplayProgressMessage(EMPTY_STR);
        asyncPushBackupToCloud.setLocalSavedPrefs(localSavedPrefs);
        asyncPushBackupToCloud.setExportToInternalCache(exportToInternalCache);
        asyncPushBackupToCloud.setProperties(properties);
        asyncPushBackupToCloud.execute(); //driving point passed onto Async
    }

    private static class AsyncPushBackupToCloud extends AsyncSignInUpModule {
        private final DataTransitProto.DataTransit dataTransit;
        private final PingServerWithData<DataTransitProto.DataTransit> pingServer;
        private final String email;
        private Boolean cloudBackupResult = null;
        private int rowsSavedOnCloud = 0;
        private LocalSavedPrefs localSavedPrefs = null;
        private PropertyReader properties = null;
        private ExportToInternalPrefs exportToInternalCache = null;

        public AsyncPushBackupToCloud(Context context, String email, String allEntries,
                                      String httpServerHost, String httpsServerHost, String serverPingEndpoint, long serverRequestTimeout, String serverUser, String serverCred, boolean isHttp) {
            super(context, isHttp ? httpServerHost : httpsServerHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred);
            this.email = email;

            if (BuildConfig.DEBUG) LOGGER.info("AsyncPushBackupToCloud isHttp: {}", isHttp);
            this.pingServer = isHttp ? new com.vv.personal.diurnal.client.exposed.http.PerformPostOp<>() : new com.vv.personal.diurnal.client.exposed.https.PerformPostOp<>();
            this.dataTransit = DataTransitProto.DataTransit.newBuilder().setEmail(email).setBackupData(allEntries).build();
            ToasterUtil.shortToaster(context, "Starting cloud backup");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            StopWatch timer = new StopWatch();
            timer.start();
            try {
                if (areExecutorsDead()) jumpStartExecutors();
                Future<PackageResponseDetail> future = executorService.submit(() ->
                        pingServer.pingServer(serverHost, serverPingEndpoint, serverRequestTimeout, serverUser, serverCred, dataTransit));

                PackageResponseDetail packageResponseDetail = future.get(serverRequestTimeout + 10, TimeUnit.SECONDS);
                ResponsePrimitiveProto.ResponsePrimitive response = ResponsePrimitiveProto.ResponsePrimitive.parseFrom(packageResponseDetail.getInputStream());
                cloudBackupResult = response.getBoolResponse();
                rowsSavedOnCloud = response.getIntegralResponse();
                timer.stop();
                if (BuildConfig.DEBUG) LOGGER.info("Received response from server in [{}]s  => [{}]", timer.getTime(TimeUnit.SECONDS), cloudBackupResult);
                packageResponseDetail.close();
                return cloudBackupResult;
            } catch (InterruptedException | IOException | NullPointerException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Failed to contact cloud-backup module for DBI. ", e);
            } catch (ExecutionException | TimeoutException e) {
                if (BuildConfig.DEBUG) LOGGER.error("Connectivity error while cloud-backup DBI. Server unreachable! ", e);
            } finally {
                if (!timer.isStopped()) timer.stop();
                killAllExecutors();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean cloudBackupResult) {
            if (cloudBackupResult == null) {
                ToasterUtil.longToaster(context, "Server unreachable! Check internet connection OR contact developer.");
                return;
            }
            if (cloudBackupResult) {
                ToasterUtil.longToaster(context, String.format(Locale.getDefault(), "Cloud backup complete of %d days", rowsSavedOnCloud));

                if (localSavedPrefs != null && exportToInternalCache != null && email != null) {
                    AsyncUserInfoPull asyncSignIn = new AsyncUserInfoPull(context, localSavedPrefs, exportToInternalCache, null, email,
                            properties.getProperty(DIURNAL_SERVER_HTTP_HOST_DBI),
                            properties.getProperty(DIURNAL_SERVER_HTTPS_HOST_DBI),
                            properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN),
                            properties.getLongProperty(DIURNAL_SERVER_ENDPOINT_DBI_SIGNIN_TIMEOUT, 90L),
                            properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_USER),
                            properties.getProperty(DIURNAL_SERVER_ENDPOINT_DBI_CRED),
                            properties.getBooleanProperty(DIURNAL_SERVER_HOST_DBI_IS_HTTP)
                    );
                    asyncSignIn.setDelayInProgressToast(11);
                    asyncSignIn.execute(); //driving point passed onto Async
                }
            } else {
                ToasterUtil.longToaster(context, "Cloud backup failed!");
            }
        }

        public void setLocalSavedPrefs(LocalSavedPrefs localSavedPrefs) {
            this.localSavedPrefs = localSavedPrefs;
        }

        public void setProperties(PropertyReader properties) {
            this.properties = properties;
        }

        public void setExportToInternalCache(ExportToInternalPrefs exportToInternalCache) {
            this.exportToInternalCache = exportToInternalCache;
        }
    }
}