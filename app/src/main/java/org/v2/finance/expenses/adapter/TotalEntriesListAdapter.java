package org.v2.finance.expenses.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.core.Entry;

import java.util.LinkedList;
import java.util.Locale;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_AMOMUNT;
import static org.v2.finance.expenses.constants.Constants.GRAY;
import static org.v2.finance.expenses.constants.Constants.GREEN_DARK;
import static org.v2.finance.expenses.constants.Constants.RED;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 24-09-2018
 * @since 24-09-2018
 */
public class TotalEntriesListAdapter extends ArrayAdapter<Entry> {

    private final Logger logger = LoggerFactory.getLogger(TotalEntriesListAdapter.class);

    private final Activity context;
    private final LinkedList<Entry> entriesList;

    public TotalEntriesListAdapter(Activity context, LinkedList<Entry> entriesList) {
        super(context, R.layout.total_entries_list, entriesList);

        this.context = context;
        this.entriesList = entriesList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.total_entries_list, null, true);

        final TextView displayEntryType = rowView.findViewById(R.id.displayEntryType);
        final TextView displayEntryCurrency = rowView.findViewById(R.id.displayEntryCurrency);
        final TextView displayEntryAmount = rowView.findViewById(R.id.displayEntryAmount);
        final TextView displayEntryDescription = rowView.findViewById(R.id.displayEntryDescription);

        final Entry referredEntry = entriesList.get(position);
        displayEntryType.setText(referredEntry.getEntryType());
        displayEntryCurrency.setText(referredEntry.getCurrency());
        displayEntryAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, referredEntry.getAmount()));
        displayEntryDescription.setText(referredEntry.getDescription());

        int colorToBeAssigned = RED;
        if (ENTRY_TYPE_POSITIVE.equals(referredEntry.getEntryType())) {
            if (referredEntry.getAmount() == 0) colorToBeAssigned = GRAY;
            else colorToBeAssigned = GREEN_DARK;
        } else if (ENTRY_TYPE_COMMENT.equals(referredEntry.getEntryType())) {
            colorToBeAssigned = GRAY;
        }

        displayEntryType.setTextColor(colorToBeAssigned);
        displayEntryCurrency.setTextColor(colorToBeAssigned);
        displayEntryAmount.setTextColor(colorToBeAssigned);
        displayEntryDescription.setTextColor(colorToBeAssigned);

        if (BuildConfig.DEBUG) logger.info("Replying from the getView function, with the amount : " + referredEntry.getAmount());
        return rowView;
    }
}
