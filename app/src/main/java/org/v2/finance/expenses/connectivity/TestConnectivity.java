package org.v2.finance.expenses.connectivity;

import android.os.AsyncTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.contracts.TestConnectivityConnector;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static org.v2.finance.expenses.constants.Constants.CONNECTION_TIMEOUT_HOST_PINGING;

/**
 * @author Vivek Verma
 * @since 30-10-2019
 */
public class TestConnectivity extends AsyncTask<Void, Void, Boolean> {

    private final Logger LOGGER = LoggerFactory.getLogger(TestConnectivity.class);
    private final TestConnectivityConnector connector;
    private final String url;
    private final long sleepBeforeTestingConnectivity;

    public TestConnectivity(TestConnectivityConnector connector, String url) {
        this(connector, url, 250);
    }

    public TestConnectivity(TestConnectivityConnector connector, String url, long sleepBeforeTestingConnectivity) {
        this.connector = connector;
        this.url = url;
        this.sleepBeforeTestingConnectivity = sleepBeforeTestingConnectivity;
    }

    private boolean pingForInternetConnectivity(String urlToHit) {
        URL url;
        try {
            url = new URL(urlToHit);
        } catch (MalformedURLException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Malformed URL : {}, Error : ", urlToHit, e);
            return false;
        }

        HttpsURLConnection httpsURLConnection = null;
        try {
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setConnectTimeout(CONNECTION_TIMEOUT_HOST_PINGING);
            httpsURLConnection.setReadTimeout(CONNECTION_TIMEOUT_HOST_PINGING);
            httpsURLConnection.connect();

            if (BuildConfig.DEBUG) LOGGER.info("Response code : {}", httpsURLConnection.getResponseCode());
            return true;
        } catch (IOException e) {
            if (BuildConfig.DEBUG) LOGGER.error("Unable to establish internet connectivity. Error : ", e);
        } finally {
            if (httpsURLConnection != null) httpsURLConnection.disconnect();
        }
        return false;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            Thread.sleep(sleepBeforeTestingConnectivity);
        } catch (InterruptedException e) {
            if (BuildConfig.DEBUG) LOGGER.warn("Pre-ping sleep interrupted!");
        }
        return pingForInternetConnectivity(url);
    }

    @Override
    protected void onPostExecute(Boolean internetConnectivity) {
        if (BuildConfig.DEBUG) LOGGER.info("Is internet connected : {}, caller {}", internetConnectivity, connector);
        connector.isConnectedToInternet(internetConnectivity);
    }

}
