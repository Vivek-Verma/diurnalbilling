package org.v2.finance.expenses.core.settings.sub;

public class Payment {
    private Upi upi;

    public Payment() {
    }

    public Payment(Upi upi) {
        this.upi = upi;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Payment{");
        sb.append("upi=").append(upi);
        sb.append('}');
        return sb.toString();
    }

    public Upi getUpi() {
        return upi;
    }

    public void setUpi(Upi upi) {
        this.upi = upi;
    }
}
