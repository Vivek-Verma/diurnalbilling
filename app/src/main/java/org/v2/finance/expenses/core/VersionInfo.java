package org.v2.finance.expenses.core;

/**
 * @author Vivek Verma
 * @since 16-12-2019
 */
public class VersionInfo {

    private final String version;
    private final String info;
    private final String date;
    private final String id;

    public VersionInfo(String version, String info, String date, String id) {
        this.version = version;
        this.info = info;
        this.date = date;
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public String getInfo() {
        return info;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VersionInfo{");
        sb.append("version='").append(version).append('\'');
        sb.append(", info='").append(info).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
