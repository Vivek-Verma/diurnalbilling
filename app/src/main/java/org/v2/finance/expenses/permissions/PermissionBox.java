package org.v2.finance.expenses.permissions;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 20-07-2019
 * @since 20-07-2019
 */
public class PermissionBox {

    private final String permission;
    private final int returnCode;

    public PermissionBox(String permission) {
        this.permission = permission;
        this.returnCode = PermissionConstants.PERMISSION_AND_CORRESPONDING_CODE_MAP.get(permission);
    }

    public String getPermission() {
        return permission;
    }

    public int getReturnCode() {
        return returnCode;
    }
}
