
package org.v2.finance.expenses.util;

import android.content.Context;
import android.text.Editable;
import android.widget.ArrayAdapter;

import com.vv.personal.diurnal.artifactory.generated.UserMappingProto;

import org.v2.finance.expenses.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vivek Verma
 * @since 17/3/21
 */
public class SignInUpUtil {

    public static boolean verifyUserName(Editable userName) {
        return userName != null && verifyUserName(userName.toString());
    }

    public static boolean verifyUserName(String userName) {
        return userName != null && !userName.trim().isEmpty() && userName.length() >= 3;
    }

    public static boolean verifyUserEmail(Editable userEmail) {
        return userEmail != null && verifyUserEmail(userEmail.toString());
    }

    public static boolean verifyUserEmail(String userEmail) {
        if (userEmail == null || userEmail.trim().isEmpty()) return false;
        userEmail = userEmail.toLowerCase();
        return userEmail.matches("[a-z0-9_.]+@[a-z0-9.]+");
    }

    public static boolean verifyUserCred(Editable userCred) {
        return userCred != null && verifyUserCred(userCred.toString());
    }

    public static boolean verifyUserCred(String userCred) {
        return userCred != null && !userCred.trim().isEmpty();
    }

    public static boolean verifyMobile(Editable mobile) {
        return mobile != null && (mobile.toString().isEmpty() || mobile.toString().matches("[1-9][0-9]{9}"));
    }

    public static boolean verifyMobileWithIsd(Editable mobile) {
        if (mobile == null || mobile.toString().isEmpty()) return false;
        String mobileWithIsd = mobile.toString();
        return mobileWithIsd.length() >= 11 || mobileWithIsd.length() <= 14;
    }

    public static boolean verifyIsdCode(Editable isdCode) {
        return isdCode != null && (isdCode.toString().isEmpty() || (isdCode.toString().length() <= 4 && isdCode.toString().matches("[1-9][0-9]+")));
    }

    public static boolean verifyOtpFromEmail(Editable signUpOtpText) {
        return signUpOtpText != null && signUpOtpText.toString().matches("[0-9]{8}");
    }

    public static ArrayAdapter<UserMappingProto.Currency> generateAndPruneCurrencyListArrayAdapter(Context context) {
        List<UserMappingProto.Currency> currencyList = new ArrayList<>(Arrays.asList(UserMappingProto.Currency.values()));
        currencyList.remove(UserMappingProto.Currency.UNRECOGNIZED);
        ArrayAdapter<UserMappingProto.Currency> currencyArrayAdapter = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, currencyList
        );
        currencyArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        return currencyArrayAdapter;
    }
}
