package org.v2.finance.expenses.ui.fragment.picker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import org.v2.finance.expenses.MainActivity;
import org.v2.finance.expenses.constants.Constants;

import java.util.Calendar;
import java.util.Locale;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DATE_PICKER_DISPLAY_DATE;
import static org.v2.finance.expenses.util.ToasterUtil.longToaster;

/**
 * @author Vivek Verma
 * @since 9/5/20
 */
@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private final Activity activity;
    private final Constants.Caller callerId;

    public DatePickerFragment(Activity activity, Constants.Caller callerId) {
        this.activity = activity;
        this.callerId = callerId;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar cal = Calendar.getInstance();
        return new DatePickerDialog(getActivity(), this, cal.get(YEAR), cal.get(MONTH), cal.get(DATE));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String newDate = String.format(Locale.getDefault(), FORMAT_DATE_PICKER_DISPLAY_DATE, day, month + 1, year);
        longToaster(activity, String.format("The date is : %s", newDate));

        if (callerId == Constants.Caller.CALLER_MAIN_ACTIVITY) {
            MainActivity mainActivity = (MainActivity) this.activity;
            mainActivity.handleSelectedDate(newDate);
        }
    }
}