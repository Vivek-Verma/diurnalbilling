package org.v2.finance.expenses.monthly;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.analytics.monthly.MonthlyExpenseStats;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_MONTH_EXPENSE_ROW_DIALOG_BOX;
import static org.v2.finance.expenses.util.Helper.convertDateForMonthlyExpenseMapKey;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 23-10-2018
 * @since 23-10-2018
 */
public class MonthlyExpenseCalculator {

    private final Logger logger = LoggerFactory.getLogger(MonthlyExpenseCalculator.class);
    private final Map<Integer, String> mapOfIndexAndDate;

    public MonthlyExpenseCalculator(Map<Integer, String> mapOfIndexAndDate) {
        this.mapOfIndexAndDate = mapOfIndexAndDate;
    }

    /**
     * @return List of string of month values in most recent month to ancient order, Sample row => <bullet>. mmm 'yy : sign currency amount
     */
    public String[] getMonthlyExpense() {
        Map<String, Double> monthlyExpense = new TreeMap<>(Collections.reverseOrder()); //month v/s amount in that month

        //this loop calculates the amount of each month and updates the 'monthlyExpense' map
        for(String date : mapOfIndexAndDate.values()) {
            EntryOfADay localEntryOfADay = Helper.mapOfDateAndDailyEntries.get(date);
            String key = convertDateForMonthlyExpenseMapKey(date); //yyyy-MM -- output key

            double amountLocal = 0.0;

            if (monthlyExpense.containsKey(key)) {
                amountLocal = monthlyExpense.get(key);
            }
            double localFrom = localEntryOfADay.getDisplayAmount();
            if (ENTRY_TYPE_NEGATIVE.equals(localEntryOfADay.getDisplayAmountSign())) {
                localFrom *= -1;
            }
            amountLocal += localFrom;
            monthlyExpense.put(key, amountLocal);
        }

        //this generates the string array required for the GUI - monthly expense dialog
        String[] monthlyExpenseList = new String[monthlyExpense.size()];
        int i = 0;
        MonthlyExpenseStats.clearMonthlyExpenseMap();
        //for (Map.Entry<String, Double> mappingEntry : monthlyExpense.entrySet()) {
        for (String key : monthlyExpense.keySet()) {
            double expense = monthlyExpense.get(key);
            key = Helper.renderDateToReadableDate(key);
            monthlyExpenseList[i] = String.format(Locale.getDefault(), FORMAT_MONTH_EXPENSE_ROW_DIALOG_BOX, key, (expense >= 0 ? ENTRY_TYPE_POSITIVE : ENTRY_TYPE_NEGATIVE), CurrencyUtil.currencySignToDisplay, Math.abs(expense));
            if (BuildConfig.DEBUG) logger.info("i : {} :: {}", i, monthlyExpenseList[i]);
            MonthlyExpenseStats.insertEntryInMonthlyExpenseMap(monthlyExpenseList[i], expense);
            i++;
        }

        return monthlyExpenseList;
    }
}
