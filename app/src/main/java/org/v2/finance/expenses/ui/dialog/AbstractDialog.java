package org.v2.finance.expenses.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.R;

/**
 * @author Vivek Verma
 * @since 28-11-2019
 */
public abstract class AbstractDialog {

    final Logger logger = LoggerFactory.getLogger(AbstractDialog.class);

    final Activity context;
    final Dialog dialog;
    final View view;

    AbstractDialog(Activity context, String title, int layout) {
        this.context = context;

        dialog = new Dialog(context, R.style.Dialog);
        dialog.setTitle(title);
        view = LayoutInflater.from(context).inflate(layout, context.findViewById(android.R.id.content), false);
        dialog.setContentView(view);
    }

    public abstract <T> void fillUpDialog(T[] content);

    public void fireUpDialog() {
        try {
            dialog.show();
        } catch (Exception e) {
            logger.error("Failed to bring up the dialog because of: ", e);
        }
    }
}
