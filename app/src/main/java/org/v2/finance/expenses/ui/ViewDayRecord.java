package org.v2.finance.expenses.ui;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.BuildConfig;
import org.v2.finance.expenses.R;
import org.v2.finance.expenses.adapter.TotalEntriesListAdapter;
import org.v2.finance.expenses.contracts.DeletionContract;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.launcher.ActivityStarter;
import org.v2.finance.expenses.port.export.Export;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.importer.ImportToLocalPrefs;
import org.v2.finance.expenses.ui.dialog.AbstractDialog;
import org.v2.finance.expenses.ui.dialog.DeletionDialog;
import org.v2.finance.expenses.util.CurrencyUtil;
import org.v2.finance.expenses.util.Helper;
import org.v2.finance.expenses.util.ModelForDeletion;
import org.v2.finance.expenses.util.TimingUtil;
import org.v2.finance.expenses.util.ToasterUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import static org.v2.finance.expenses.constants.Constants.BILLING_DIRECTORY_NAME;
import static org.v2.finance.expenses.constants.Constants.BILLING_FILE_NAME_PREFIX;
import static org.v2.finance.expenses.constants.Constants.DEFAULT_TITLE;
import static org.v2.finance.expenses.constants.Constants.EMPTY_STR;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.FILE_DIRECTORY_HIERARCHY;
import static org.v2.finance.expenses.constants.Constants.FILE_PATH_PREFIX_INTERNAL_STORAGE;
import static org.v2.finance.expenses.constants.Constants.FORMAT_DISPLAY_AMOMUNT;
import static org.v2.finance.expenses.constants.Constants.GRAY;
import static org.v2.finance.expenses.constants.Constants.GREEN_DARK;
import static org.v2.finance.expenses.constants.Constants.KEY_EXTRAS_INTENT_TX_DATA;
import static org.v2.finance.expenses.constants.Constants.RED;
import static org.v2.finance.expenses.constants.Constants.ROUTING_FROM_DAY_RECORD;
import static org.v2.finance.expenses.constants.Constants.ROUTING_TO_VIEW_DAY_RECORD;
import static org.v2.finance.expenses.util.Helper.activateHeavyImportingFlag;
import static org.v2.finance.expenses.util.Helper.deactivateHeavyImportingFlag;
import static org.v2.finance.expenses.util.Helper.fireUpBannerAd;
import static org.v2.finance.expenses.util.Helper.getLayOverEntryOfADay;
import static org.v2.finance.expenses.util.Helper.getRoutingPath;
import static org.v2.finance.expenses.util.ToasterUtil.shortToaster;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 23-09-2018
 */
public class ViewDayRecord extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, DeletionContract {

    private final Logger logger = LoggerFactory.getLogger(ViewDayRecord.class);

    private final Activity context = this;

    TextView displayDateForDayRecord;
    TextView displayDayRecordAmountSign;
    TextView displayDayRecordCurrency;
    TextView displayDayRecordAmount;
    EditText textDisplayTitleForDayRecord;
    ImageButton buttonAddNewEntry;
    ImageButton buttonSave;
    ImageButton buttonDelete;
    ListView dayRecordListView;
    FloatingActionButton fabAddNewEntry;

    static LinkedList<Entry> entriesList;
    static EntryOfADay entryOfADay;
    private final Map<Integer, Entry> indexAndEntryMap = new HashMap<>();

    private final ListAdapter EMPTY_LIST_VIEW_ADAPTER = null;

    //for the deletion part
    private ViewGroup defViewGroup;
    private static final Set<Integer> itemPositionsToBeDeleted = new TreeSet<>();
    private LocalSavedPrefs localSavedPrefs;
    private ExportToInternalPrefs exportToInternalCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_record);
        deactivateHeavyImportingFlag();

        Toolbar toolbar = findViewById(R.id.toolbar_day_rec);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
            try {
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(view -> onBackPressed());
            } catch (NullPointerException ignored) {
            }
        }

        localSavedPrefs = (LocalSavedPrefs) getIntent().getSerializableExtra(KEY_EXTRAS_INTENT_TX_DATA);
        boolean runPopulate = false;
        if (localSavedPrefs == null) { // if MainActivity not launched from BAU flow
            localSavedPrefs = new LocalSavedPrefs();
            runPopulate = true;
        }
        ImportToLocalPrefs importToLocalPrefs = new ImportToLocalPrefs(context, localSavedPrefs);
        exportToInternalCache = new ExportToInternalPrefs(context, localSavedPrefs);
        if (runPopulate) { // if MainActivity not launched from BAU flow
            importToLocalPrefs.populateLocalPrefsFromActualInternalPrefs();
        }

        if (BuildConfig.DEBUG) logger.info("starting new class of day record");

        fireUpBannerAd(context, R.id.banner_ad_day_rec);

        displayDateForDayRecord = findViewById(R.id.displayDateForDayRecord);
        buttonSave = findViewById(R.id.buttonSave);
        buttonDelete = findViewById(R.id.buttonDeleteFromDayRecord);
        textDisplayTitleForDayRecord = findViewById(R.id.textDisplayTitleForDayRecord);
        buttonAddNewEntry = findViewById(R.id.buttonAddNewEntry);
        //buttonAddNewEntry.setImageResource(ICON_ADD_NEW_RES_ID);
        displayDayRecordAmountSign = findViewById(R.id.displayDayRecordAmountSign);
        displayDayRecordCurrency = findViewById(R.id.displayDayRecordCurrency);
        displayDayRecordAmount = findViewById(R.id.displayDayRecordAmount);
        dayRecordListView = findViewById(R.id.dayRecordListView);
        fabAddNewEntry = findViewById(R.id.fabAddNewEntry);

        buttonSave.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        buttonAddNewEntry.setOnClickListener(this);
        fabAddNewEntry.setOnClickListener(this);

        dayRecordListView.setOnItemClickListener(this);

        defViewGroup = findViewById(android.R.id.content);
        if (BuildConfig.DEBUG) logger.debug("After init of the new class");
        int receivedRoutingPath = getRoutingPath();
        //entryOfADay = (EntryOfADay) getIntent().getSerializableExtra(KEY_TO_DAILY_RECORD);
        entryOfADay = getLayOverEntryOfADay();
        Helper.layOverEntry = null;
        if (BuildConfig.DEBUG) logger.info("Reading the entryOfADay object => {}", entryOfADay.toString());

        displayDateForDayRecord.setText(entryOfADay.getDisplayDate());
        displayDayRecordCurrency.setText(CurrencyUtil.currencySignToDisplay);
        displayDayRecordAmountSign.setText(entryOfADay.getDisplayAmountSign());
        displayDayRecordAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, entryOfADay.getDisplayAmount()));

        entriesList = entryOfADay.getDailyEntries();
        enrichEntriesListInDescOrder(entriesList);

        int colorToBeAssigned = GRAY;
        /*if (receivedRoutingPath == ROUTING_TO_NEW_DAY_RECORD) {
            buttonEditDate.setEnabled(true);
        }*/
        if (receivedRoutingPath == ROUTING_TO_VIEW_DAY_RECORD) {
            textDisplayTitleForDayRecord.setText(entryOfADay.getTitle());
            if (!Helper.isDayRecordEditable) {
                if (entriesList != null) {
                    //dayRecordListView.setEnabled(false);
                    dayRecordListView.setOnItemClickListener(null);
                }
                buttonSave.setEnabled(false);
                textDisplayTitleForDayRecord.setEnabled(false);
                buttonAddNewEntry.setEnabled(false);
                fabAddNewEntry.setEnabled(false);
                buttonDelete.setEnabled(false);
            }

            colorToBeAssigned = RED; //default
            if (ENTRY_TYPE_POSITIVE.equals(entryOfADay.getDisplayAmountSign())) {
                if (entryOfADay.getDisplayAmount() == 0) colorToBeAssigned = GRAY;
                else colorToBeAssigned = GREEN_DARK;
            }

            enrichDayRecordForEntriesDisplay(entriesList);
        }

        displayDayRecordAmountSign.setTextColor(colorToBeAssigned);
        displayDayRecordCurrency.setTextColor(colorToBeAssigned);
        displayDayRecordAmount.setTextColor(colorToBeAssigned);
    }

    private void enrichEntriesListInDescOrder(LinkedList<Entry> entriesList) {
        if (BuildConfig.DEBUG) logger.info("Will be deciding whether to sort the entries of this day!");
        if (decideToSort(entriesList)) {
            if (BuildConfig.DEBUG) logger.info("Will be sorting the entries of this day!");
            Collections.sort(entriesList, new Comparator<Entry>() {
                @Override
                public int compare(Entry o1, Entry o2) {
                    return Double.compare(o2.getAmount(), o1.getAmount());
                }
            });
        }
    }

    private boolean decideToSort(LinkedList<Entry> entriesList) {
        if (entriesList.isEmpty()) return false;
        double val1 = entriesList.get(0).getAmount();
        //checking if the list is already sorted DESC
        for (Entry entry : entriesList) {
            if (entry.getAmount() > val1) return true;
            val1 = entry.getAmount();
        }
        return false;
    }

    public static Set<Integer> getItemPositionsToBeDeleted() {
        return itemPositionsToBeDeleted;
    }

    private void enrichDayRecordForEntriesDisplay(LinkedList<Entry> entriesList) {
        updateIndexMappingForEntries(entriesList);
        updateListView(entriesList); //updating list only if there is something in it ... obviously
        reCalculateDailyAmount(entriesList);
    }

    private void updateIndexMappingForEntries(LinkedList<Entry> entriesList) {
        int index = 0;
        indexAndEntryMap.clear();
        for (Entry entry : entriesList) {
            indexAndEntryMap.put(index++, entry);
        }
    }

    public void updateListView(LinkedList<Entry> entriesList) {
        dayRecordListView.setAdapter(EMPTY_LIST_VIEW_ADAPTER); //for clearing and then re-filling
        TotalEntriesListAdapter totalEntriesListAdapter = new TotalEntriesListAdapter(this, entriesList);
        dayRecordListView.setAdapter(totalEntriesListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) logger.info("shouting from the onResume class of view day record class");

        //re-calc the daily amount from here.. i.e.  a function call for the same
        enrichEntriesListInDescOrder(entriesList);
        reCalculateDailyAmount(entriesList);
        if (!entriesList.isEmpty()) updateListView(entriesList);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Helper.ROUTING_PATH = ROUTING_FROM_DAY_RECORD;
        //deactivateHeavyImportingFlag(); //as for saving the data, this will not be the path //default now
    }

    @Override
    public void onClick(View view) {
        if (view == buttonDelete) {
            processDayRecordsForDeletion();
        } else if (view == buttonAddNewEntry || view == fabAddNewEntry) {
            new ActivityStarter(context).createNewEntryToDay(displayDateForDayRecord.getText().toString(), entryOfADay);
        } else if (view == buttonSave) {
            saveDayRecord();
        }
    }

    private void processDayRecordsForDeletion() {
        if (entriesList.isEmpty()) {
            logger.info("Nothing to delete..");
            shortToaster(context, "Nothing to delete");
            return;
        }

        //generating data to be fed
        ModelForDeletion[] modelItems = new ModelForDeletion[entriesList.size()];
        for (int i = 0; i < entriesList.size(); i++)
            modelItems[i] = new ModelForDeletion(entriesList.get(i).toString());

        itemPositionsToBeDeleted.clear();
        final AbstractDialog dialog = new DeletionDialog(context, this, "Entries present:--", R.layout.deletion_prompt);
        dialog.fillUpDialog(modelItems);
        dialog.fireUpDialog();
    }

    private void saveDayRecord() {
        EntryOfADay saveEntryOfADay = getLayOverEntryOfADay();

        if (saveEntryOfADay.getDailyEntries().isEmpty()) {
            ToasterUtil.longToaster(context, "Not saving empty day record");
            Helper.mapOfDateAndDailyEntries.remove(saveEntryOfADay.getDisplayDate()); //this might cause trouble -- not sure about it though - 20210421
            Helper.deactivateHeavyImportingFlag();
            onBackPressed();
            return;
        }

        if (saveEntryOfADay.getTitle() == null) { //whilst reading from the file
            saveEntryOfADay.setTitle(DEFAULT_TITLE);
        }

        saveEntryOfADay.setTitle(textDisplayTitleForDayRecord.getText() != null ? textDisplayTitleForDayRecord.getText().toString() : EMPTY_STR);
        saveEntryOfADay.calculateDayExpense();
        //////saveEntryOfADay.setDisplayCurrency("₹"); //for rectifying the error in display currency of 23-11-2018
        final String data = saveEntryOfADay.getEntireDataForSaving();
        if (BuildConfig.DEBUG) logger.info("data :-\n{}", data);

        StringBuilder fileNameBuilder = new StringBuilder();
        fileNameBuilder.append(FILE_PATH_PREFIX_INTERNAL_STORAGE).append(FILE_DIRECTORY_HIERARCHY).append(BILLING_DIRECTORY_NAME).append(FILE_DIRECTORY_HIERARCHY).append(BILLING_FILE_NAME_PREFIX);

        String saveDate = entryOfADay.getDisplayDate();
        final String fileName = fileNameBuilder.append(Helper.convertDisplayDateToFileTitleDate(saveDate)).toString();
        if (BuildConfig.DEBUG) logger.info("The file name being used : {}", fileName);

        Helper.incrementNumberOfSavesPerformed();

        final boolean saveOpResult = new Export(fileName, data).export();
        if (saveOpResult) {
            ToasterUtil.shortToaster(this, "Records updated successfully!");

            localSavedPrefs.setLastSavedTimeStamp(TimingUtil.extractCurrentUtcTimestamp());
            exportToInternalCache.writeLastSavedTime();
            //not invoking a call to dbi server just yet, as it will lead to starting of heroku server again just for noting a ts.
            //Figure out an effective way to do so.

            activateHeavyImportingFlag();
            Helper.ROUTING_PATH = ROUTING_FROM_DAY_RECORD;
            finish();
        } else {
            //deactivateHeavyImportingFlag(); //default now
            if (BuildConfig.DEBUG) logger.error("Error in writing to the internal storage!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (BuildConfig.DEBUG) if (BuildConfig.DEBUG) logger.info("Position selected : {}", position);
        Entry entryToIndex = entriesList.get(position);
        if (BuildConfig.DEBUG) logger.info("Total entries list adapter : {}", entryToIndex.getAmount());

        new ActivityStarter(context).viewEntryOfADay(displayDateForDayRecord.getText().toString(), entryToIndex, entryOfADay);
    }

    private void reCalculateDailyAmount(LinkedList<Entry> entriesList) {
        double amount = 0;

        for (Entry entry : entriesList) {
            if (!ENTRY_TYPE_COMMENT.equals(entry.getEntryType())) {
                double local = entry.getAmount();
                amount += (ENTRY_TYPE_NEGATIVE.equals(entry.getEntryType())) ? (local * -1) : local;
            }
        }

        int colorToBeAssigned = RED;
        String displayAmtSign = ENTRY_TYPE_POSITIVE;
        if (amount >= 0) {
            if (amount == 0) colorToBeAssigned = GRAY;
            else colorToBeAssigned = GREEN_DARK;
        } else {
            displayAmtSign = ENTRY_TYPE_NEGATIVE;
        }
        displayDayRecordAmountSign.setText(displayAmtSign);
        entryOfADay.setDisplayAmountSign(displayAmtSign); //updating the object's amount sign

        displayDayRecordAmountSign.setTextColor(colorToBeAssigned);
        displayDayRecordCurrency.setTextColor(colorToBeAssigned);
        displayDayRecordAmount.setTextColor(colorToBeAssigned);

        amount = Math.abs(amount);
        displayDayRecordAmount.setText(String.format(Locale.getDefault(), FORMAT_DISPLAY_AMOMUNT, amount));
        entryOfADay.setDisplayAmount(amount); // updating the object's amount
    }


    @Override
    public void handleDialogOk() {
        if (!itemPositionsToBeDeleted.isEmpty()) {
            for (Integer index : itemPositionsToBeDeleted) {
                if (BuildConfig.DEBUG) logger.info("Removing '{}' at index {}", entriesList.get(index).toString(), index);
                entriesList.remove(indexAndEntryMap.get(index));
            }
            if (BuildConfig.DEBUG) logger.info("Entries list size now : {}", entriesList.size());
            enrichDayRecordForEntriesDisplay(entriesList);
        } else {
            logger.info("Nothing selected to delete from the day record...");
        }
    }

    @Override
    public void handleDialogCancel() {
        //DO NOTHING
    }
}
