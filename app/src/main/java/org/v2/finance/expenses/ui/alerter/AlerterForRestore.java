package org.v2.finance.expenses.ui.alerter;

import android.content.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.v2.finance.expenses.core.LocalSavedPrefs;
import org.v2.finance.expenses.port.export.ExportToInternalPrefs;
import org.v2.finance.expenses.port.importer.EmergencyBackupRestore;
import org.v2.finance.expenses.port.importer.EmergencyRestoreEnum;
import org.v2.finance.expenses.util.Helper;

import java.util.ArrayList;
import java.util.List;

import static org.v2.finance.expenses.constants.Constants.EXTERNAL_BACKUP_FILE_LOCATION;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 16-07-2019
 * @since 16-07-2019
 */
public class AlerterForRestore extends AbstractAlerter {

    private final Logger LOGGER = LoggerFactory.getLogger(AlerterForRestore.class);
    private EmergencyRestoreEnum selectedRestoreChoice = EmergencyRestoreEnum.LOCAL;
    private final LocalSavedPrefs localSavedPrefs;
    private final ExportToInternalPrefs exportToInternalCache;

    public AlerterForRestore(Context context, String title, String messageForDisplay, LocalSavedPrefs localSavedPrefs, ExportToInternalPrefs exportToInternalCache) {
        super(context, title, messageForDisplay);
        this.localSavedPrefs = localSavedPrefs;
        this.exportToInternalCache = exportToInternalCache;

        List<String> choices = new ArrayList<>(2);
        choices.add("Restore from Local Backup");
        if (Helper.isPremiumUser()) choices.add("Restore from Cloud Backup");
        final String[] choicesArr = new String[choices.size()];
        alert.setSingleChoiceItems(choices.toArray(choicesArr), selectedRestoreChoice.getRestoreSequence(), (dialog, which) ->
                selectedRestoreChoice = EmergencyRestoreEnum.values()[which]);
    }

    @Override
    void initOkButton(String textForButton, String displayPositive) {
        alert.setPositiveButton(textForButton, (dialog, which) -> {
            LOGGER.info("Going for emergency restore as user clicked OK");
            new EmergencyBackupRestore(context, EXTERNAL_BACKUP_FILE_LOCATION, selectedRestoreChoice, localSavedPrefs, exportToInternalCache).fireBackUpRestore();
        });
    }

    @Override
    void initNoButton(String textForButton, String displayNegative) {
        alert.setNegativeButton(textForButton, (dialog, which) -> {
            LOGGER.warn("User denied emergency restore..");
            displayToast(displayNegative);
        });
    }
}
