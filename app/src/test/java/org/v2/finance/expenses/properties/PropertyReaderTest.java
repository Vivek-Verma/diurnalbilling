package org.v2.finance.expenses.properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.v2.finance.expenses.constants.Constants.PRECISION;

/**
 * @author Vivek Verma
 * @since 3/5/20
 */
@RunWith(JUnit4.class)
public class PropertyReaderTest {
    private final PropertyReader propertyReader = new PropertyReader();

    @Test
    public void testGetProperty() {
        assertEquals("t12", propertyReader.getProperty("D1", "t12"));
    }

    @Test
    public void testGetIntProperty() {
        assertEquals(10, propertyReader.getIntProperty("D1", 10));
    }

    @Test
    public void testGetDoubleProperty() {
        assertEquals(10.1, propertyReader.getDoubleProperty("D1", 10.1), PRECISION);
    }

    @Test
    public void testGetBooleanProperty() {
        assertFalse(propertyReader.getBooleanProperty("D1"));
    }
}