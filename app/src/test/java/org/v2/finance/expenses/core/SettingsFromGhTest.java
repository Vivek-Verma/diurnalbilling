package org.v2.finance.expenses.core;

import org.junit.Test;
import org.v2.finance.expenses.core.settings.SettingsFromGh;

import static org.junit.Assert.assertEquals;

/**
 * @author Vivek Verma
 * @since 4/5/21
 */
public class SettingsFromGhTest {

    @Test
    public void testSettingsReadBack() {
        String settingsJson = VersionCheckTest.readSampleFile("/home/v2k/AndroidStudioProjects/diurnalbilling/app/src/test/java/org/v2/finance/expenses/core/amaterasu_settings-test.json");

        SettingsFromGh settingsFromGH = SettingsFromGh.populatePaymentDetails(settingsJson);
        System.out.println(settingsFromGH);
        assertEquals("SettingsFromGh{payment=Payment{upi=Upi{id='money-warden@apl', amount=30.0, baseCurrency='INR', fxRates=[FxRate{curr='INR', rate=1.0}, FxRate{curr='CND', rate=61.0}, FxRate{curr='EUR', rate=90.0}, FxRate{curr='FR', rate=90.0}, FxRate{curr='GBP', rate=103.0}, " +
                "FxRate{curr='RUB'," +
                " rate=1.0}, FxRate{curr='USD', rate=75.0}, FxRate{curr='YEN', rate=0.75}]}}}", settingsFromGH.toString());

        assertEquals(1.0, settingsFromGH.getPayment().getUpi().getFxRates().get(0).getRate(), Math.pow(10, -6));
    }
}
