package org.v2.finance.expenses.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Vivek Verma
 * @since 17/3/21
 */
@RunWith(JUnit4.class)
public class SignInUpUtilTest {
    @Test
    public void testVerifyUserEmail() {
        assertTrue(SignInUpUtil.verifyUserEmail("someone@somewhere.com"));
        assertTrue(SignInUpUtil.verifyUserEmail("someone.somE@somewhere.com"));
        assertTrue(SignInUpUtil.verifyUserEmail("someone_somE@somewhere.com"));
        assertFalse(SignInUpUtil.verifyUserEmail("someo#$ne@somewhere.com"));
    }

    @Test
    public void testVerifyUserCred() {
        assertTrue(SignInUpUtil.verifyUserCred("secret"));
        assertTrue(SignInUpUtil.verifyUserCred("secret"));
        assertFalse(SignInUpUtil.verifyUserCred(""));
        //assertFalse(SignInUpUtil.verifyUserCred(null));
    }

    @Test
    public void testComputeExpiryTimestamp() {
        long processedTimestamp = TimingUtil.computeExpiryTimestamp(10);
        Date date = new Date(processedTimestamp);
        System.out.println(date);

        processedTimestamp = TimingUtil.computeExpiryTimestamp(15);
        System.out.println(processedTimestamp);
        date = new Date(processedTimestamp);
        System.out.println(date);
    }
}