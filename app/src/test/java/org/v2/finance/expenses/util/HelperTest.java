package org.v2.finance.expenses.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.v2.finance.expenses.util.Helper.convertDisplayDateToFileTitleDate;
import static org.v2.finance.expenses.util.Helper.obtainDayOfWeek;
import static org.v2.finance.expenses.util.Helper.returnFormattedDateForImport;

/**
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 17-11-2018
 * @since 17-11-2018
 */
public class HelperTest {

    @Test
    public void testConvertDisplayDateToFileTitleDate() {
        assertEquals("2018_11_18", convertDisplayDateToFileTitleDate("18-11-2018"));
        assertEquals("2018_11_8", convertDisplayDateToFileTitleDate("8-11-2018"));
        assertEquals("20112_151_81", convertDisplayDateToFileTitleDate("81-151-20112"));
    }

    @Test
    public void testReturnFormattedDateForImport() {
        assertEquals("2018_11_18", returnFormattedDateForImport("18-11-2018"));
        assertEquals("2018_11_08", returnFormattedDateForImport("8-11-2018"));
    }

    @Test
    public void testObtainDayOfWeek() {
        assertEquals("TUE", Helper.obtainDayOfWeek("10-09-2019"));
    }

    @Test
    @Ignore("Ignoring for now... will investigate why it's not working")
    public void testGetMonthNumberFromName() {
        Assert.assertEquals("02", Helper.getMonthNumberFromName("Feb"));
        Assert.assertEquals("11", Helper.getMonthNumberFromName("Nov"));
        Assert.assertEquals("12", Helper.getMonthNumberFromName("Dec"));
        Assert.assertEquals("05", Helper.getMonthNumberFromName("May"));
        Assert.assertEquals("01", Helper.getMonthNumberFromName("Jan"));
    }

    @Test
    public void testExtractEOADInOrder() {

    }

    @Test
    public void testConvertDisplayDateToActualDate() {
        Date date = Helper.convertDisplayDateToActualDate("14-04-2019");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        assertEquals(14, calendar.get(Calendar.DATE));
        assertEquals(3, calendar.get(Calendar.MONTH));
        assertEquals(2019, calendar.get(Calendar.YEAR));
    }

    @Test
    public void testIsDayWeekend() {
        Date date = Helper.convertDisplayDateToActualDate("12-04-2019"); //friday
        assertFalse(Helper.isDayWeekend(obtainDayOfWeek(date)));

        date = Helper.convertDisplayDateToActualDate("13-04-2019");//saturday
        assertTrue(Helper.isDayWeekend(obtainDayOfWeek(date)));

        date = Helper.convertDisplayDateToActualDate("14-04-2019");
        assertTrue(Helper.isDayWeekend(obtainDayOfWeek(date)));

        date = Helper.convertDisplayDateToActualDate("15-04-2019");
        assertFalse(Helper.isDayWeekend(obtainDayOfWeek(date)));
    }

    @Test
    public void testGetCurrentTime() {
        Long timeStamp = Helper.getCurrentTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        System.out.println(timeStamp + " -- date : " + calendar.getTime());
    }

    @Test
    public void testGetCurrentDate() {
        System.out.println(Helper.getCurrentDate());
    }
}