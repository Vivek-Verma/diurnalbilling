package org.v2.finance.expenses.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

/**
 * @author Vivek Verma
 * @since 8/4/21
 */
@RunWith(JUnit4.class)
public class TimingUtilTest {

    @Test
    public void testExtractCurrentUtcTimestamp() {
        System.out.println(TimingUtil.extractCurrentUtcTimestamp());
    }

    @Test
    public void testParseTimestampForSettingsDisplay() {
        long ms = 1619807787313L;
        String formattedTime = TimingUtil.parseTimestampForSettingsDisplay(ms);
        System.out.println(formattedTime);
        assertEquals("01-05-21 00:06:27", formattedTime);
    }
}