package org.v2.finance.expenses.core;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Vivek Verma
 * @since 13-10-2019
 */
public class VersionCheckTest {
    VersionCheck versionCheck;
    Gson gson;
    private final String sampleAmaterasu = "{\"version\": \"1.0.6\",\"update\": \"u1\"}";

    public static String readSampleFile(String filePath) {
        StringBuilder data = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(new File(filePath)))) {
            String buffer;
            while ((buffer = in.readLine()) != null)
                data.append(buffer).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(data);
        return data.toString();
    }

    @Before
    public void before() {
        gson = new Gson();
        versionCheck = gson.fromJson(sampleAmaterasu, VersionCheck.class);
    }

    @Test
    public void test1() {
        String sampleJson = readSampleFile("/home/v2k/AndroidStudioProjects/diurnalbilling/app/src/test/java/org/v2/finance/expenses/core/amaterasu.json");
        versionCheck = gson.fromJson(sampleJson, VersionCheck.class);
        System.out.println(versionCheck.getChangeLog());
        System.out.println(versionCheck.getChangeLog().get(0));
    }

    @Test
    public void test() {
        versionCheck.computeActualVersion();
        //versionCheck = new VersionCheck("1.0.6", "u1");
        Assert.assertEquals(106, versionCheck.getVersionRemote(), 0.0);
        Assert.assertEquals("u1", versionCheck.getUpdate());

        Assert.assertTrue(versionCheck.isNewVersionAvailable("1.0.5"));
        Assert.assertTrue(versionCheck.isNewVersionAvailable("1"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.4.0"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.0.6"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.0.7"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("20.5.2"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("20"));
    }

    @Test
    public void testConvertStringToDouble() {
        Assert.assertEquals(1061, versionCheck.convertStringToDouble("10.6.1"), 0.0);
        Assert.assertEquals(161, versionCheck.convertStringToDouble("1.6.1"), 0.0);
        Assert.assertEquals(106.5, versionCheck.convertStringToDouble("1.0.6.5"), 0.0);
    }

    @Test
    public void testForDecimalRelease() {
        versionCheck.setVersion("1.0.6.5");
        versionCheck.computeActualVersion();
        Assert.assertEquals(106.5, versionCheck.getVersionRemote(), 0.0);
        Assert.assertEquals("u1", versionCheck.getUpdate());

        Assert.assertTrue(versionCheck.isNewVersionAvailable("1.0.5"));
        Assert.assertTrue(versionCheck.isNewVersionAvailable("1"));
        Assert.assertTrue(versionCheck.isNewVersionAvailable("1.0.6"));
        Assert.assertTrue(versionCheck.isNewVersionAvailable("1.0.6.4"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.0.6.6"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.4.0"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("1.0.7"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("20.5.2"));
        Assert.assertFalse(versionCheck.isNewVersionAvailable("20"));
    }
}