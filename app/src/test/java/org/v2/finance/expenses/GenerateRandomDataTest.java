package org.v2.finance.expenses;

import org.junit.Test;
import org.v2.finance.expenses.core.Entry;
import org.v2.finance.expenses.core.EntryOfADay;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_COMMENT;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_NEGATIVE;
import static org.v2.finance.expenses.constants.Constants.ENTRY_TYPE_POSITIVE;
import static org.v2.finance.expenses.constants.Constants.NEW_LINE;

/**
 * This class generates random data for the app, and the resultant text file will serve as the input for the emulator
 *
 * @author Vivek Verma
 * @version 1.0
 * @lastMod 08-04-2021
 * @since 13-07-2019
 */

public class GenerateRandomDataTest {

    private final String[] signs = {ENTRY_TYPE_NEGATIVE, ENTRY_TYPE_POSITIVE, ENTRY_TYPE_COMMENT};

    @Test
    public void generateRandomDataForBilling() {
        StringBuilder testData = new StringBuilder();
        for (int i = 1; i <= 100; i++) {
            EntryOfADay entryOfADay = generateRandomEntryOfADay(5);
            testData.append(entryOfADay.getEntireDataForSaving()).append(NEW_LINE);
        }
        System.out.println(testData);
    }

    private EntryOfADay generateRandomEntryOfADay(int upperLimEntriesCount) {
        //14-04-2019 -TITLE- :: - ₹ 125.00
        int randomEntries = ThreadLocalRandom.current().nextInt(0, upperLimEntriesCount + 1);
        LinkedList<Entry> list = new LinkedList<>();
        for (int i = 1; i <= randomEntries; i++) list.add(generateRandomEntry());

        return new EntryOfADay(generateRandomDate(), generateRandomTitle(), list);
    }

    private Entry generateRandomEntry() {
        //- ₹ 100.00 : lunch
        String entryType = generateRandomSign();
        String description = getRandomDescription(50);
        if (ENTRY_TYPE_COMMENT.equals(entryType)) {
            return new Entry(description);
        }
        double expense = generateRandomExpense(1, 5);
        return new Entry(entryType, expense, description);
    }

    private String generateRandomSign() {
        int index = ThreadLocalRandom.current().nextInt(0, signs.length);
        return signs[index];
    }

    private double generateRandomExpense(int lowerLim, int upperLim) {
        return ThreadLocalRandom.current().nextDouble(lowerLim, Math.pow(10, upperLim));
    }

    private String generateRandomDate() {
        String baseEpochTime = "156";
        long suffixEpochTime = (long) (Math.random() * 10000000000L);
        baseEpochTime = baseEpochTime + suffixEpochTime;
        //System.out.println(System.currentTimeMillis());
        //System.out.println(baseEpochTime);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        calendar.setTimeInMillis(Long.valueOf(baseEpochTime));
        return sdf.format(calendar.getTime());
    }

    private String generateRandomTitle() {
        StringBuilder title = new StringBuilder();
        for (int i = 1; i <= 7; i++) title.append(getRandomCharUpper());
        return title.toString();
    }

    private char getRandomCharUpper() {
        return (char) ThreadLocalRandom.current().nextInt(65, 91);
    }

    private char getRandomChar() {
        return (char) ThreadLocalRandom.current().nextInt(47, 123);
    }

    private String getRandomDescription(int length) {
        StringBuilder desc = new StringBuilder();
        for (int i = 1; i <= length; i++) desc.append(getRandomChar());
        return desc.toString();
    }
}
